﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;
using System.Linq;

namespace KataHeap
{
    public class TrieNode<T>
    {
        private IList<TrieNode<T>> _childNodes;
        public char Key { get; private set; }
        public T Value { get; set; }
        public IEnumerable<TrieNode<T>> Children { get { return _childNodes; } }

        public TrieNode(char key)
        {
            Key = key;
        }

        public TrieNode<T> AddChildNode(char key)
        {
            if (_childNodes == null)
            {
                _childNodes = new List<TrieNode<T>>();
            }

            if (ContainsChildNode(key))
            {
                return GetChildNode(key);
            }

            var newNode = new TrieNode<T>(key);
            _childNodes.Add(newNode);
            return newNode;
        }

        public bool ContainsChildNode(char key)
        {
            return _childNodes != null
                   && _childNodes.Any(n => n.Key.Equals(key));
        }

        public TrieNode<T> GetChildNode(char key)
        {
            return _childNodes == null
                ? null
                : _childNodes.FirstOrDefault(n => n.Key.Equals(key));
        }
    }
}
