﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using KataSoundSynthesizer.Oscillators;

namespace KataSoundSynthesizer.Effect
{
    class Chorus : IEffectComponent
    {
        private enum Phase
        {
            Negative180,
            Negative90,
            Zero,
            Positive90,
            Positive180
        }

        private const float PI2 = (float)(2 * Math.PI);
        private const float FrequencyScale = 5.0f;
        private const int DelayLineMask = 4095;
        private const int DelayLineLength = DelayLineMask + 1;
        private readonly float[] _leftDelayLine = new float[DelayLineLength];
        private readonly float[] _rightDelayLine = new float[DelayLineLength];
        private Phase _phase = Phase.Positive90;
        private float _sine;
        private float _cosine;
        private float _f;
        private int _delayLength;
        private int _writeIndex;
        private float _delay;
        private float _frequency;
        private int _sampleRate;

        public float Mix { get; set; }
        public float FeedbackLevel { get; set; }
        public float Delay
        {
            get { return _delay; } 
            set { _delay = value; InitializeDefaults(); }
        }

        public float Depth { get; set; }

        public float Frequency 
        {
            get { return _frequency; }
            set { _frequency = value; InitializeDefaults(); }
        }

        public int SampleRate
        {
            get { return _sampleRate; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("SampleRate", "must be >= 1");
                }

                _sampleRate = value;
                InitializeDefaults();
            }
        }


        public Chorus()
        {
            Mix = 0.75f;
            Delay = 0.5f;
            Depth = 0.0003f;
            FeedbackLevel = 0.05f;
            Frequency = 1.0f;
            _sampleRate = 1;

            InitializeDefaults();
        }

        private Chorus(Chorus chorus)
        {
            _phase = chorus._phase;
            _sampleRate = chorus._sampleRate;
            Mix = chorus.Mix;
            FeedbackLevel = chorus.FeedbackLevel;
            Delay = chorus.Delay;
            Depth = chorus.Depth;
            Frequency = chorus.Frequency;

            InitializeDefaults();
        }

        public IEffectComponent MakeInstanceCopy()
        {
            return new Chorus(this);
        }

        private void InitializeDefaults()
        {
            _sine = 0;
            _cosine = 1;
            _f = PI2*(Frequency*FrequencyScale)/_sampleRate;

            _delayLength = (int)(Delay*(DelayLineLength/4));
            _writeIndex = 0;
        }


        public void Apply(float[,] input)
        {
            var inputLength = input.Length/2;
            var readIndex = 0f;
            var delayOutput = 0f;

            for (var i = 0; i < inputLength; ++i)
            {
                IncrementFrequency();

                readIndex = CalculateReadIndex(GetPhaseOutputLeft());
                delayOutput = GetDelayOutput(readIndex, _leftDelayLine);
                _leftDelayLine[_writeIndex] = input[0, i] + delayOutput*FeedbackLevel + 1;

                readIndex = CalculateReadIndex(GetPhaseOutputRight());
                delayOutput = GetDelayOutput(readIndex, _rightDelayLine);
                _rightDelayLine[_writeIndex] = input[1, i] + delayOutput*FeedbackLevel + 1;

                _writeIndex = (_writeIndex + 1) & DelayLineMask;
            }
        }

        public void Reset()
        {
            Array.Clear(_leftDelayLine, 0, _leftDelayLine.Length);
            Array.Clear(_rightDelayLine, 0, _rightDelayLine.Length);
        }

        private void IncrementFrequency()
        {
            _sine = _sine + _f*_cosine;
            _cosine = _cosine - _f*_sine;
        }

        private float CalculateReadIndex(float output)
        {
            var offset = PowerOfTwoTable.GetPower(output*Depth)*_delayLength;
            var readIndex = _writeIndex - offset;

            if (readIndex < 0)
            {
                readIndex += DelayLineLength - 1;
                readIndex = Math.Max(readIndex, 0);
            }

            return readIndex;
        }

        private float GetPhaseOutputLeft()
        {
            var phaseOutput = 0.0f;

            switch (_phase)
            {
                case Phase.Zero:
                case Phase.Positive90:
                case Phase.Positive180:
                    phaseOutput = _sine;
                    break;

                case Phase.Negative180:
                    phaseOutput = -_sine;
                    break;

                case Phase.Negative90:
                    phaseOutput = -_cosine;
                    break;

                default:
                    break;
            }

            return phaseOutput;
        }

        private float GetPhaseOutputRight()
        {
            var phaseOutput = 0.0f;

            switch (_phase)
            {
                case Phase.Zero:
                    phaseOutput = _sine;
                    break;

                case Phase.Positive90:
                    phaseOutput = _cosine;
                    break;

                case Phase.Positive180:
                    phaseOutput = -_sine;
                    break;

                case Phase.Negative180:
                case Phase.Negative90:
                    phaseOutput = _sine;
                    break;

                default:
                    break;
            }

            return phaseOutput;
        }

        private static float GetDelayOutput(float readIndex, float[] delayLine)
        {
            var n = (int) readIndex;
            var x1 = delayLine[n] - 1;
            var x2 = delayLine[(n + 1) & DelayLineMask] - 1;

            return Lerp(readIndex, x1, x2);
        }

        private static float Lerp(float index, float x1, float x2)
        {
            var fractional = index - (int) index;
            var difference = x1 - x2;
            return x1 - difference*fractional;
        }
    }
}
