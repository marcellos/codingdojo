﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace KataHeap
{
    [TestFixture]
    public class LinkedListTests
    {
        private LinkedList<int> _linkedList;

        [SetUp]
        public void Setup()
        {
            _linkedList = new LinkedList<int>();
        }

        [TearDown]
        public void Teardown()
        {
            _linkedList = null;
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(LinkedList<int>), _linkedList);
        }

        [Test]
        public void Add_WithNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _linkedList.Add(null));
        }

        [Test]
        public void Add_WithNode_CorrectCount()
        {
            _linkedList.Add(new ListNode<int>(100));
            Assert.AreEqual(1, _linkedList.Count);
        }

        [Test]
        public void Add_With2Nodes_CorrectCount()
        {
            _linkedList.Add(new ListNode<int>(100));
            _linkedList.Add(new ListNode<int>(101));
            Assert.AreEqual(2, _linkedList.Count);
        }

        [Test]
        public void Add_With100Nodes_CorrectCount()
        {
            for (var i = 0; i < 100; ++i)
            {
                _linkedList.Add(new ListNode<int>(i));
            }
            Assert.AreEqual(100, _linkedList.Count);
        }

        [Test]
        public void Add_With10e6Nodes_CorrectCount()
        {
            const int NumberOfItems = (int) 10e6;

            for (var i = 0; i < NumberOfItems; ++i)
            {
                _linkedList.Add(new ListNode<int>(i));
            }
            Assert.AreEqual(NumberOfItems, _linkedList.Count);
        }

        [Test]
        public void InsertAt_WithPositionNodeNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _linkedList.InsertAt(null, new ListNode<int>(100)));
        }

        [Test]
        public void InsertAt_WithNodeNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _linkedList.InsertAt(new ListNode<int>(100), null));
        }

        [Test]
        public void InsertAt_WithNodeNotInList_ThrowsException()
        {
            var node1 = new ListNode<int>(100);
            var node2 = new ListNode<int>(120);
            _linkedList.Add(node1);

            Assert.Throws<ArgumentException>(() => _linkedList.InsertAt(node2, new ListNode<int>(110)));
        }

        [Test]
        public void InsertAt_With1Node_InsertedInCenter()
        {
            var node1 = new ListNode<int>(100);
            var node2 = new ListNode<int>(120);
            _linkedList.Add(node1);
            _linkedList.Add(node2);
            _linkedList.InsertAt(node1, new ListNode<int>(110));
            Assert.AreEqual(3, _linkedList.Count);
        }

        [Test]
        public void InsertAt_With1Node_InsertedAtTail()
        {
            var node1 = new ListNode<int>(100);
            var node2 = new ListNode<int>(110);
            _linkedList.Add(node1);
            _linkedList.Add(node2);
            _linkedList.InsertAt(node2, new ListNode<int>(120));
            Assert.AreEqual(3, _linkedList.Count);
        }

        [Test]
        public void Remove_WithNodeNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _linkedList.Remove(null));
        }

        [Test]
        public void Remove_WithNodeNotInList_ThrowsException()
        {
            var node = new ListNode<int>(100);

            Assert.Throws<ArgumentException>(() => _linkedList.Remove(node));
        }

        [Test]
        public void Remove_WithRootNodeOnly_CorrectCount()
        {
            var node = new ListNode<int>(100);
            _linkedList.Add(node);
            _linkedList.Remove(node);
            Assert.AreEqual(0, _linkedList.Count);
        }

        [Test]
        public void Remove_WithRootNode_CorrectCount()
        {
            var node1 = new ListNode<int>(100);
            var node2 = new ListNode<int>(110);
            _linkedList.Add(node1);
            _linkedList.Add(node2);
            _linkedList.Remove(node1);
            Assert.AreEqual(1, _linkedList.Count);
        }

        [Test]
        public void Remove_WithTailNode_CorrectCount()
        {
            var node1 = new ListNode<int>(100);
            var node2 = new ListNode<int>(110);
            _linkedList.Add(node1);
            _linkedList.Add(node2);
            _linkedList.Remove(node2);
            Assert.AreEqual(1, _linkedList.Count);
        }

        [Test]
        public void Find_WithNull_ThrowsException()
        {
            var linkedList = new LinkedList<string>();

            Assert.Throws<ArgumentNullException>(() => linkedList.Find(null));
        }

        [Test]
        public void Find_WithKeyNotInList_ReturnEmptyList()
        {
            _linkedList.Add(new ListNode<int>(100));
            _linkedList.Add(new ListNode<int>(110));
            _linkedList.Add(new ListNode<int>(120));
            var result = _linkedList.Find(500);
            Assert.IsInstanceOf(typeof(IEnumerable<ListNode<int>>), result);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void Find_WithKeyInList_ReturnListWith1Item()
        {
            _linkedList.Add(new ListNode<int>(100));
            _linkedList.Add(new ListNode<int>(110));
            _linkedList.Add(new ListNode<int>(120));
            var result = _linkedList.Find(110);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void Find_WithKey_ReturnListWith5Items()
        {
            for (var j = 0; j < 5; j++)
            {
                for (var i = 0; i < 10; i++)
                {
                    _linkedList.Add(new ListNode<int>(i));
                }
            }
            var result = _linkedList.Find(4);
            Assert.AreEqual(5, result.Count());
        }
    }
}
