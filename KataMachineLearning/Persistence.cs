﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace KataMachineLearning
{
    static class Persistence
    {
        private const string Delimiter = ";";

        public static ArtificialNeuralNetwork Load(string filepath)
        {
            var lines = File.ReadAllLines(filepath);
            var lineIndex = 0;

            var at = (Activation.ActivationType) Enum.Parse(typeof (Activation.ActivationType), lines[lineIndex++]);

            var layersString = lines[lineIndex++].Split(new[] { Delimiter }, StringSplitOptions.RemoveEmptyEntries);
            var layerNodes = ConvertArray<int>(layersString);

            var matrixSizes = MatrixSizes(layerNodes);
            var weightMatrices = new List<Matrix>();
            foreach (var matrixSize in matrixSizes)
            {
                var matrixLines = new string[matrixSize.Item2];
                for (var i = 0; i < matrixSize.Item2; ++i)
                {
                    matrixLines[i] = lines[lineIndex++];
                }

                var values = ConvertToArray(matrixSize, matrixLines);
                var w = CreateMatrix(matrixSize, values);
                weightMatrices.Add(w);
            }

            var ann = new ArtificialNeuralNetwork(layerNodes, weightMatrices.ToArray());
            ann.SetActivationType(at);

            return ann;
        }

        public static void Save(ArtificialNeuralNetwork ann, string filepath)
        {
            var lines = new List<string>();

            lines.Add(ann.ActivationType.ToString());
            lines.Add(string.Join(Delimiter, ann.LayerNodes));

            foreach (var w in ann.Weights)
            {
                for (var column = 0; column < w.Columns; ++column)
                {
                    var values = new double[w.Rows];
                    for (var row = 0; row < w.Rows; ++row)
                    {
                        values[row] = w[row, column];
                    }

                    lines.Add(string.Join(Delimiter, values));
                }
            }

            File.WriteAllLines(filepath, lines);
        }


        private static T[] ConvertArray<T>(string[] a)
        {
            var b = new T[a.Length];

            for (var i = 0; i < a.Length; ++i)
            {
                b[i] = (T) Convert.ChangeType(a[i], typeof (T));
            }

            return b;
        }

        private static Tuple<int, int>[] MatrixSizes(int[] layerNodes)
        {
            var sizes = new List<Tuple<int, int>>();

            for (var i = 0; i < layerNodes.Length - 1; ++i)
            {
                sizes.Add(new Tuple<int, int>(layerNodes[i], layerNodes[i + 1]));
            }

            return sizes.ToArray();
        }

        private static double[,] ConvertToArray(Tuple<int, int> matrixSize, string[] matrixLines)
        {
            var w = new double[matrixSize.Item1,matrixSize.Item2];

            for (var j = 0; j < matrixSize.Item2; ++j)
            {
                var valuesString = matrixLines[j].Split(new[] {Delimiter}, StringSplitOptions.RemoveEmptyEntries);
                var values = ConvertArray<double>(valuesString);

                for (var i = 0; i < matrixSize.Item1; ++i)
                {
                    w[i, j] = values[i];
                }
            }

            return w;
        }

        private static Matrix CreateMatrix(Tuple<int, int> matrixSize, double[,] values)
        {
            var m = new Matrix(matrixSize.Item1, matrixSize.Item2);

            for (var j = 0; j < m.Columns; ++j)
            {
                for (var i = 0; i < m.Rows; ++i)
                {
                    m[i, j] = values[i, j];
                }
            }

            return m;
        }
    }
}
