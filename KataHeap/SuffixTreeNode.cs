﻿#region license and copyright
/*
The MIT License, Copyright (c) 2011-2020 Marcel Schneider
for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataHeap
{
    public class SuffixTreeNode
    {
        public int Begin { get; private set; }
        public int End { get; private set; }
        public char Edge { get; private set; }
        public IDictionary<char, IList<SuffixTreeNode>> Children { get; private set; }
        public SuffixTreeNode Link { get; set; }

        public SuffixTreeNode(int begin, int end, char edge)
        {
            Begin = begin;
            End = end;
            Edge = edge;
            Children = new Dictionary<char, IList<SuffixTreeNode>>();
        }

        public void AddChildNode(char edge, SuffixTreeNode node)
        {
            if (Children.ContainsKey(edge))
            {
                var nodeList = Children[edge];
                nodeList.Add(node);
            }
            else
            {
                var nodeList = new List<SuffixTreeNode> {node};
                Children.Add(edge, nodeList);
            }
        }

        public int EdgeLength(int position)
        {
            return Math.Min(End, position + 1) - Begin;
        }

        public void SetBegin(int begin)
        {
            Begin = begin;
        }
    }
}
