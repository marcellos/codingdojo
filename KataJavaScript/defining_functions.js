/*

1) Defining Functions

*/

/*
assert(true, "I'll pass.");
assert("truey", "So will I.");
assert(false, "I'll fail.");
assert(null, "So will I.");
log("Just a simple log", "of", "values.", true);
error("I'm an error!");
*/

/*
function isNimble() { return true; }
var canFly = function() { return true; };
window.isDeadly = function() { return true; };
log(isNimble, canFly, isDeadly);
*/

/*
var canFly = function () { return true; };
window.isDeadly = function () { return true; };
assert(isNimble() && canFly() && isDeadly(), "Still works, even though isNimble is moved.");
function isNimble() { return true; }
*/

function stealthCheck() {
    return stealth();
    function stealth(){return true;}
}

stealthCheck();
