﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Runtime.InteropServices;

namespace KataUSB
{
    public class HidDeviceException : Exception
    {
        public HidDeviceException(string message)
            : base(message)
        { }

        public HidDeviceException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public static HidDeviceException CreateWithWin32Error(string message)
        {
            return new HidDeviceException(string.Format("Msg:{0} Win32Err:{1:X8}", message, Marshal.GetLastWin32Error()));
        }
    }
}
