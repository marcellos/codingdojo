﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Linq;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace KataCompiler
{
    [TestFixture]
    public class TextScannerTests
    {
        private TextScanner _textScanner;

        [SetUp]
        public void Setup()
        {
            _textScanner = new TextScanner();
        }

        [TearDown]
        public void Teardown()
        {
            _textScanner = null;
        }

        [Test]
        public void TextScanner_Scan_TextSpan()
        {
            Console.Out.WriteLine("tab is whitespace: {0}", Char.IsWhiteSpace('\t'));
            Console.Out.WriteLine("tab is control: {0}", Char.IsControl('\t'));
            var textScannerInput = Encoding.Default.GetBytes(Properties.Resources.TextScannerInput);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var textSpans = _textScanner.Scan(textReader);
                Assert.IsNotNull(textSpans);
                foreach (var textSpan in textSpans)
                {
                    Console.Out.WriteLine("type: '{0}', line: '{1}''{2}-{3}'", 
                        textSpan.SpanType, 
                        textSpan.Line, 
                        textSpan.Begin.Column, 
                        textSpan.End.Column);
                }
            }
        }

        [Test]
        public void TextScanner_Scan_TextSpanContainsText()
        {
            var textScannerInput = Encoding.Default.GetBytes(Properties.Resources.TextScannerInput);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var textSpans = _textScanner.Scan(textReader)
                    .Where(span => span.SpanType == SpanTypeEnum.Text);
                Assert.IsNotNull(textSpans);
                foreach (var textSpan in textSpans)
                {
                    Console.Out.WriteLine("text: '{0}'", textSpan.Text);
                    if (textSpan.Text.Contains('\t'))
                    {
                        Console.Out.WriteLine("oops tab - line: '{0}''{1}-{2}'",
                            textSpan.Line,
                            textSpan.Begin.Column,
                            textSpan.End.Column);
                    }
                }
            }
        }

        [Test]
        public void TextScanner_ScanScript_TextSpanContainsText()
        {
            var textScannerInput = Encoding.Default.GetBytes(Properties.Resources.jquery_extract);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var textSpans = _textScanner.Scan(textReader)
                    .Where(span => span.SpanType == SpanTypeEnum.Text);
                Assert.IsNotNull(textSpans);
                foreach (var textSpan in textSpans)
                {
                    Console.Out.WriteLine("text: '{0}'", textSpan.Text);
                }
            }
        }

        [Test]
        public void TextScanner_Scan_StringLiteralsArePreserved()
        {
            const string text = "The \"quick brown fox\" jumps over the \"\\\"lazy\\\"\" dog.";
            var textScannerInput = Encoding.Default.GetBytes(text);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var textSpans = _textScanner.Scan(textReader)
                    .Where(span => span.SpanType == SpanTypeEnum.Text);
                Assert.IsNotNull(textSpans);
                foreach (var textSpan in textSpans)
                {
                    Console.Out.WriteLine("text: '{0}'", textSpan.Text);
                }
            }
        }

    }
}
