﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace KataRx.Coroutines2
{
    public class Sequence : IDisposable
    {
        private IDisposable _sequence;
        private ISequentialResult _result;

        private readonly List<Func<IObserver<ISequentialResult>, ISequentialResult, ISequentialResult>> _sequences
            = new List<Func<IObserver<ISequentialResult>, ISequentialResult, ISequentialResult>>();

        public void Add(Func<IObserver<ISequentialResult>, ISequentialResult, ISequentialResult> sequence)
        {
            _sequences.Add(sequence);
        }

        public void Run(Action<Exception> exception, Action<ISequentialResult> completed)
        {
            //_sequence = Observable.Iterate(_Sequencer)
            //    .Subscribe(
            //        next => { },
            //        exception,
            //        () => completed(_result));
            //foreach (var observable in CreateObservables())
            //{
            //    observable.Subscribe(
            //        next => { },
            //        exception,
            //        () => completed(_result));
            //}

            CreateObservables().ToList().ForEach(observable =>
                    {
                        if (_sequence != null) 
                            _sequence.Dispose();
                        _sequence = observable.Subscribe(
                            next => { },
                            exception,
                            () => completed(_result)
                            );
                    }
                );
        }

        private IEnumerable<IObservable<object>> CreateObservables()
        {
            var x = 0;

            _result = new DefaultResult(null) { Value = this };

            while (x < _sequences.Count)
            {
                var sequence = _sequences[x];
                var step = Observable.Create<ISequentialResult>(
                    observer =>
                    {
                        var newResult = sequence(observer, _result);
                        newResult.Results.Add(_result);
                        newResult.Results.AddRange(_result.Results);
                        _result = newResult;
                        return () => { };
                    }
                    );

                yield return step;

                x++;
            }

            // add to self when done
            _result.Results.Add(_result);
        }

        public void Dispose()
        {
            _sequence.Dispose();
        }
    }

}
