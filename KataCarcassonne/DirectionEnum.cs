﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataCarcassonne
{
    enum DirectionEnum
    {
        Up,
        Right,
        Down,
        Left
    }
}
