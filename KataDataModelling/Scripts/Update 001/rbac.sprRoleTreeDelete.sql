IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rbac].[sprRoleTreeDelete]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [rbac].[sprRoleTreeDelete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		CodingDojo
-- Description:	Delete node from RoleTree
-- =============================================
CREATE PROCEDURE [rbac].[sprRoleTreeDelete]
(
	@roleTreeId INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY
	BEGIN TRANSACTION roleTreeDelete

	DECLARE 
		@nodeLeft INT,
		@nodeRight INT

	SELECT @nodeLeft=rt.[Left], @nodeRight=rt.[Right]
	FROM rbac.RoleTree rt
	WHERE rt.RoleTreeId = @roleTreeId 

	UPDATE rbac.RoleTree
	SET [Left] = [Left] - 1, [Right] = [Right] - 1
	WHERE [Left] > @nodeLeft 
		AND [Right] < @nodeRight
	
	UPDATE rbac.RoleTree 
	SET [Left] = [Left] - 2
	WHERE [Left] > @nodeLeft

	UPDATE rbac.RoleTree 
	SET [Right] = [Right] - 2
	WHERE [Right] > @nodeRight

	DELETE FROM rbac.RoleTree
	WHERE RoleTreeId = @roleTreeId

	COMMIT TRANSACTION roleTreeDelete
END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION roleTreeDelete

END CATCH

END

GO


