﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataEventSync
{
    public interface IEventAggregator
    {
        Action<Action> PublicationThreadMarshaler { get; set; }
        void Subscribe(object instance);
        void Unsubscribe(object instance);
        void Publish(object message);
        void Publish(object message, Action<Action> marshal);
    }
}
