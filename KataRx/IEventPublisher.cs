﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataRx
{
    public interface IEventPublisher
    {
        void Publish<T>(T message);
        IObservable<T> GetEvent<T>();
    }
}
