﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataEventSync
{
    [TestFixture]
    public class RulesEngineFixture
    {
        private IEventAggregator _events;
        private RulesEngine<string> _engine;

        [SetUp]
        public void Setup()
        {
            _events = new EventAggregator();
            _engine = new RulesEngine<string>(_events);
        }

        [TearDown]
        public void Teardown()
        {
            _engine = null;
            _events = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(RulesEngine<string>), _engine);
        }

        [Test]
        public void FaultyRule1Test()
        {
            var recipient = new Recipient();
            _events.Subscribe(recipient);
            var rule = new FaultyRule1();
            _engine.AddRule(rule);
            _events.Publish("something");
            Assert.IsInstanceOf(typeof(NotImplementedException), rule.Error);
        }

        [Test]
        public void FaultyRule2Test()
        {
            var recipient = new Recipient();
            _events.Subscribe(recipient);
            var rule = new FaultyRule2();
            _engine.AddRule(rule);
            _events.Publish("something");
            Assert.AreEqual("always", recipient.Message);
            _engine.RemoveRule(rule);
            _events.Unsubscribe(recipient);
        }

        [Test]
        public void Rule1Test()
        {
            var recipient = new Recipient();
            _events.Subscribe(recipient);
            var rule = new Rule1();
            _engine.AddRule(rule);
            _events.Publish("$event1");
            _events.Publish("$event3");
            _events.Publish("$event2");
            Assert.AreEqual("$rule1", recipient.Message);
            _engine.RemoveRule(rule);
            _events.Unsubscribe(recipient);
        }

        [Test]
        public void Rule2Test1()
        {
            var recipient = new Recipient();
            _events.Subscribe(recipient);
            var rule = new Rule2();
            _engine.AddRule(rule);
            _events.Publish("$event4");
            Assert.AreEqual("$rule2", recipient.Message);
            _engine.RemoveRule(rule);
            _events.Unsubscribe(recipient);
        }

        [Test]
        public void Rule2Test2()
        {
            var recipient = new Recipient();
            _events.Subscribe(recipient);
            var rule = new Rule2();
            _engine.AddRule(rule);
            _events.Publish("$event5");
            Assert.AreEqual("$rule2", recipient.Message);
            _engine.RemoveRule(rule);
            _events.Unsubscribe(recipient);
        }

        [Test]
        public void Rule3Test()
        {
            var recipient = new Recipient();
            _events.Subscribe(recipient);
            var rule1 = new Rule1();
            var rule2 = new Rule2();
            var rule3 = new Rule3();
            _engine.AddRule(rule1);
            _engine.AddRule(rule2);
            _engine.AddRule(rule3);
            _events.Publish("$event1");
            _events.Publish("$event3");
            _events.Publish("$event2");
            _events.Publish("$event4");
            Assert.AreEqual("$rule3", recipient.Message);
            _engine.RemoveRule(rule1);
            _engine.RemoveRule(rule2);
            _engine.RemoveRule(rule3);
            _events.Unsubscribe(recipient);
        }

        internal class FaultyRule1 : Rule<string>
        {
            protected override object DoEvaluate(string message)
            {
                return base.DoEvaluate(message);
            }
        }

        internal class FaultyRule2 : Rule<string>
        {
            protected override object DoEvaluate(string message)
            {
                return "always";
            }
        }

        internal class Recipient : IHandle<string>
        {
            public string Message { get; private set; }

            public void Handle(string message)
            {
                Message = message;
            }
        }
    }
}
