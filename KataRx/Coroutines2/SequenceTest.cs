﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Reactive;
using NUnit.Framework;

namespace KataRx.Coroutines2
{
    [TestFixture]
    public class SequenceTest
    {
        [Test, Explicit]
        public void QueryDiskResult_ReturnWithoutError()
        {
            var observer = Observer.Create<object>(Console.WriteLine);
            new QueryDiskResult(observer);
        }

        [Test] 
        public void Sequence_ReturnResult()
        {
            var sequence = new Sequence();
            sequence.Add((o, r) => new QueryDiskResult(o));
            sequence.Add((o, r) => new QueryDiskResult(o));
            sequence.Add((o, r) => new QueryDiskResult(o));
            sequence.Run(
                ex => Console.WriteLine(ex.Message), 
                result => Console.WriteLine("run end result: {0}", result.Value));
            sequence.Dispose();
        }
    }
}
