﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

namespace KataMachineLearning
{
    [TestFixture]
    public class ActivationTest
    {
        [Test]
        public void SoftStep_WithSeries_PrintOutput()
        {
            const double step = 0.1;
            const double rangeBegin = -1.0;
            const double rangeEnd = 1.0;
            var i = rangeBegin;

            while (i <= rangeEnd)
            {
                Console.WriteLine("{0};{1}", i, Activation.SoftStep(i));
                i += step;
            }
        }
    }
}
