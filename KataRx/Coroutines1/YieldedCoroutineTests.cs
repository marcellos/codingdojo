﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using NUnit.Framework;

namespace KataRx.Coroutines1
{
    [TestFixture]
    public class YieldedCoroutineTests
    {
        private static IEnumerable<Func<string, string>> Workflow()
        {
            var current = "Start";
            Thread.Sleep(500);
            yield return value => value + ":" + current + "->" + (current = "First");
            Thread.Sleep(700);
            yield return value => value + ":" + current + "->" + (current = "Second");
            Thread.Sleep(800);
            yield return value => value + ":" + current + "->" + (current = "Third");
        }


        [Test]
        public void SimpleTest()
        {
            var coroutine = Workflow().AsCoroutine();
            Console.WriteLine(coroutine.Invoke("Invoke 1"));
            Console.WriteLine(coroutine.Invoke("Invoke 2"));
            Console.WriteLine(coroutine.Invoke("Invoke 3"));
            Console.WriteLine("done");
        }

        [Test]
        public void AutoResetTest()
        {
            var current = "Start";
            var coroutine = new Func<string, string>[]
                {
                    value => value + ":" + current + "->" + (current = "First"),
                    value => value + ":" + current + "->" + (current = "Second"),
                    value => value + ":" + current + "->" + (current = "Third")
                }.AsCoroutine(true);

            Console.WriteLine(coroutine.Invoke("Invoke 1"));
            Console.WriteLine(coroutine.Invoke("Invoke 2"));
            Console.WriteLine(coroutine.Invoke("Invoke 3"));
            Console.WriteLine(coroutine.Invoke("Invoke 4"));
            Console.WriteLine(coroutine.Invoke("Invoke 5"));
            Console.WriteLine("done");
        }

        [Test]
        public void ObservableCoroutineTest()
        {
            Func<string, IObservable<string>> query =
                value =>
                from first in Observable.Return(value + ":First")
                from second in Observable.Range(1, 3).Select(i => first  + ":Second=" + i)
                from third in Observable.Return(second + ":Third")
                select third;

            using (query("Start").Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("done")))
            {
                Console.Read();
            }
        }
    }
}
