﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;
using NUnit.Framework;

namespace KataPotter
{
    [TestFixture]
    public class TestFixture
    {
        private ShoppingBasket _basket;
        private IList<Discount> _discounts;
        private GrandTotal _grandTotal;

        [SetUp]
        public void SetUp()
        {
            _basket = new ShoppingBasket();
            _discounts = new List<Discount>();
            _grandTotal = new GrandTotal(_basket, _discounts);
        }

        [TearDown]
        public void TearDown()
        {
            _basket = null;
            _discounts = null;
            _grandTotal = null;
        }

        [Test]
        public void TestItemCompare()
        {
            var item1 = new Item { Name = "book1", Price = 8 };
            var item2 = new Item { Name = "book1", Price = 8 };
            Assert.IsTrue(item1.Equals(item2));
        }

        [Test]
        public void TestItemTotal()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _grandTotal.Calculate();
            Assert.AreEqual(16, _grandTotal.ItemTotal);
        }

        [Test]
        public void TestNoDiscount2Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 2, _grandTotal.ItemTotal);
            Assert.AreEqual(8 * 2, _grandTotal.Total);
        }

        [Test]
        public void TestNoDiscount3Items()
        {
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 3, _grandTotal.ItemTotal);
            Assert.AreEqual(8 * 3, _grandTotal.Total);
        }

        [Test]
        public void TestDiscount2Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 2, _grandTotal.ItemTotal);
            Assert.AreEqual(8 * 2 * 0.95M, _grandTotal.Total);
        }

        [Test]
        public void TestDiscount3Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 3, _grandTotal.ItemTotal);
            Assert.AreEqual(8 * 3 * 0.9M, _grandTotal.Total);
        }

        [Test]
        public void TestDiscount4Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 4, _grandTotal.ItemTotal);
            Assert.AreEqual(8 * 4 * 0.8M, _grandTotal.Total);
        }

        [Test]
        public void TestDiscount5Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            _basket.AddItem(new Item { Name = "book5", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 5, _grandTotal.ItemTotal);
            Assert.AreEqual(8 * 5 * 0.75M, _grandTotal.Total);
        }

        [Test]
        public void Test2Discounts3Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 3, _grandTotal.ItemTotal);
            Assert.AreEqual(8 + (8 * 2 * 0.95M), _grandTotal.Total);
        }

        [Test]
        public void Test2Discounts4Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 4, _grandTotal.ItemTotal);
            Assert.AreEqual(2 * (8 * 2 * 0.95M), _grandTotal.Total);
        }

        [Test]
        public void Test2Discounts6Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 6, _grandTotal.ItemTotal);
            Assert.AreEqual((8 * 4 * 0.8M) + (8 * 2 * 0.95M), _grandTotal.Total);
        }

        [Test]
        public void Test1Discounts6Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            _basket.AddItem(new Item { Name = "book5", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 6, _grandTotal.ItemTotal);
            Assert.AreEqual(8 + (8 * 5 * 0.75M), _grandTotal.Total);
        }

        [Test]
        public void TestEdge2Discounts8Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            _basket.AddItem(new Item { Name = "book5", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 8, _grandTotal.ItemTotal);
            Assert.AreEqual(2 * (8 * 4 * 0.8M), _grandTotal.Total);
        }

        [Test]
        public void TestEdge5Discounts23Items()
        {
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book1", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book2", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book3", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            _basket.AddItem(new Item { Name = "book4", Price = 8 });
            _basket.AddItem(new Item { Name = "book5", Price = 8 });
            _basket.AddItem(new Item { Name = "book5", Price = 8 });
            _basket.AddItem(new Item { Name = "book5", Price = 8 });
            _basket.AddItem(new Item { Name = "book5", Price = 8 });
            var discount = new Discount("5% per unique item");
            discount.Percentage = 0.05M;
            discount.Basket = _basket;
            _discounts.Add(discount);
            _grandTotal.Calculate();
            Assert.AreEqual(8 * 23, _grandTotal.ItemTotal);
            Assert.AreEqual(3 * (8 * 5 * 0.75M) + 2 * (8 * 4 * 0.8M), _grandTotal.Total);
        }
    }
}
