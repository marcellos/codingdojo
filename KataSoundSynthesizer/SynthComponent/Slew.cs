﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataSoundSynthesizer.SynthComponent
{
    class Slew : SynthComponentBase, IAdsrEnvelope
    {
        private const int NotesPerOctave = 12;
        private int _previousNoteNumber;
        private float _slewTime;
        private Direction _direction;
        private float _target;
        private float _a0, _b1;
        private float _output;

        public override int SampleRate { get; set; }
        public float AttackTime { get; set; }
        public float DecayTime { get; set; }
        public float SustainLevel { get; set; }
        public float ReleaseTime { get; set; }
        public float VelocitySensitivity { get; set; }
        public TriggerModeEnum TriggerMode { get; set; }

        public float SlewTime
        {
            get { return _slewTime; }
            set
            {
                if (value < 0f)
                {
                    throw new ArgumentOutOfRangeException("SlewTime", "must be >= 0");
                }

                _slewTime = value;
            }
        }


        public Slew()
        {
            _slewTime = 0.25f;
            _direction = Direction.Up;
            _target = 0f;
        }

        private Slew(IAdsrEnvelope adsrEnvelope)
        {
            SlewTime = adsrEnvelope.SlewTime;
            _direction = Direction.Up;
            _target = 0f;
        }

        public override ISynthComponent MakeInstanceCopy()
        {
            return new Slew(this);
        }

        public void Trigger(int noteNumber, float velocity)
        {
            var s = _slewTime*SampleRate + 1;
            var x = (float) Math.Exp(-1/s);

            _a0 = x - 1;
            _b1 = x;

            _output = (float) (_previousNoteNumber - noteNumber)/NotesPerOctave;

            if (_output < 0)
            {
                _direction = Direction.Up;
                _target = 0.01f;
            }
            else
            {
                _direction = Direction.Down;
                _target = -0.01f;
            }

            _previousNoteNumber = noteNumber;
        }

        public void Release(int noteNumber)
        {
        }


        public override void RenderSamples(int offset, int count)
        {
            InitialiseMonoBuffer(count);

            var o = 0f;

            var buffer = GetMonoBuffer(); 
            var sampleCount = offset + count;
            for (var i = offset; i < sampleCount; ++i)
            {
                o = 0f;

                if (_direction == Direction.Up)
                {
                    if (_output < _target)
                    {
                        o = _a0 * _target + _b1 * _output;
                        _output = o;
                    }
                }
                else
                {
                    if (_output > _target)
                    {
                        o = _a0 * _target + _b1 * _output;
                        _output = o;
                    }
                }

                _monoBuffer[i] += o;
            }
        }
    }

    enum Direction
    {
        Up,
        Down
    }
}
