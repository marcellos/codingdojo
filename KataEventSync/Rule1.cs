﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataEventSync
{
    public class Rule1 : Rule<string>
    {
        bool _event1, _event2, _event3;

        protected override object DoEvaluate(string message)
        {
            _event1 = _event1 || message.Equals("$event1");
            _event2 = _event2 || message.Equals("$event2");
            _event3 = _event3 || message.Equals("$event3");
            if (_event1 && _event2 && _event3)
            {
                _event1 = _event2 = _event3 = false;
                return "$rule1";
            }

            return null;
        }
    }
}
