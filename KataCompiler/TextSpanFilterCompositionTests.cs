﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace KataCompiler
{
    [TestFixture]
    public class TextSpanFilterCompositionTests
    {
        private TextScanner _textScanner;

        [SetUp]
        public void Setup()
        {
            _textScanner = new TextScanner();
        }

        [TearDown]
        public void Teardown()
        {
            _textScanner = null;
        }

        [Test]
        public void TextScanner_ScanScript_AllCommentIsFiltered()
        {
            var textScannerInput = Encoding.Default.GetBytes(Properties.Resources.jquery_extract);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var spans = _textScanner.Scan(textReader);
                var whiteSpaceFilter = new WhitespaceFilter();
                var cStyleCommentFilter = new CStyleCommentFilter(whiteSpaceFilter);
                var endOfLineFilter = new EndOfLineFilter(cStyleCommentFilter);
                var textSpans = endOfLineFilter.Apply(spans);
                Assert.IsNotNull(textSpans);
                Assert.AreEqual(false, textSpans.Any(span =>
                        span.Text.Contains(CStyleCommentFilter.SinglelineCommentMarker)
                        || span.Text.Contains(CStyleCommentFilter.MultilineCommentBeginMarker)
                        || span.Text.Contains(CStyleCommentFilter.MultilineCommentEndMarker)
                        || span.SpanType == SpanTypeEnum.Whitespace
                        || span.SpanType == SpanTypeEnum.EndOfLine));
            }
        }

    }
}
