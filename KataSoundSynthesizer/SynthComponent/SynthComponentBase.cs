﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataSoundSynthesizer.SynthComponent
{
    abstract class SynthComponentBase : ISynthComponent
    {
        protected float[] _monoBuffer;

        public abstract void RenderSamples(int offset, int count);
        public abstract int SampleRate { get; set; }
        public abstract ISynthComponent MakeInstanceCopy();

        protected void InitialiseMonoBuffer(int size)
        {
            if (_monoBuffer == null || _monoBuffer.Length != size)
            {
                _monoBuffer = new float[size];
            }
        }

        protected void SetMonoBuffer(float[] monoBuffer)
        {
            _monoBuffer = monoBuffer;
        }

        public float[] GetMonoBuffer()
        {
            return _monoBuffer;
        }
    }
}
