﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataEventSync
{
    public class User
    {
        public string Name { get; private set; }
        public bool IsOnline { get; private set; }

        public User(string name, bool isOnline = false)
        {
            Name = name;
            IsOnline = isOnline;
        }
    }
}
