﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace KataUSB
{
    public class UsbListener : NativeWindow
    {
        public event Action UsbDeviceChanged;
        private const int WM_DEVICECHANGE = 0x0219;
        private const int DBT_DEVNODES_CHANGED = 0x0007;
        private const int DEVTYP_DEVICEINTERFACE = 0x05;
        private const int DEVICE_NOTIFY_WINDOW_HANDLE = 0;
        private IntPtr _notificationHandle;

        public UsbListener()
        {
            CreateHandle(new CreateParams());
        }

        public void RegisterHID()
        {
            Guid hidGuid;
            HID.HidD_GetHidGuid(out hidGuid);
            _notificationHandle = RegisterDeviceNotifications(Handle, hidGuid);
        }

        public void UnregisterHID()
        {
            UnregisterDeviceNotifications(_notificationHandle);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_DEVICECHANGE && m.WParam.ToInt32() == DBT_DEVNODES_CHANGED)
            {
                OnUsbDeviceChanged();
            }

            base.WndProc(ref m);
        }

        private void OnUsbDeviceChanged()
        {
            if (UsbDeviceChanged != null)
            {
                UsbDeviceChanged();
            }
        }

        private static IntPtr RegisterDeviceNotifications(IntPtr hwnd, Guid classGuid)
        {
            var dbi = new User32.DeviceBroadcastInterface();
            dbi.Size = Marshal.SizeOf(dbi);
            dbi.ClassGuid = classGuid;
            dbi.DeviceType = DEVTYP_DEVICEINTERFACE;
            dbi.Reserved = 0;
            return User32.RegisterDeviceNotification(hwnd, dbi, DEVICE_NOTIFY_WINDOW_HANDLE);
        }

        private static void UnregisterDeviceNotifications(IntPtr handle)
        {
            User32.UnregisterDeviceNotification(handle);
        }
    }
}
