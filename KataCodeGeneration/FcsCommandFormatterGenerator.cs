﻿
/* generated file, do not edit! */

using System.Text;
using System.Globalization;
namespace KataCodeGeneration
{
  class FcsCommandFormatter : IFcsCommandFormatter<byte[]>
  {
    
    public byte[] Format(JumpToApplicationCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.JumpToApplication);
      sb.Append(" APP");

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(JumpToBootloaderCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.JumpToBootloader);
      sb.Append(" BOOT");

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportFirmwareVersionCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportFirmwareVersion);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportMessagePayloadCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportMessagePayload);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ApplicationDataEraseCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ApplicationDataErase);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(StopAliveTimeoutCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.StopAliveTimeout);
      sb.Append(" ");
      sb.Append(cmd.Timeout.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SendDataHexCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SendDataHex);
      sb.Append(" ");
      sb.Append(cmd.Data);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportProductNameCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportProductName);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportSerialNumberCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportSerialNumber);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SetProductNameCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SetProductName);
      sb.Append(" ");
      sb.Append(cmd.Data);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SetSerialNumberCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SetSerialNumber);
      sb.Append(" ");
      sb.Append(cmd.SerialNr.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportFreeOtpLocationsCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportFreeOtpLocations);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportOtpDataCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportOtpData);
      sb.Append(" ");
      sb.Append(cmd.StartId.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.NumData.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SetOtpDataCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SetOtpData);
      sb.Append(" ");
      sb.Append(cmd.StartId.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.Data);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(EnableDeviceFunctionCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.EnableDeviceFunction);
      sb.Append(" ");
      sb.Append(cmd.FunctionId.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportAllDevicesCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportAllDevices);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ConfigureFaststopEventIdCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ConfigureFaststopEventId);
      sb.Append(" ");
      sb.Append(cmd.EventId.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ConfigureMasterNodeIdCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ConfigureMasterNodeId);
      sb.Append(" ");
      sb.Append(cmd.NodeId.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportFaststopEventIdCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportFaststopEventId);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportMasterNodeIdCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportMasterNodeId);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SendEventToNodeCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SendEventToNode);
      sb.Append(" ");
      sb.Append(cmd.TimedEvent);
      sb.Append(",");
      sb.Append(cmd.EventId.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.SendTime.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SetLowlevelInterruptCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SetLowlevelInterrupt);
      sb.Append(" ");
      sb.Append(cmd.ModuleId.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.EventType);
      sb.Append(",");
      sb.Append(cmd.EventId.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SetSerialtunnelConfigurationCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SetSerialtunnelConfiguration);
      sb.Append(" ");
      sb.Append(cmd.ComId.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.BaudRate.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.WordLength.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.StopBits.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.Parity.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SetSerialtunnelOnOffCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SetSerialtunnelOnOff);
      sb.Append(" ");
      sb.Append(cmd.ComId.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.OnOff.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportComPowerCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportComPower);
      sb.Append(" ");
      sb.Append(cmd.ComId.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportCanPowerCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportCanPower);

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SwitchComPowerCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SwitchComPower);
      sb.Append(" ");
      sb.Append(cmd.ComId.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.OnOff.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SwitchCanPowerCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SwitchCanPower);
      sb.Append(" ");
      sb.Append(cmd.OnOff.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(ReportVoltageLimitsCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.ReportVoltageLimits);
      sb.Append(" ");
      sb.Append(cmd.Voltage.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(SetVoltageLimitsCommand cmd)
    {
      var sb = new StringBuilder();
      sb.Append(FcsEncoding.SetVoltageLimits);
      sb.Append(" ");
      sb.Append(cmd.Voltage.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.Min.ToString(CultureInfo.InvariantCulture));
      sb.Append(",");
      sb.Append(cmd.Max.ToString(CultureInfo.InvariantCulture));

      return ConvertStringToBytes(sb.ToString());
    }


    public byte[] Format(FileSystemControlCommand cmd)
    {
      var sb = new StringBuilder();

      return ConvertStringToBytes(sb.ToString());
    }

    private static byte[] ConvertStringToBytes(string message)
    {
      return Encoding.Default.GetBytes(message);
    }
  }
}
