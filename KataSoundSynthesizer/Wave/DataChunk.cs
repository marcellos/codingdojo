﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System.Collections.Generic;
using KataSoundSynthesizer.Riff;

namespace KataSoundSynthesizer.Wave
{
    class DataChunk : Chunkbase
    {
        public byte[] Data { get; set; }

        public DataChunk()
        {
            _fields = new List<ChunkField>
                {
                    new ChunkField
                        {
                            name = "marker",
                            fieldType = ChunkMetadata.ChunkFieldType.AnsiCharacter,
                            size = 4*ChunkMetadata.ChunkFieldTypeSizeMap[ChunkMetadata.ChunkFieldType.AnsiCharacter]
                        },
                    new ChunkField
                        {
                            name = "size",
                            fieldType = ChunkMetadata.ChunkFieldType.LittleEndianUint32,
                            size = ChunkMetadata.ChunkFieldTypeSizeMap[ChunkMetadata.ChunkFieldType.LittleEndianUint32]
                        }
                };

            _valueMap = CreateValueMapFromField(_fields);
        }
    }
}
