﻿/*
 * The MIT License, Copyright (c) 2011-2019 Marcel Schneider
 * for details see License.txt
 */

KataCodeGeneration: Working with generated code
===============================================

In this Kata we are going to write a byte parser. There is a set of 
byte based method calls that ought to be parsed to types, and 
vice versa bytes from instances ought to be generated.

1) We are going to use the built-in T4 templating system of 
Visual Studio. As a data source we can read an XML file.
So the first task is creating a function to load a .tt template 
into the engine, read in the data source and produce some test output.

2) From the data source we need to generate an enum for each literal 
string/bytes that need to be parsed.

3) Rendering methods should be generated with the correct number of arguments. 
The holding class can be versioned, e.g. numbered type.

4) A public API should be generated.

5) Test code should be generated.