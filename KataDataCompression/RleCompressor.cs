﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using System.IO;

namespace KataDataCompression
{
    class RleCompressor : ICompressor
    {
        private readonly byte[] _dataBuffer = new byte[1];
        private readonly byte[] _encodingBuffer = new byte[2];
        private byte _value;
        private byte _count;
        

        public void Compress(Stream input, Stream output)
        {
            _value = 0;
            _count = 1;
            int read = 0;

            read = input.Read(_dataBuffer, 0, 1);
            _value = _dataBuffer[0];

            while ((read = input.Read(_dataBuffer, 0, _dataBuffer.Length)) > 0)
            {
                if (_dataBuffer[0] == _value)
                {
                    _count++;

                    if (_count == 0) // use byte overflow
                    {
                        _encodingBuffer[0] = byte.MaxValue;
                        _encodingBuffer[1] = _value;
                        output.Write(_encodingBuffer, 0, _encodingBuffer.Length);
                        _count = 1;
                    }
                }
                else
                {
                    _encodingBuffer[0] = _count;
                    _encodingBuffer[1] = _value;
                    output.Write(_encodingBuffer, 0, _encodingBuffer.Length);
                    _count = 1;
                }

                _value = _dataBuffer[0];
            }

            _encodingBuffer[0] = _count;
            _encodingBuffer[1] = _value;
            output.Write(_encodingBuffer, 0, _encodingBuffer.Length);
        }

        public void Decompress(Stream input, Stream output)
        {
            int read = 0;
            while((read = input.Read(_encodingBuffer, 0, _encodingBuffer.Length)) > 0)
            {
                _count = _encodingBuffer[0];
                _value = _encodingBuffer[1];

                for(var i = 0; i < _count; i++)
                {
                    output.WriteByte(_value);
                }
            }
        }
    }
}
