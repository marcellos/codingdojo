﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataRx.Coroutines3
{
    class SequentialContinuationResult : IContinuationResult
    {
        private readonly IEnumerator<IContinuationResult> _enumerator;
        private ContinuationExecutionContext _context;

        public SequentialContinuationResult(IEnumerator<IContinuationResult> enumerator )
        {
            _enumerator = enumerator;
        }

        public void Execute(ContinuationExecutionContext context)
        {
            _context = context;
            ChildCompleted(null, new ContinuationResultCompletionEventArgs());
        }

        public event EventHandler<ContinuationResultCompletionEventArgs> Completed = delegate {};
        public void SetContextCallback(Action<ContinuationExecutionContext> callback)
        {
            throw new NotImplementedException();
        }

        void ChildCompleted(object sender, ContinuationResultCompletionEventArgs eventArgs)
        {
            var previous = sender as IContinuationResult;
            if (previous != null)
            {
                previous.Completed -= ChildCompleted;
            }

            if (eventArgs.Error != null || eventArgs.WasCancelled)
            {
                OnComplete(eventArgs.Error, eventArgs.WasCancelled);
                return;
            }

            var moveNextSucceded = false;
            try
            {
                moveNextSucceded = _enumerator.MoveNext();
            }
            catch (Exception ex)
            {
                OnComplete(ex, false);
                return;
            }

            if (moveNextSucceded)
            {
                try
                {
                    var next = _enumerator.Current;
                    next.Completed += ChildCompleted;
                    next.Execute(_context);
                }
                catch (Exception ex)
                {
                    OnComplete(ex, false);
                    return;
                }
            }
            else
            {
                OnComplete(null, false);
            }
        }

        void OnComplete(Exception error, bool wasCancelled)
        {
            _enumerator.Dispose();
            Completed(this, new ContinuationResultCompletionEventArgs {Error = error, WasCancelled = wasCancelled});
        }
    }
}
