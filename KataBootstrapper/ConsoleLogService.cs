﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataBootstrapper
{
    public class ConsoleLogService : ILogService
    {
        public void Info(string format, params object[] args)
        {
            Console.Write("{0} [CONSOLE LOG] ", "INFO:");
            Console.WriteLine(format, args);
        }
    }
}
