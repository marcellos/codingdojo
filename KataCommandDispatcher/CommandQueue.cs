﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace KataCommandDispatcher
{
    public class CommandQueue<T> : ICommandQueue where T : class, ICommand
    {
        private readonly IDependencyResolver _dependencyResolver;
        private readonly BlockingCollection<CommandBindings> _commandQueue;

        public CommandQueue(IDependencyResolver dependencyResolver)
        {
            if (dependencyResolver == null)
            {
                throw new ArgumentNullException("dependencyResolver");
            }

            _dependencyResolver = dependencyResolver;
            _commandQueue = new BlockingCollection<CommandBindings>(new ConcurrentQueue<CommandBindings>());
        }

        public void Enqueue<TCommand>(TCommand command) where TCommand : class, ICommand
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            // resolve handlers and add to queue
            var handlerInstances = _dependencyResolver.GetAll<ICommandHandler<TCommand>>();
            if (handlerInstances == null || !handlerInstances.Any())
            {
                throw new InvalidOperationException(string.Format("no handlers found for command of type '{0}'",
                                                                  command.GetType().Name));
            }

            var handlers = handlerInstances.Cast<ICommandHandler>().ToList();
            _commandQueue.Add(new CommandBindings(command, handlers));
        }

        public CommandBindings Dequeue(CancellationToken token)
        {
            return _commandQueue.Take(token);
        }

    }
}
