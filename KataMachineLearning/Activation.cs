﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataMachineLearning
{
    static class Activation
    {
        public enum ActivationType
        {
            SoftStep,
            Linear
        }


        public static double SoftStep(double v)
        {
            // input range: 0..1
            return 1.0/(1 + Math.Pow(Math.E, -v));
        }
    }
}
