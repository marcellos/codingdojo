﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System.IO;

namespace KataCompiler.Parser
{
    interface IErrorReporter
    {
        int NumberOfErrors { get; }
        int NumberOfWarnings { get; }
        void AddError(TokenValue token, string message);
        void AddWarning(TokenValue token, string message);
        string Render(TextReader textReader);
    }
}
