﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataSoundSynthesizer
{
    class SweepWaveStream : WaveStreamBase
    {
        private int _sample;
        private int _sampleCountTotal;
        private const float PI2 = (float)(2 * Math.PI);
        private const float amplitude = 0.25f;
        private const float frequency = 440f;
        private const float Sweep = 0.01f;
        private float _sweepTotal;

        public SweepWaveStream(WaveFormat waveFormat)
            : base(waveFormat)
        {
        }

        protected override int Read(float[] samples, int offset, int count)
        {
            var sampleRate = WaveFormat.sampleRate;
            var channels = WaveFormat.channels;
            var index = 0;

            for (var n = 0; n < count/channels; ++n)
            {
                var f = (frequency + _sweepTotal) * PI2;
                var value = (float)(amplitude * Math.Sin((_sample * f) / sampleRate));
                _sample++;

                samples[index + offset] = value;
                if (channels == 2)
                {
                    samples[index + offset + 1] = value;
                }

                if (_sample > sampleRate)
                {
                    _sample = 0;
                }

                _sweepTotal += Sweep;

                index += channels;
            }

            _sampleCountTotal += count;

            var returnValue = 0;
            if (_sampleCountTotal < sampleRate)
            {
                returnValue = count;
            }

            Console.WriteLine("sampleCountTotal: {0}", _sampleCountTotal);
            return returnValue;
        }
    }
}
