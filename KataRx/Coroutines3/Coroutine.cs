﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataRx.Coroutines3
{
    static class Coroutine
    {
        public static Func<IEnumerator<IContinuationResult>, IContinuationResult> CreateParentEnumerator = enumerator => new SequentialContinuationResult(enumerator);

        public static void BeginExecute(IEnumerator<IContinuationResult> coroutine, ContinuationExecutionContext context = null, EventHandler<ContinuationResultCompletionEventArgs> callback = null )
        {
            var enumerator = CreateParentEnumerator(coroutine);

            if (callback != null)
            {
                enumerator.Completed += callback;
            }

            enumerator.Completed += Completed;
            enumerator.Execute(context ?? new ContinuationExecutionContext());
        }

        public static event EventHandler<ContinuationResultCompletionEventArgs> Completed = (s, e) =>
            {
                var continuation = (IContinuationResult) s;
                continuation.Completed -= Completed;
                if (e.Error != null)
                {
                    Console.WriteLine("Coroutine execution error: '{0}'", e.Error.Message);
                }
                else if (e.WasCancelled)
                {
                    Console.WriteLine("Coroutine execution cancelled");
                }
                else
                {
                    Console.WriteLine("Coroutine execution completed");
                }
            };
    }
}
 