﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataRomanNumerals
{
    class Numeral
    {
        public Numeral(string letter, string leftBound, string rightBound)
        {
            Letter = letter;
            LeftBound = leftBound;
            RightBound = rightBound;
        }

        public string Letter { get; private set; }
        public string LeftBound { get; private set; }
        public string RightBound { get; private set; }
    }
}
