﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataMicrokernel
{
    public class Scope
    {
        private readonly Scope _parentScope;
        private readonly IDictionary<Type, object> _instanceCache;

        public Scope(Scope parentScope = null)
        {
            _parentScope = parentScope;
            _instanceCache = new Dictionary<Type, object>();
        }

        public void Add(Type type, object instance)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }

            if (!_instanceCache.ContainsKey(type))
            {
                _instanceCache.Add(type, instance);
            }
        }

        public static object Find(Scope scope, Type type)
        {
            if (scope == null)
            {
                return null;
            }

            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            return scope._instanceCache.ContainsKey(type)
                ? scope._instanceCache[type]
                : Find(scope._parentScope, type);
        }
    }
}
