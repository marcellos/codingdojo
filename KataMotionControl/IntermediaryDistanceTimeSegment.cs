﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataMotionControl
{
    public class IntermediaryDistanceTimeSegment
    {
        public double DistanceInMillimeters { get; private set; }
        public double TimeInSeconds { get; private set; }

        public IntermediaryDistanceTimeSegment(double distanceInMillimeters, double timeInSeconds)
        {
            DistanceInMillimeters = distanceInMillimeters;
            TimeInSeconds = timeInSeconds;
        }

    }
}
