﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NUnit.Framework;

namespace KataCommandDispatcher
{
    [TestFixture]
    public class CommandQueueFixture
    {
        private IDependencyResolver _dependencyResolver;
        private ICommandQueue _commandQueue;

        [SetUp]
        public void Setup()
        {
            _dependencyResolver = new MockResolver();
            _commandQueue = new CommandQueue<ICommand>(_dependencyResolver);
        }

        [TearDown]
        public void Teardown()
        {
            _commandQueue = null;
            _dependencyResolver = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(CommandQueue<ICommand>), _commandQueue);
        }

        [Test]
        public void DependencyResolverNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => new CommandQueue<ICommand>(null));
        }

        [Test]
        public void EnqueueNullTest()
        {
            ConsoleCommand command = null;

            Assert.Throws<ArgumentNullException>(() => _commandQueue.Enqueue(command));
        }

        [Test]
        public void EnqueueDequeueTest()
        {
            var myCommand = new ConsoleCommand("test command");
            _commandQueue.Enqueue(myCommand);
            var cancelToken = new CancellationTokenSource();
            var commandAndHandlersItem = _commandQueue.Dequeue(cancelToken.Token);
            Assert.AreEqual(myCommand, commandAndHandlersItem.Command);
            Assert.AreEqual(1, commandAndHandlersItem.HandlerInstances.Count());
        }

        [Test]
        public void NoHandlerFoundTest()
        {
            Assert.Throws<ArgumentException>(() => _commandQueue.Enqueue(new TestCommand()));
        }

        [Test]
        public void EmptyResolverTest()
        {
            var q = new CommandQueue<ICommand>(new EmptyResolver());

            Assert.Throws<InvalidOperationException>(() => q.Enqueue(new TestCommand()));
        }

        [Test]
        public void NullResolverTest()
        {
            var q = new CommandQueue<ICommand>(new NullResolver());

            Assert.Throws<InvalidOperationException>(() => q.Enqueue(new TestCommand()));
        }

        [Test]
        public void BadResolverTest()
        {
            var q = new CommandQueue<ICommand>(new BadResolver());

            Assert.Throws<NotImplementedException>(() => q.Enqueue(new TestCommand()));
        }

        private class MockResolver : IDependencyResolver
        {
            public IEnumerable<object> GetAll<T>()
            {
                return new List<object>() { new ConsoleCommandHandler() };
            }
        }

        private class EmptyResolver : IDependencyResolver
        {
            public IEnumerable<object> GetAll<T>()
            {
                return new List<object>();
            }
        }

        private class NullResolver : IDependencyResolver
        {
            public IEnumerable<object> GetAll<T>()
            {
                return null;
            }
        }

        private class BadResolver : IDependencyResolver
        {
            public IEnumerable<object> GetAll<T>()
            {
                throw new NotImplementedException();
            }
        }

        private class TestCommand : Command
        {
        }
    }
}
