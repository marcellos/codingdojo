﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using KataSoundSynthesizer.SynthComponent;
using KataSoundSynthesizer.Tone;

namespace KataSoundSynthesizer.Oscillators
{
    class WaveFormOscillator : SynthComponentBase, IOscillator
    {
        private readonly float[] _waveForm;
        private readonly IOscillator _fmModulator;
        private readonly Slew _slew;
        private float _accu;

        public WaveFormOscillator(float[] waveForm, IOscillator fmModulator, Slew slew)
        {
            if (waveForm == null)
            {
                throw new ArgumentNullException("waveForm");
            }

            if (fmModulator == null)
            {
                throw new ArgumentNullException("fmModulator");
            }

            if (slew == null)
            {
                throw new ArgumentNullException("slew");
            }

            _waveForm = waveForm;
            _fmModulator = fmModulator;
            _slew = slew;
            Multiplier = OctaveMultiplier.x1;
        }

        public WaveFormOscillator(WaveFormOscillator osc)
        {
            _waveForm = osc._waveForm;
            _fmModulator = osc._fmModulator;
            _slew = osc._slew;

            SampleRate = osc.SampleRate;
            WaveForm = osc.WaveForm;
            Amplitude = osc.Amplitude;
            Frequency = osc.Frequency;
            Phase = osc.Phase;
            DutyCycle = osc.DutyCycle;
            CutOff = osc.CutOff;
            Detune = osc.Detune;
            FmLevel = osc.FmLevel;
            Multiplier = osc.Multiplier;
        }

        public override int SampleRate { get; set; }
        public override ISynthComponent MakeInstanceCopy()
        {
            return new WaveFormOscillator(this);
        }

        public WaveFormEnum WaveForm { get; set; }
        public float Amplitude { get; set; }
        public float Frequency { get; set; }
        public float Phase { get; set; }
        public float DutyCycle { get; set; }
        public float CutOff { get; set; }
        public float Detune { get; set; }
        public float FmLevel { get; set; }
        public OctaveMultiplier Multiplier { get; set; }

        public override void RenderSamples(int offset, int count)
        {
            InitialiseMonoBuffer(count);
            
            _fmModulator.RenderSamples(offset, count);
            _slew.RenderSamples(offset, count);
            var fmBuffer = _fmModulator.GetMonoBuffer();
            var slewBuffer = _slew.GetMonoBuffer();
            var modulatorNote = 0f;
            var frequency = 0f;

            var sampleCount = offset + count;
            for (var i = offset; i < sampleCount; ++i)
            {
                modulatorNote = 12 * fmBuffer[i] * FmLevel + Frequency * (int)Multiplier;
                modulatorNote += 12*slewBuffer[i];

                if (modulatorNote < 0)
                {
                    modulatorNote = 0;
                }
                else if (modulatorNote >= Scale.MaxToneIndex)
                {
                    modulatorNote = Scale.MaxToneIndex - 1;
                }

                frequency = (float) (PowerOfTwoTable.GetPower((modulatorNote - Scale.A440ToneIndex)/12)*Scale.A440);
                _accu += frequency*_waveForm.Length/SampleRate;

                if (_accu >= _waveForm.Length)
                {
                    _accu -= _waveForm.Length;
                }

                _monoBuffer[i] = _waveForm[(int) _accu]*Amplitude*Phase;
            }
        }
    }
}
