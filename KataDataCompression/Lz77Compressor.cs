﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using System.IO;

namespace KataDataCompression
{
    class Lz77Compressor : ICompressor
    {
        private const byte BufferSize = byte.MaxValue;
        private readonly byte[] _ringbufferWindow = new byte[BufferSize];
        private byte _windowPointer;
        private bool _isRingbufferOverflow;
        private readonly byte[] _bufferLookahead = new byte[BufferSize];
        private byte _lookaheadPointer;

        private readonly byte[] _dataBuffer = new byte[1];
        private readonly byte[] _encodingBuffer = new byte[3];

        public void Compress(Stream input, Stream output)
        {
            byte value = 0;
            byte position = 0;
            byte matchPosition = 0;
            var hasMatch = false;
            var isEncoding = true;


            if (!TryReadStream(input, out value))
            {
                return;
            }

            AddToLookahead(value);
            while (isEncoding)
            {
                hasMatch = TryFindLongestMatch(out position);
                if (hasMatch)
                {
                    matchPosition = position;
                    while (true)
                    {
                        if (!TryReadStream(input, out value))
                        {
                            isEncoding = false;
                            break;
                        }

                        AddToLookahead(value);
                        hasMatch = TryFindLongestMatch(out position);
                        if (!hasMatch)
                        {
                            var location = (byte)(_windowPointer - matchPosition);
                            var length = (byte)(_lookaheadPointer - 1);
                            EncodePointer(output, location, length);

                            for (var i = 0; i < _lookaheadPointer - 1; i++)
                            {
                                AddToWindow(_bufferLookahead[i]);
                            }

                            var peek = _bufferLookahead[_lookaheadPointer - 1];
                            ResetLookahead();
                            AddToLookahead(peek);

                            break;
                        }

                        matchPosition = position;
                    }
                }
                else
                {
                    EncodeNullPointer(output, value);
                    AddToWindow(value);
                    ResetLookahead();
                    if (!TryReadStream(input, out value))
                    {
                        isEncoding = false;
                    }

                    AddToLookahead(value);
                }
            }

            if (hasMatch)
            {
                var location = (byte)(_windowPointer - matchPosition);
                EncodePointer(output, location, _lookaheadPointer);
            }
        }

        public void Decompress(Stream input, Stream output)
        {
        }

        public void EncodeNullPointer(Stream output, byte value)
        {
            _encodingBuffer[0] = 0;
            _encodingBuffer[1] = 0;
            _encodingBuffer[2] = value;
            output.Write(_encodingBuffer, 0, 3);
        }

        public void EncodePointer(Stream output, byte location, byte length)
        {
            _encodingBuffer[0] = location;
            _encodingBuffer[1] = length;
            output.Write(_encodingBuffer, 0, 2);
        }

        public bool TryReadStream(Stream input, out byte data)
        {
            data = 0;
            var hasRead = false;
            int read = 0;
            read = input.Read(_dataBuffer, 0, 1);
            hasRead = read > 0;

            if (hasRead)
            {
                data = _dataBuffer[0];
            }

            return hasRead;
        }

        public void AddToWindow(byte value)
        {
            _ringbufferWindow[_windowPointer] = value;
            AdvanceWindowPointer();
        }

        public void AdvanceWindowPointer()
        {
            var currentPointer = _windowPointer;
            _windowPointer++;
            _isRingbufferOverflow |= (_windowPointer < currentPointer);
        }

        public void AddToLookahead(byte value)
        {
            _bufferLookahead[_lookaheadPointer] = value;
            AdvanceLookaheadPointer();
        }

        public void AdvanceLookaheadPointer()
        {
            _lookaheadPointer++;
        }

        public void ResetLookahead()
        {
            _lookaheadPointer = 0;
        }

        public bool TryFindLongestMatch(out byte position)
        {
            position = 0;
            bool result = false;
            byte bufferCount = 0;
            byte matchPosition = 0;
            byte ringbufferWindowLength = _isRingbufferOverflow
                ? BufferSize
                : _windowPointer;
            byte bufferPosition = ringbufferWindowLength;
            bufferPosition--;

            // return if lookahead pointer is not set
            if (_lookaheadPointer == 0)
            {
                return result;
            }

            // 1. scan buffer for bufferLookahead[0] backwards
            // 2. hold buffer position, advance buffer and bufferLookahead by one
            // 3. if the there is a whole match then return the buffer position
            // 4. if there is no match advance buffer position by one and repeat from step 1

            while (bufferCount < ringbufferWindowLength
                && !_isRingbufferOverflow)
            {
                // scan
                while (bufferPosition < BufferSize 
                    && _ringbufferWindow[bufferPosition] != _bufferLookahead[0])
                {
                    bufferPosition--;
                }

                // abort if the scan did not find anything
                if (bufferPosition == BufferSize)
                {
                    break;
                }

                // hold buffer position
                position = bufferPosition;
                while (_ringbufferWindow[bufferPosition] == _bufferLookahead[matchPosition]
                    && matchPosition < _lookaheadPointer) 
                {
                    bufferPosition++;
                    matchPosition++;
                }

                // did the whole match pass?
                if (matchPosition == _lookaheadPointer)
                {
                    result = true;
                    break;
                }
                else
                {
                    matchPosition = 0;
                    bufferPosition = position;
                    bufferPosition--;
                }

                bufferCount++;
            }

            return result;
        }
    }
}
