﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;
using System.Linq;

namespace KataEventSync
{
    public class RulesEngine<T> : IHandle<T>
    {
        private readonly IEventAggregator _events;
        private readonly List<Rule<T>> _rules = new List<Rule<T>>();
        private int _recursionLevel;
        private int _handlingLevel;

        public RulesEngine(IEventAggregator events)
        {
            _events = events;
        }

        public void AddRule(Rule<T> rule)
        {
            lock (_rules)
            {
                if (!_rules.Contains(rule))
                {
                    _events.Unsubscribe(this);
                    _rules.Add(rule);
                    _recursionLevel++;
                    _events.Subscribe(this);
                }
            }
        }

        public void RemoveRule(Rule<T> rule)
        {
            lock (_rules)
            {
                if (_rules.Any() && _rules.Contains(rule))
                {
                    _events.Unsubscribe(this);
                    _rules.Remove(rule);
                    _recursionLevel--;
                    if (_rules.Any())
                    {
                        _events.Subscribe(this);
                    }
                }
            }
        }

        public void Handle(T message)
        {
            lock (_rules)
            {
                // evaluate rules and publish their responses
                var responses = _rules.Select(r => r.Evaluate(message))
                    .Where(response => response != null)
                    .ToList();

                if (_handlingLevel >= _recursionLevel)
                {
                    return;
                }

                foreach (var response in responses)
                {
                    _handlingLevel++;
                    _events.Publish(response);
                    _handlingLevel--;
                }
            }
        }
    }
}
