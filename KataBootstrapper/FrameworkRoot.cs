﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataBootstrapper
{
    public class FrameworkRoot : IFrameworkStart
    {
        readonly ILogService _logService;

        public FrameworkRoot(ILogService logService)
        {
            if (logService == null)
            {
                throw new ArgumentNullException("logService");
            }

            _logService = logService;
        }

        public void Start()
        {
            _logService.Info("framework booted successfully");
        }

        public void Stop()
        {
            _logService.Info("framework shutdown successfully");
        }
    }
}
