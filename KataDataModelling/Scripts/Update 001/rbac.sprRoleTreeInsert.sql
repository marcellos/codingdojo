IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rbac].[sprRoleTreeInsert]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [rbac].[sprRoleTreeInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		CodingDojo
-- Description:	Insert new node into RoleTree
-- =============================================
CREATE PROCEDURE [rbac].[sprRoleTreeInsert]
(
	@roleId INT,
	@parentRoleTreeId INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY
	BEGIN TRANSACTION roleTreeInsert

	DECLARE 
		@nodeRight INT,
		@level INT

	SELECT @nodeRight=rt.[Right], @level = rt.[Level]
	FROM rbac.RoleTree rt
	WHERE rt.RoleTreeId = @parentRoleTreeId 

	UPDATE rbac.RoleTree 
	SET [Left] = [Left] + 2
	WHERE [Left] >= @nodeRight

	UPDATE rbac.RoleTree 
	SET [Right] = [Right] + 2
	WHERE [Right] >= @nodeRight

	INSERT INTO rbac.RoleTree ([Left], [Right], [Level], RoleId)
	VALUES (@nodeRight, @nodeRight + 1, @level + 1, @roleId)	

	COMMIT TRANSACTION roleTreeInsert
END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION roleTreeInsert

END CATCH

END

GO


