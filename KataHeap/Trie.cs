﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataHeap
{
    public class Trie<T>
    {
        private readonly TrieNode<T> _root;

        public Trie()
        {
            _root = new TrieNode<T>('\0');
        }

        public void Add(string word, T value)
        {
            if (word == null) throw new ArgumentNullException("word");

            var node = _root;
            foreach (var c in word)
            {
                var newNode = node.AddChildNode(c);
                node = newNode;
            }
            node.Value = value;
        }

        public T GetValue(string word)
        {
            var node = _root;
            foreach (var c in word)
            {
                if (!node.ContainsChildNode(c))
                {
                    return default(T);
                }

                var child = node.GetChildNode(c);
                node = child;
            }

            // if the value equals default(T) we can return 
            // a list of all leaves for this prefix
            // from child nodes -> IEnumerable<T>
            return node.Value;
        }

        public IEnumerable<T> GetPartialValue(string word)
        {
            var node = _root;
            foreach (var c in word)
            {
                if (!node.ContainsChildNode(c))
                {
                    return new[] { default(T) };
                }

                var child = node.GetChildNode(c);
                node = child;
            }

            var results = new List<T>();
            if (node.Value.Equals(default(T)))
            {
                TraverseDepthFirst(node, n =>
                    {
                        if (!n.Value.Equals(default(T)))
                        {
                            results.Add(n.Value);
                        }
                    });
            }
            else
            {
                results.Add(node.Value);
            }

            return results;
        }

        private static void TraverseDepthFirst(TrieNode<T> node, Action<TrieNode<T>> action)
        {
            if (node == null) return;
            action(node);
            if (node.Children == null) return;
            foreach (var child in node.Children)
            {
                TraverseDepthFirst(child, action);
            }
        }
    }
}
