﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace KataCompiler
{
    [TestFixture]
    public class TokenizerTests
    {
        private TextScanner _textScanner;
        private ITextSpanFilter _textSpanFilter;

        [SetUp]
        public void Setup()
        {
            _textScanner = new TextScanner();
            var whiteSpaceFilter = new WhitespaceFilter();
            var cStyleCommentFilter = new CStyleCommentFilter(whiteSpaceFilter);
            _textSpanFilter = new EndOfLineFilter(cStyleCommentFilter);
        }

        [TearDown]
        public void Teardown()
        {
            _textScanner = null;
            _textSpanFilter = null;
        }

        [Test]
        public void TextScanner_ScanScript_TokenizerReturnsTokens()
        {
            var textScannerInput = Encoding.Default.GetBytes(Properties.Resources.jquery_extract);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var spans = _textScanner.Scan(textReader);
                var textSpans = _textSpanFilter.Apply(spans);
                var tokenizer = new Tokenizer();
                var tokenValues = tokenizer.Tokenize(textSpans);
                Assert.IsNotNull(tokenValues);
                foreach (var tokenValue in tokenValues)
                {
                    Console.WriteLine(FormatTokenValue(tokenValue));
                }
            }
        }

        [Test]
        public void TextScanner_ScanNumber_TokenizerReturnsNumber()
        {
            var src = "var a=1.5e-3+2; " + Environment.NewLine + "return a;";
            var textScannerInput = Encoding.Default.GetBytes(src);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var spans = _textScanner.Scan(textReader);
                var textSpans = _textSpanFilter.Apply(spans);
                var tokenizer = new Tokenizer();
                var tokenValues = tokenizer.Tokenize(textSpans);
                Assert.IsNotNull(tokenValues);
                foreach (var tokenValue in tokenValues)
                {
                    Console.WriteLine(FormatTokenValue(tokenValue));
                }
            }
        }

        [Test]
        public void TextScanner_ScanNumberHex_TokenizerReturnsNumber()
        {
            const string src = "var a=0xff+2;";
            var textScannerInput = Encoding.Default.GetBytes(src);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var spans = _textScanner.Scan(textReader);
                var textSpans = _textSpanFilter.Apply(spans);
                var tokenizer = new Tokenizer();
                var tokenValues = tokenizer.Tokenize(textSpans);
                Assert.IsNotNull(tokenValues);
                foreach (var tokenValue in tokenValues)
                {
                    Console.WriteLine(FormatTokenValue(tokenValue));
                }
            }
        }

        [Test]
        public void TextScanner_ScanStringLiteral_TokenizerReturnsLiteral()
        {
            const string src = "var txt=\"my string with \\\"quotes.\\\"\";";
            var textScannerInput = Encoding.Default.GetBytes(src);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var spans = _textScanner.Scan(textReader);
                var textSpans = _textSpanFilter.Apply(spans);
                var tokenizer = new Tokenizer();
                var tokenValues = tokenizer.Tokenize(textSpans);
                Assert.IsNotNull(tokenValues);
                foreach (var tokenValue in tokenValues)
                {
                    Console.WriteLine(FormatTokenValue(tokenValue));
                }
            }
        }

        private static string FormatTokenValue(TokenValue tokenValue)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("tok:'{0}' ", tokenValue.TokenId);
            sb.AppendFormat("src:'{0}' ", tokenValue.Span.Text);

            if (!string.IsNullOrEmpty(tokenValue.Literal))
            {
                sb.AppendFormat("lit:'{0}' ", tokenValue.Literal);
            }

            if (!string.IsNullOrEmpty(tokenValue.Message))
            {
                sb.AppendFormat("msg:'{0}'", tokenValue.Message);
            }

            return sb.ToString();
        }
    }
}
