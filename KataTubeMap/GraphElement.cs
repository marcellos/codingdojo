﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataTubeMap
{
    public abstract class GraphElement<TNode, TEdge>
    {
        protected IGraph<TNode, TEdge> _graph;

        protected internal IGraph<TNode, TEdge> Graph
        {
            get
            {
                if (_graph == null)
                {
                    throw new InvalidOperationException("Element not connected to IGraph<TNode, TEdge> instance");
                }

                return _graph;
            }
            set { _graph = value; }
        }
    }
}
