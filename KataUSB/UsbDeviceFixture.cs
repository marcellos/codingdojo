﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataUSB
{
    [TestFixture]
    public class UsbDeviceFixture
    {
        [Test]
        public void EnumerateTest()
        {
            Guid hidGuid;
            HID.HidD_GetHidGuid(out hidGuid);
            foreach (var path in UsbDevice.Enumerate(hidGuid))
            {
                Console.WriteLine(path);
            }
        }

        [Test]
        public void CreateDeviceTest()
        {
            Guid hidGuid;
            HID.HidD_GetHidGuid(out hidGuid);
            var device = UsbDevice.CreateDevice(hidGuid, 0x046d, 0xc214, typeof(SomeDevice));
            Assert.IsInstanceOf(typeof(UsbDevice), device);
        }

        internal class SomeDevice : UsbDevice
        {
            protected override void Initialize(string devicePath)
            {
            }
        }
    }
}
