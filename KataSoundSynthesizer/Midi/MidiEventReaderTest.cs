﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System.Linq;
using NUnit.Framework;

namespace KataSoundSynthesizer.Midi
{
    [TestFixture]
    public class MidiEventReaderTest
    {
        [Test]
        public void ReadVariableLengthEncodedValue_WhenZero_ThenReturnValue()
        {
            var buffer = new byte[] { 0 };
            uint value;
            var read = MidiEventReader.ReadVariableLengthEncodedValue(0, buffer, out value);
            Assert.AreEqual(1, read);
            Assert.AreEqual(0, value);
        }

        [Test]
        public void ReadVariableLengthEncodedValue_When1Byte_ThenReturnValue()
        {
            var buffer = new byte[] { 0x40 };
            uint value;
            var read = MidiEventReader.ReadVariableLengthEncodedValue(0, buffer, out value);
            Assert.AreEqual(1, read);
            Assert.AreEqual(0x40, value);
        }

        [Test]
        public void ReadVariableLengthEncodedValue_When1ByteAgain_ThenReturnValue()
        {
            var buffer = new byte[] { 0x7f };
            uint value;
            var read = MidiEventReader.ReadVariableLengthEncodedValue(0, buffer, out value);
            Assert.AreEqual(1, read);
            Assert.AreEqual(0x7f, value);
        }

        [Test]
        public void ReadVariableLengthEncodedValue_When2Bytes_ThenReturnValue()
        {
            var buffer = new byte[] { 0x81, 0x00 };
            uint value;
            var read = MidiEventReader.ReadVariableLengthEncodedValue(0, buffer, out value);
            Assert.AreEqual(2, read);
            Assert.AreEqual(0x80, value);
        }

        [Test]
        public void ReadVariableLengthEncodedValue_When2BytesAgain_ThenReturnValue()
        {
            var buffer = new byte[] { 0xc0, 0x00 };
            uint value;
            var read = MidiEventReader.ReadVariableLengthEncodedValue(0, buffer, out value);
            Assert.AreEqual(2, read);
            Assert.AreEqual(0x2000, value);
        }

        [Test]
        public void DecodeBuffer_WhenMetaEvent_ThenReturnEvents()
        {
            var buffer = new byte[] {0x00, 0xff, 0x03, 0x07, 0x54, 0x72, 0x61, 0x63, 0x6b, 0x20, 0x31};
            var events = MidiEventReader.DecodeBuffer(buffer);
            Assert.AreEqual(1, events.Count());
        }

        [Test]
        public void DecodeBuffer_WhenMidiEvent_ThenReturnEvents()
        {
            var buffer = new byte[] {0x00, 0x90, 0x30, 0x46};
            var events = MidiEventReader.DecodeBuffer(buffer);
            Assert.AreEqual(1, events.Count());
        }

        [Test]
        public void DecodeBuffer_WhenMetaEventSetTempo_ThenReturnEvents()
        {
            var buffer = new byte[] {0x00, 0xff, 0x51, 0x03, 0x07, 0xa1, 0x20};
            var events = MidiEventReader.DecodeBuffer(buffer);
            Assert.AreEqual(1, events.Count());
        }

        [Test]
        public void DecodeBuffer_WhenMetaEventTimeSignature_ThenReturnEvents()
        {
            var buffer = new byte[] {0x00, 0xff, 0x58, 0x02, 0x04, 0x02};
            var events = MidiEventReader.DecodeBuffer(buffer);
            Assert.AreEqual(1, events.Count());
        }

        [Test]
        public void DecodeBuffer_WhenMetaEventTrackEnd_ThenReturnEvents()
        {
            var buffer = new byte[] {0x00, 0xff, 0x2f, 0x00};
            var events = MidiEventReader.DecodeBuffer(buffer);
            Assert.AreEqual(1, events.Count());
        }
    }
}
