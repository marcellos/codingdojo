﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataUSB
{
    public enum Attack3DeviceEnum
    {
        Button1Pressed,
        Button1Released,
        Button2Pressed,
        Button2Released,
        Button3Pressed,
        Button3Released,
        Button4Pressed,
        Button4Released,
        Button5Pressed,
        Button5Released,
        Button6Pressed,
        Button6Released,
        Button7Pressed,
        Button7Released,
        Button8Pressed,
        Button8Released,
        Button9Pressed,
        Button9Released,
        Button10Pressed,
        Button10Released,
        Button11Pressed,
        Button11Released,
        XYMovement,
        ThrustMovement
    }
}
