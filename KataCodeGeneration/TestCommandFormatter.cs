﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Globalization;
using System.Text;

namespace KataCodeGeneration
{
    class TestCommandFormatter : ITestCommandFormatter<byte[]>
    {
        public byte[] Format(TestCommand cmd)
        {
            var sb = new StringBuilder();
            sb.Append("test-cmd-");
            sb.Append(cmd.Value.ToString(CultureInfo.InvariantCulture));
            return ConvertStringToBytes(sb.ToString());
        }

        private static byte[] ConvertStringToBytes(string message)
        {
            return Encoding.Default.GetBytes(message);
        }
    }
}
