﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataRx.Coroutines3
{
    public interface IContinuationResult
    {
        void Execute(ContinuationExecutionContext context);
        event EventHandler<ContinuationResultCompletionEventArgs> Completed;
        void SetContextCallback(Action<ContinuationExecutionContext> callback);
    }
}
