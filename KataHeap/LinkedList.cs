﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataHeap
{
    public class LinkedList<T>
    {
        private ListNode<T> _root;
        private ListNode<T> _tail;

        public int Count { get; private set; }

        public void Add(ListNode<T> node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }

            if (_root == null && _tail == null)
            {
                _root = node;
                _tail = node;
                Count++;
                return;
            }

            _tail.Next = node;
            _tail = node;
            _tail.Next = null;
            Count++;
        }

        public void InsertAt(ListNode<T> positionNode, ListNode<T> node)
        {
            if (positionNode == null)
            {
                throw new ArgumentNullException("positionNode");
            }
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }

            if (!Contains(positionNode))
            {
                throw new ArgumentException("List does not contain node.", "positionNode");
            }

            if (positionNode == _tail)
            {
                Add(node);
                return;
            }

            var temp = positionNode.Next;
            positionNode.Next = node;
            node.Next = temp;
            Count++;
        }

        public void Remove(ListNode<T> node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }

            if (!Contains(node))
            {
                throw new ArgumentException("List does not contain node.", "node");
            }

            if (_root == _tail)
            {
                _root = null;
                _tail = null;
                Count--;
                return;
            }

            if (_root == node)
            {
                _root = _root.Next;
                Count--;
                return;
            }

            var previousNode = Traverse(_root, n => n.Next == node);
            previousNode.Next = node.Next;
            if (_tail == node)
            {
                _tail = previousNode;
            }
            Count--;
        }

        public IEnumerable<ListNode<T>> Find(T key)
        {
            if (Equals(key, default(T)))
            {
                throw new ArgumentNullException("key");
            }

            var result = new List<ListNode<T>>();
            Traverse(_root, n => { if (n.Key.Equals(key)) result.Add(n); });
            return result;
        }

        private static void Traverse(ListNode<T> node, Action<ListNode<T>> action)
        {
            if (node == null) return;
            action(node);
            Traverse(node.Next, action);
        }

        private static ListNode<T> Traverse(ListNode<T> node, Func<ListNode<T>, bool> func)
        {
            if (node == null) return null;
            return func(node) ? node : Traverse(node.Next, func);
        }

        private bool Contains(ListNode<T> node)
        {
            var containedNode = Traverse(_root, n => n == node);
            return containedNode != null;
        }
    }
}
