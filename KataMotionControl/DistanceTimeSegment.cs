﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataMotionControl
{
    public class DistanceTimeSegment
    {
        public double DistanceInIncrements { get; private set; }
        public double TimeInMilliSeconds { get; private set; }

        public DistanceTimeSegment(double distanceInIncrements, double timeInMilliSeconds)
        {
            DistanceInIncrements = distanceInIncrements;
            TimeInMilliSeconds = timeInMilliSeconds;
        }

    }
}
