﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;

namespace KataSoundSynthesizer.SynthComponent
{
    class AdsrEnvelope : SynthComponentBase, IAdsrEnvelope
    {
        private enum State
        {
            Attack,
            Decay,
            Release,
            Completed
        }

        private const int TimeScaler = 3;
        private const float Ceiling = 0.63212f; // -3dB
        private State _state = State.Completed;
        private float _a0, _b1;
        private float _output;
        private float _amplitude;

        public override int SampleRate { get; set; }
        public float AttackTime { get; set; }
        public float DecayTime { get; set; }
        public float SustainLevel { get; set; }
        public float ReleaseTime { get; set; }
        public float VelocitySensitivity { get; set; }
        public TriggerModeEnum TriggerMode { get; set; }
        public float SlewTime { get; set; }

        public AdsrEnvelope()
        {
            DecayTime = 0.25f;
            ReleaseTime = 0.25f;
            TriggerMode = TriggerModeEnum.Retrigger;
            _output = 1.0f;
            _amplitude = 1.0f;
        }

        private AdsrEnvelope(IAdsrEnvelope adsrEnvelope)
        {
            SampleRate = adsrEnvelope.SampleRate;
            AttackTime = adsrEnvelope.AttackTime;
            DecayTime = adsrEnvelope.DecayTime;
            SustainLevel = adsrEnvelope.SustainLevel;
            ReleaseTime = adsrEnvelope.ReleaseTime;
            VelocitySensitivity = adsrEnvelope.VelocitySensitivity;
        }

        public override ISynthComponent MakeInstanceCopy()
        {
            return new AdsrEnvelope(this);
        }


        public override void RenderSamples(int offset, int count)
        {
            InitialiseMonoBuffer(count);

            var sampleCount = offset + count;
            for (var i = offset; i < sampleCount; ++i)
            {
                switch (_state)
                {
                    case State.Attack:
                        _output = _a0 + _b1 * _output;

                        if (_output >= Ceiling + 1.0f)
                        {
                            var d = DecayTime * TimeScaler * SampleRate + 1.0f;
                            var x = (float)Math.Exp(-1.0 / d);

                            _a0 = 1 - x;
                            _b1 = x;

                            _output = Ceiling + 1;

                            _state = State.Decay;
                        }
                        break;

                    case State.Decay:
                        _output = _a0 * SustainLevel + _b1 * _output;
                        break;

                    case State.Release:
                        _output = _a0 + _b1 * _output;

                        if (_output < 0.0f)
                        {
                            _output = 1.0f;
                            _state = State.Completed;
                        }
                        break;

                    case State.Completed:
                        break;

                    default:
                        break;
                }

                _monoBuffer[i] = (_output - 1.0f) / Ceiling * _amplitude; 
            }
        }

        public void Trigger(int noteNumber, float velocity)
        {
            if (TriggerMode == TriggerModeEnum.Legato
                && (_state == State.Attack || _state == State.Decay))
            {
                return;
            }

            //var d = AttackTime*TimeScaler*_sampleRate + 1.0f;
            var x = (float) Math.Exp(-1.0/(AttackTime*TimeScaler*SampleRate));

            _a0 = (1.0f - x)*2.0f;
            _b1 = x;

            _amplitude = (float) Math.Pow((1.0f - VelocitySensitivity) + velocity*VelocitySensitivity, 2);

            _state = State.Attack;
        }

        public void Release(int noteNumber)
        {
            if (_state == State.Release || _state == State.Completed)
            {
                return;
            }

            var d = ReleaseTime*TimeScaler*SampleRate + 1.0f;
            var x = (float) Math.Exp(-1.0/d);

            _a0 = (1.0f - x)*0.9f;
            _b1 = x;

            _state = State.Release;
        }
    }

    public enum TriggerModeEnum
    {
        Retrigger,
        Legato
    }
}
