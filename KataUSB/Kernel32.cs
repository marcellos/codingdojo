﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace KataUSB
{
    public static class Kernel32
    {
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;
        public const uint FILE_FLAG_OVERLAPPED = 0x40000000;
        public const uint OPEN_EXISTING = 3;

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern SafeFileHandle CreateFile(
            [MarshalAs(UnmanagedType.LPStr)] string name,
            uint access,
            uint shareMode,
            IntPtr security,
            uint creationFlags,
            uint attributes,
            IntPtr template);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern int CloseHandle(
            SafeFileHandle handle);
    }
}
