﻿/* generated file, do not edit! */

namespace KataCodeGeneration
{
  public interface IFcsCommandFormatter<out T>
  {
    T Format(JumpToApplicationCommand cmd);
    T Format(JumpToBootloaderCommand cmd);
    T Format(ReportFirmwareVersionCommand cmd);
    T Format(ReportMessagePayloadCommand cmd);
    T Format(ApplicationDataEraseCommand cmd);
    T Format(StopAliveTimeoutCommand cmd);
    T Format(SendDataHexCommand cmd);
    T Format(ReportProductNameCommand cmd);
    T Format(ReportSerialNumberCommand cmd);
    T Format(SetProductNameCommand cmd);
    T Format(SetSerialNumberCommand cmd);
    T Format(ReportFreeOtpLocationsCommand cmd);
    T Format(ReportOtpDataCommand cmd);
    T Format(SetOtpDataCommand cmd);
    T Format(EnableDeviceFunctionCommand cmd);
    T Format(ReportAllDevicesCommand cmd);
    T Format(ConfigureFaststopEventIdCommand cmd);
    T Format(ConfigureMasterNodeIdCommand cmd);
    T Format(ReportFaststopEventIdCommand cmd);
    T Format(ReportMasterNodeIdCommand cmd);
    T Format(SendEventToNodeCommand cmd);
    T Format(SetLowlevelInterruptCommand cmd);
    T Format(SetSerialtunnelConfigurationCommand cmd);
    T Format(SetSerialtunnelOnOffCommand cmd);
    T Format(ReportComPowerCommand cmd);
    T Format(ReportCanPowerCommand cmd);
    T Format(SwitchComPowerCommand cmd);
    T Format(SwitchCanPowerCommand cmd);
    T Format(ReportVoltageLimitsCommand cmd);
    T Format(SetVoltageLimitsCommand cmd);
    T Format(FileSystemControlCommand cmd);
  }
}
