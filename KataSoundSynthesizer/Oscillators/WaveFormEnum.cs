﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

namespace KataSoundSynthesizer.Oscillators
{
    enum WaveFormEnum
    {
        Sine,
        Square,
        Triangle,
        Sawtooth,
        Noise
    }
}
