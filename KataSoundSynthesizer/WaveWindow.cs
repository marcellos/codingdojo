﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Runtime.InteropServices;

namespace KataSoundSynthesizer
{
    class WaveWindowNative : System.Windows.Forms.NativeWindow
    {
        private readonly WaveCallback _waveCallback;

        public WaveWindowNative(WaveCallback waveCallback)
        {
            _waveCallback = waveCallback;
        }

        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            var message = (WaveMessage) m.Msg;
            switch (message)
            {
                case WaveMessage.WaveOutDone:
                case WaveMessage.WaveInData:
                    var hOutputDevice = m.WParam;
                    var waveHeader = new WaveHeader();
                    Marshal.PtrToStructure(m.LParam, waveHeader);
                    _waveCallback(hOutputDevice, message, IntPtr.Zero, waveHeader, IntPtr.Zero);
                    break;
                case WaveMessage.WaveOutOpen:
                case WaveMessage.WaveOutClose:
                case WaveMessage.WaveInOpen:
                case WaveMessage.WaveInClose:
                    _waveCallback(m.WParam, message, IntPtr.Zero, null, IntPtr.Zero);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
    }

    class WaveWindow : System.Windows.Forms.Form
    {
        private readonly WaveCallback _waveCallback;

        public WaveWindow(WaveCallback waveCallback)
        {
            _waveCallback = waveCallback;
        }

        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            var message = (WaveMessage) m.Msg;
            switch (message)
            {
                case WaveMessage.WaveOutDone:
                case WaveMessage.WaveInData:
                    var hOutputDevice = m.WParam;
                    var waveHeader = new WaveHeader();
                    Marshal.PtrToStructure(m.LParam, waveHeader);
                    _waveCallback(hOutputDevice, message, IntPtr.Zero, waveHeader, IntPtr.Zero);
                    break;
                case WaveMessage.WaveOutOpen:
                case WaveMessage.WaveOutClose:
                case WaveMessage.WaveInOpen:
                case WaveMessage.WaveInClose:
                    _waveCallback(m.WParam, message, IntPtr.Zero, null, IntPtr.Zero);
                    break;
                default:
                    base.WndProc(ref m);
                    break;                
            }
        }
    }
}
