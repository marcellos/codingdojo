﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace KataHeap
{
    [TestFixture]
    public class TrieTests
    {
        private Trie<int> _trie;

        [SetUp]
        public void Setup()
        {
            _trie = new Trie<int>();
        }

        [TearDown]
        public void Teardown()
        {
            _trie = null;
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(Trie<int>), _trie);
        }

        [Test]
        public void Add_WithWordIsNull_ThenThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => _trie.Add(null, default(int)));
        }

        [Test]
        public void Add_WithWordAndValue_ThenValueIsStored()
        {
            _trie.Add("cat", 0);
        }

        [Test]
        public void Add_WithTwoWordsAndValues_ThenValuesAreStored()
        {
            _trie.Add("cat", 0);
            _trie.Add("cap", 1);
        }

        [Test]
        public void GetValue_WithTwoWords_ThenReturnCorrectValue()
        {
            _trie.Add("cat", 1);
            _trie.Add("cap", 2);
            Assert.AreEqual(1, _trie.GetValue("cat"));
            Assert.AreEqual(2, _trie.GetValue("cap"));
        }

        [TestCase("cup")]
        [TestCase("car")]
        [TestCase("cats")]
        [TestCase("ca")]
        public void GetValue_WithWordNotContained_ThenReturnDefault(string word)
        {
            _trie.Add("cat", 1);
            _trie.Add("cap", 2);
            Assert.AreEqual(0, _trie.GetValue(word));
        }

        [Test]
        public void GetPartialValue_WithTwoWords_ThenReturnThemBoth()
        {
            _trie.Add("cat", 1);
            _trie.Add("cap", 2);
            var results = _trie.GetPartialValue("ca");
            CollectionAssert.AreEqual(new[] { 1, 2 }, results);
        }

        [Test]
        public void GetPartialValue_WithThreeWords_ThenReturnTwoMatching()
        {
            _trie.Add("car", 1);
            _trie.Add("cup", 2);
            _trie.Add("cupboard", 3);
            var results = _trie.GetPartialValue("cu");
            CollectionAssert.AreEqual(new[] { 2, 3 }, results);
        }

        [Test]
        public void GetPartialValue_WithThreeWords_ThenReturnSingleMatch()
        {
            _trie.Add("car", 1);
            _trie.Add("cup", 2);
            _trie.Add("cupboard", 3);
            var results = _trie.GetPartialValue("cup");
            CollectionAssert.AreEqual(new[] { 2 }, results);
        }

        [Test]
        public void TrieOfIndices_WithRepeatingWords_ThenReturnCorrectIndices()
        {
            var trie = new Trie<IList<int>>();
            const string text = "A test is a test is a test";
            AddingTextToTrie(trie, text);

            var idx_A = trie.GetValue("A");
            CollectionAssert.AreEqual(new[] { 0 }, idx_A);
            var idx_test = trie.GetValue("test");
            CollectionAssert.AreEqual(new[] { 2, 12, 22 }, idx_test);
            var idx_is = trie.GetValue("is");
            CollectionAssert.AreEqual(new[] { 7, 17 }, idx_is);
            var idx_a = trie.GetValue("a");
            CollectionAssert.AreEqual(new[] { 10, 20 }, idx_a);
        }

        private static void AddingTextToTrie(Trie<IList<int>> trie, string text)
        {
            // -- breaking text on white space --
            var word = string.Empty;
            var wordIndex = 0;
            for (var i = 0; i < text.Length; i++)
            {
                var c = text[i];
                if (!Char.IsWhiteSpace(c))
                {
                    word += c;
                    continue;
                }

                if (!string.IsNullOrEmpty(word))
                {
                    AddWordindex(trie, word, wordIndex);
                }

                word = string.Empty;
                wordIndex = i + 1; // point to potential begin of word
            }

            // flushing word buffer
            if (!string.IsNullOrEmpty(word))
            {
                AddWordindex(trie, word, wordIndex);
            }
            // -- breaking text on white space --             
        }

        private static void AddWordindex(Trie<IList<int>> trie, string word, int wordIndex)
        {
            var indices = trie.GetValue(word);
            if (indices != null)
            {
                indices.Add(wordIndex);
            }
            else
            {
                trie.Add(word, new List<int> { wordIndex });
            }
        }

    }
}
