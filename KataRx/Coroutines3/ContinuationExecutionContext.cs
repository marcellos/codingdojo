﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Reflection;

namespace KataRx.Coroutines3
{
    public class ContinuationExecutionContext
    {
        private WeakReference _target;

        public object Target
        {
            get { return _target == null ? null : _target.Target; }
            set { _target = new WeakReference(value); }
        }

        public MethodInfo Method { get; set; }
        public object State { get; set; }
    }
}
