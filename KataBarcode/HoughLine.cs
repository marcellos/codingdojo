﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataBarcode
{
    public class HoughLine : IComparable
    {
        public int Theta { get; private set; }
        public int Radius { get; private set; }
        public int Intensity { get; private set; }

        public HoughLine(int theta, int radius, int intensity)
        {
            Theta = theta;
            Radius = radius;
            Intensity = intensity;
        }

        public int CompareTo(object obj)
        {
            return -Intensity.CompareTo(((HoughLine) obj).Intensity);
        }
    }
}
