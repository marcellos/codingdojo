﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;

namespace KataCarcassonne
{
    class Tile
    {
        private int _directionPointer;
        private readonly Tile[] _neighbours;
        private readonly IList<TileArea> _tileAreas;
        private readonly IDictionary<int, TileArea>[] _areaSideMap;

        public Tile()
        {
            _directionPointer = (int)DirectionEnum.Up;
            _neighbours = new Tile[4];
            _tileAreas = new List<TileArea>();
            _areaSideMap = new SortedList<int, TileArea>[4];
            for (var i = 0; i < 4; i++)
            {
                _areaSideMap[i] = new SortedList<int, TileArea>();
            }
        }

        public Tile Up { get { return _neighbours[0]; } set { _neighbours[0] = value; } }
        public Tile Right { get { return _neighbours[1]; } set { _neighbours[1] = value; } }
        public Tile Down { get { return _neighbours[2]; } set { _neighbours[2] = value; } }
        public Tile Left { get { return _neighbours[3]; } set { _neighbours[3] = value; } }
        public bool IsConnected { get { return Up != null || Right != null || Down != null || Left != null; } }
        public IList<TileArea> TileAreas { get { return _tileAreas; } }
        public IDictionary<int, TileArea> SideUp { get { return _areaSideMap[_directionPointer % 4]; } }
        public IEnumerable<KeyValuePair<int, TileArea>> SideLeft { get { return _areaSideMap[(_directionPointer + 1) % 4]; } }
        public IDictionary<int, TileArea> SideDown { get { return _areaSideMap[(_directionPointer + 2) % 4]; } }
        public IDictionary<int, TileArea> SideRight { get { return _areaSideMap[(_directionPointer + 3) % 4]; } }

        public DirectionEnum Direction { get { return (DirectionEnum)_directionPointer; } }

        public void Rotate(DirectionEnum direction)
        {
            if (IsConnected)
            {
                return;
            }

            _directionPointer = (int)direction;
        }

        public void AddAreas(IEnumerable<KeyValuePair<int, TileArea>> properties, DirectionEnum direction)
        {
            var areas = GetSide(direction);
            foreach (var kvp in properties)
            {
                if (!areas.Contains(kvp))
                {
                    areas.Add(kvp);
                }

                if (!TileAreas.Any(tile => tile.Equals(kvp.Value)))
                {
                    TileAreas.Add(kvp.Value);
                }
            }
        }

        protected IDictionary<int, TileArea> GetSide(DirectionEnum direction)
        {
            var directionNumber = (int)direction;
            // switch left/right
            if (directionNumber % 2 != 0)
            {
                directionNumber = (directionNumber + 2) % 4;
            }

            return _areaSideMap[(_directionPointer + directionNumber) % 4];
        }

        public static void SetNeighbour(Tile a, Tile b, DirectionEnum direction)
        {
            a._neighbours[(int)direction] = b;
            b._neighbours[(int)(direction + 2) % 4] = a;
        }

        public static bool IsNeighbourMatch(Tile a, Tile b, DirectionEnum direction)
        {
            var sideA = a.GetSide(direction);
            var sideB = b.GetSide((DirectionEnum)(((int)direction + 2) % 4));
            var countEqual = sideA.Count() == sideB.Count();
            if (!countEqual)
            {
                return false;
            }

            var index = 0;
            return sideB.Reverse().All(prop => sideA.ElementAt(index++).Value.Equals(prop.Value));
        }

        public static bool IsAreaEndpoint(Tile tile, TileArea prop)
        {
            if (!tile.TileAreas.Any(t => t.Equals(prop)))
            {
                throw new ArgumentException("does not belong to tile", "prop");
            }

            var edgeCount = 0;
            edgeCount += tile.SideUp.Any(t => t.Value.Equals(prop)) ? 1 : 0;
            edgeCount += tile.SideRight.Any(t => t.Value.Equals(prop)) ? 1 : 0;
            edgeCount += tile.SideDown.Any(t => t.Value.Equals(prop)) ? 1 : 0;
            edgeCount += tile.SideLeft.Any(t => t.Value.Equals(prop)) ? 1 : 0;
            return edgeCount == 1;
        }
    }
}
