﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataTubeMap
{
    [TestFixture]
    public class GraphEdgeFixture
    {
        private GraphNode<string, int> _firstNode;
        private GraphNode<string, int> _secondNode;
        private GraphEdge<string, int> _edge;

        [SetUp]
        public void Setup()
        {
            _firstNode = new GraphNode<string, int>("first");
            _secondNode = new GraphNode<string, int>("second");
        }

        [TearDown]
        public void Teardown()
        {
            _edge = null;
            _firstNode = null;
            _secondNode = null;
        }

        [Test]
        public void InstanceTest()
        {
            _edge = new GraphEdge<string, int>(1, _firstNode, _secondNode, EdgeDirectionEnum.OmniDirectional);
            Assert.IsInstanceOf(typeof(GraphEdge<string, int>), _edge);
        }

        [Test]
        public void InstanceFirstNodeNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => 
                _edge = new GraphEdge<string, int>(1, null, _secondNode, EdgeDirectionEnum.OmniDirectional));
        }

        [Test]
        public void InstanceSecondNodeNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => 
                _edge = new GraphEdge<string, int>(1, _firstNode, null, EdgeDirectionEnum.OmniDirectional));
        }
    }
}
