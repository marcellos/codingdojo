﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataRx.Coroutines1
{
    public static class Coroutine
    {
        public static Func<TInput,TOutput> AsCoroutine<TInput, TOutput>(
            this IEnumerable<Func<TInput, TOutput>> workflow)
        {
            return new Coroutine<TInput, TOutput>(workflow).Invoke;
        }

        public static Func<TInput, TOutput> AsCoroutine<TInput, TOutput>(
            this IEnumerable<Func<TInput, TOutput>> workflow, bool autoReset )
        {
            return new Coroutine<TInput, TOutput>(workflow, autoReset).Invoke;
        }

    }

    public sealed class Coroutine<TInput, TOutput>
    {
        public bool Ended { get; private set; }

        private readonly IEnumerator<Func<TInput, TOutput>> _workflow;
        private Func<TInput, TOutput> _current;
        private bool _autoReset;

        public Coroutine(IEnumerable<Func<TInput, TOutput>> workflow) : this (workflow, false)
        {
        }

        public Coroutine(IEnumerable<Func<TInput, TOutput>> workflow, bool autoReset)
        {
            _workflow = workflow.GetEnumerator();
            _autoReset = autoReset;

            MoveNext();
        }

        private void MoveNext()
        {
            if (_workflow.MoveNext())
            {
                _current = _workflow.Current;
                if (_current == null)
                {
                    throw new InvalidOperationException("Corourtine may not yield null continuations");
                }
            }
            else
            {
                _current = null;
                Ended = true;
            }
        }

        public void Reset()
        {
            _workflow.Reset();
            Ended = false;
            MoveNext();
            if (Ended)
            {
                _autoReset = false;
            }
        }

        public TOutput Invoke(TInput value)
        {
            if (Ended)
            {
                if (_autoReset)
                {
                    Reset();
                    if (Ended)
                    {
                        throw new InvalidOperationException("the coroutine is empty");
                    }
                }
                else
                {
                    throw new InvalidOperationException("the coroutine has ended");
                }
            }

            var result = _current(value);
            MoveNext();
            return result;
        }
    }
}
