﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataRx.Coroutines2
{
    public abstract class BaseResult<T> : ISequentialResult
    {
        protected IObserver<ISequentialResult> Observer { get; set; }

        public List<ISequentialResult> Results { get; protected set; }
        public object Value { get; set; }

        protected BaseResult(IObserver<ISequentialResult> observer )
        {
            Observer = observer;
            Results = new List<ISequentialResult>();
        }

        public T TypedValue
        {
            get { return (T)Value; }
            set { Value = value; }
        }
    }
}
