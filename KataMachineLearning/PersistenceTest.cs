﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using NUnit.Framework;

namespace KataMachineLearning
{
    [TestFixture]
    public class PersistenceTest
    {
        private const string FilepathRead = @"examples\persistence.txt";
        private const string FilepathWrite = @"examples\persistence_write.txt";

        private static string CurrentDirectory = TestContext.CurrentContext.TestDirectory;

        [Test]
        public void Load_WhenFileFound_ThenSetupAnn()
        {
            var ann = Persistence.Load(Path.Combine(CurrentDirectory, FilepathRead));
            Assert.IsInstanceOf<ArtificialNeuralNetwork>(ann);
        }

        [Test]
        public void Save_WhenAnn_ThenPersist()
        {
            var layerNodes = new[] {2, 3, 1};
            var w1 = new Matrix(layerNodes[0], layerNodes[1]);
            var w2 = new Matrix(layerNodes[1], layerNodes[2]);
            var w = new[] {w1, w2};
            var ann = new ArtificialNeuralNetwork(layerNodes, w);
            Persistence.Save(ann, FilepathWrite);
            Assert.AreEqual(true, File.Exists(FilepathWrite));
        }
    }
}
