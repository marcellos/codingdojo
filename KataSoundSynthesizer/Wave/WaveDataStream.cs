﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;

namespace KataSoundSynthesizer.Wave
{
    class WaveDataStream : WaveStreamBase
    {
        private int _sampleCountTotal;
        private readonly float[] _waveBuffer;
        private int _waveBufferIndex;

        public WaveDataStream(WaveFormat waveFormat, float[] waveBuffer) 
            : base(waveFormat)
        {
            _waveBuffer = waveBuffer;
        }

        protected override int Read(float[] samples, int offset, int count)
        {
            var channels = WaveFormat.channels;
            var sampleCount = count / channels;
            var index = 0;

            for (var n = 0; n < sampleCount; ++n)
            {
                samples[index + offset] = _waveBuffer[_waveBufferIndex];

                if (channels == 2)
                {
                    samples[index + offset + 1] = _waveBuffer[_waveBufferIndex + 1];
                }

                index += channels;
                _waveBufferIndex += channels;

                if (_waveBufferIndex >= _waveBuffer.Length)
                {
                    _waveBufferIndex = 0;
                }
            }

            _sampleCountTotal += count;

            var returnValue = count;
            if (_sampleCountTotal >= _waveBuffer.Length)
            {
                returnValue = 0;
            }

            return returnValue;
        }
    }
}
