﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NAudio.Wave;

namespace KataSoundSynthesizer
{
    class NAudioSinewaveProvider32 : WaveProvider32
    {
        private int _sample;
        private int _sampleCountTotal;
        private const float _PI2 = (float)(2*Math.PI);
        private float _frequency_PI2;

        public NAudioSinewaveProvider32(float frequency, float amplitude)
        {
            Frequency = frequency;
            Amplitude = amplitude;

            _frequency_PI2 = _PI2*Frequency;
        }

        public float Frequency { get; private set; }
        public float Amplitude { get; private set; }

        public override int Read(float[] buffer, int offset, int sampleCount)
        {
            var sampleRate = WaveFormat.SampleRate;
            var channels = WaveFormat.Channels;
            var index = 0;

            for (var n = 0; n < sampleCount/channels; ++n)
            {
                var value = (float)(Amplitude * Math.Sin((_sample * _frequency_PI2) / sampleRate));
                _sample++;

                buffer[index + offset] = value;

                if (channels == 2)
                {
                    buffer[index + offset + 1] = value;
                }

                if (_sample >= sampleRate)
                {
                    _sample = 0;
                }

                index += channels;
            }

            _sampleCountTotal += sampleCount;

            var returnValue = 0;
            if (_sampleCountTotal < sampleRate)
            {
                returnValue = sampleCount;
            }

            Console.WriteLine("sampleCountTotal: {0}", _sampleCountTotal);
            return returnValue;
        }
    }
}
