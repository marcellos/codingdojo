﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace KataCompiler
{
    [TestFixture]
    public class CStyleCommentFilterTests
    {
        private TextScanner _textScanner;

        [SetUp]
        public void Setup()
        {
            _textScanner = new TextScanner();
        }

        [TearDown]
        public void Teardown()
        {
            _textScanner = null;
        }

        [Test]
        public void TextScanner_ScanScript_CStyleCommentIsFiltered()
        {
            var textScannerInput = Encoding.Default.GetBytes(Properties.Resources.jquery_extract);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var spans = _textScanner.Scan(textReader);
                var cStyleCommentFilter = new CStyleCommentFilter();
                var textSpans = cStyleCommentFilter.Apply(spans);
                Assert.IsNotNull(textSpans);
                Assert.AreEqual(false, textSpans.Any(span =>
                        span.Text.Contains(CStyleCommentFilter.SinglelineCommentMarker)
                        || span.Text.Contains(CStyleCommentFilter.MultilineCommentBeginMarker)
                        || span.Text.Contains(CStyleCommentFilter.MultilineCommentEndMarker)));
                //foreach (var textSpan in textSpans)
                //{
                //    Console.WriteLine("- " + textSpan.Text);
                //}
            }
        }

        [Test]
        public void TextScanner_ScanText_SinglelineCommentIsFiltered()
        {
            var src = "var a;///*comment*/" + Environment.NewLine + "return a;";
            var textScannerInput = Encoding.Default.GetBytes(src);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var spans = _textScanner.Scan(textReader);
                var cStyleCommentFilter = new CStyleCommentFilter();
                var textSpans = cStyleCommentFilter.Apply(spans);
                Assert.IsNotNull(textSpans);
                Assert.AreEqual(7, textSpans.Count());
                //foreach (var textSpan in textSpans)
                //{
                //    Console.WriteLine("- " + textSpan.Text);
                //}
            }

        }

        [Test]
        public void TextScanner_ScanText_MultilineCommentIsFiltered()
        {
            const string src = "var/**//**/a;/**/";
            var textScannerInput = Encoding.Default.GetBytes(src);
            using (var fs = new MemoryStream(textScannerInput))
            {
                var textReader = new StreamReader(fs);
                var spans = _textScanner.Scan(textReader);
                var cStyleCommentFilter = new CStyleCommentFilter();
                var textSpans = cStyleCommentFilter.Apply(spans);
                Assert.IsNotNull(textSpans);
                Assert.AreEqual(2, textSpans.Count());
                //foreach (var textSpan in textSpans)
                //{
                //    Console.WriteLine("- " + textSpan.Text);
                //}
            }
        }

    }
}
