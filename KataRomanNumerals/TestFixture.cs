﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using NUnit.Framework;

namespace KataRomanNumerals
{
    [TestFixture]
    public class TestFixture
    {
        [Test]
        public void TestI()
        {
            Assert.AreEqual("I", RomanConverter.Convert("1"));
        }

        [Test]
        public void TestII()
        {
            Assert.AreEqual("II", RomanConverter.Convert("2"));
        }

        [Test]
        public void TestIII()
        {
            Assert.AreEqual("III", RomanConverter.Convert("3"));
        }

        [Test]
        public void TestIV()
        {
            Assert.AreEqual("IV", RomanConverter.Convert("4"));
        }

        [Test]
        public void TestV()
        {
            Assert.AreEqual("V", RomanConverter.Convert("5"));
        }

        [Test]
        public void TestVI()
        {
            Assert.AreEqual("VI", RomanConverter.Convert("6"));
        }

        [Test]
        public void TestVII()
        {
            Assert.AreEqual("VII", RomanConverter.Convert("7"));
        }

        [Test]
        public void TestVIII()
        {
            Assert.AreEqual("VIII", RomanConverter.Convert("8"));
        }

        [Test]
        public void TestIX()
        {
            Assert.AreEqual("IX", RomanConverter.Convert("9"));
        }

        [Test]
        public void TestX()
        {
            Assert.AreEqual("X", RomanConverter.Convert("10"));
        }

        [Test]
        public void TestL()
        {
            Assert.AreEqual("L", RomanConverter.Convert("50"));
        }

        [Test]
        public void TestC()
        {
            Assert.AreEqual("C", RomanConverter.Convert("100"));
        }

        [Test]
        public void TestD()
        {
            Assert.AreEqual("D", RomanConverter.Convert("500"));
        }

        [Test]
        public void TestM()
        {
            Assert.AreEqual("M", RomanConverter.Convert("1000"));
        }

        [Test]
        public void TestMCMXCIX()
        {
            Assert.AreEqual("MCMXCIX", RomanConverter.Convert("1999"));
        }

        [Test]
        public void TestMMVIII()
        {
            Assert.AreEqual("MMVIII", RomanConverter.Convert("2008"));
        }

        [Test]
        public void TestMMMMMMMMMM()
        {
            Assert.AreEqual("MMMMMMMMMM", RomanConverter.Convert("10000"));
        }

        [Test]
        public void Test1()
        {
            Assert.AreEqual("1", RomanConverter.Convert("I"));
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual("2", RomanConverter.Convert("II"));
        }

        [Test]
        public void Test3()
        {
            Assert.AreEqual("3", RomanConverter.Convert("III"));
        }

        [Test]
        public void Test4()
        {
            Assert.AreEqual("4", RomanConverter.Convert("IV"));
        }

        [Test]
        public void Test5()
        {
            Assert.AreEqual("5", RomanConverter.Convert("V"));
        }

        [Test]
        public void Test6()
        {
            Assert.AreEqual("6", RomanConverter.Convert("VI"));
        }

        [Test]
        public void Test7()
        {
            Assert.AreEqual("7", RomanConverter.Convert("VII"));
        }

        [Test]
        public void Test8()
        {
            Assert.AreEqual("8", RomanConverter.Convert("VIII"));
        }

        [Test]
        public void Test9()
        {
            Assert.AreEqual("9", RomanConverter.Convert("IX"));
        }

        [Test]
        public void Test10()
        {
            Assert.AreEqual("10", RomanConverter.Convert("X"));
        }

        [Test]
        public void Test50()
        {
            Assert.AreEqual("50", RomanConverter.Convert("L"));
        }

        [Test]
        public void Test100()
        {
            Assert.AreEqual("100", RomanConverter.Convert("C"));
        }

        [Test]
        public void Test500()
        {
            Assert.AreEqual("500", RomanConverter.Convert("D"));
        }

        [Test]
        public void Test1000()
        {
            Assert.AreEqual("1000", RomanConverter.Convert("M"));
        }

        [Test]
        public void Test1999()
        {
            Assert.AreEqual("1999", RomanConverter.Convert("MCMXCIX"));
        }

        [Test]
        public void Test2008()
        {
            Assert.AreEqual("2008", RomanConverter.Convert("MMVIII"));
        }

        [Test]
        public void Test19()
        {
            Assert.AreEqual("19", RomanConverter.Convert("XIX"));
        }
    }
}
