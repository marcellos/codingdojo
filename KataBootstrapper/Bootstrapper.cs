﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;

namespace KataBootstrapper
{
    public abstract class Bootstrapper
    {
        static readonly IKernelLog Log = LogManager.GetLog(typeof(Bootstrapper));
        protected static bool _isInitialized;
        protected readonly bool _useAppDomain;

        public AppDomain AppDomain { get; protected set; }

        protected Bootstrapper(bool useAppDomain = true)
        {
            if (_isInitialized)
            {
                return;
            }

            _useAppDomain = useAppDomain;
            _isInitialized = true;
        }

        ~Bootstrapper()
        {
            DoShutdown();
            _isInitialized = false;
        }

        public void Startup()
        {
            DoStartup();
            _isInitialized = true;
        }

        public void Shutdown()
        {
            DoShutdown();
            _isInitialized = false;
        }

        protected virtual void DoStartup()
        {
            Log.Info("bootstrapper startup");
            if (_useAppDomain)
            {
                if (AppDomain == null)
                {
                    AppDomain = AppDomain.CurrentDomain;
                }

                AppDomain.DomainUnload += OnDomainUnload;
                AppDomain.ProcessExit += OnProcessExit;
                AppDomain.UnhandledException += OnUnhandledException;
                AppDomain.FirstChanceException += OnFirstChanceException;
            }

            Configure();

            IoC.GetInstance = IocGetInstance;
            IoC.GetAllInstances = IocGetAllInstances;
            IoC.BuildUp = IocBuildUp;
            IoC.RegisterService = IocRegisterService;
            IoC.RegisterServiceSingleton = IocRegisterServiceSingleton;

            OnStartup();
            Log.Info("bootstrapper startup finished");
        }

        protected virtual void DoShutdown()
        {
            Log.Info("bootstrapper shutdown");
            if (AppDomain != null)
            {
                AppDomain.DomainUnload -= OnDomainUnload;
                AppDomain.ProcessExit -= OnProcessExit;
                AppDomain.UnhandledException -= OnUnhandledException;
                AppDomain.FirstChanceException -= OnFirstChanceException;
            }

            OnShutdown();
            Log.Info("bootstrapper shutdown finished");
        }

        protected virtual void OnDomainUnload(object sender, EventArgs e) { }
        protected virtual void OnProcessExit(object sender, EventArgs e) { }

        protected virtual void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Error(e.ExceptionObject as Exception);
        }

        protected virtual void OnFirstChanceException(object sender, FirstChanceExceptionEventArgs e)
        {
            Log.Error(e.Exception);
        }

        protected abstract void Configure();
        protected virtual void OnStartup() { }
        protected virtual void OnShutdown() { }

        protected virtual object IocGetInstance(Type service, string key)
        {
            return Activator.CreateInstance(service);
        }

        protected virtual IEnumerable<object> IocGetAllInstances(Type service)
        {
            return new[] { Activator.CreateInstance(service) };
        }

        protected virtual void IocBuildUp(object instance) { }
        protected virtual void IocRegisterService(Type interfaceType, Type service) { }
        protected virtual void IocRegisterServiceSingleton(Type interfaceType, Type service) { }

    }

    public abstract class Bootstrapper<T> : Bootstrapper where T : IFrameworkStart
    {
        protected IFrameworkStart _frameworkStart;

        protected override void OnStartup()
        {
            _frameworkStart = IoC.Get<T>();
            if (_frameworkStart != null)
            {
                _frameworkStart.Start();
            }
        }

        protected override void OnShutdown()
        {
            if (_frameworkStart != null)
            {
                _frameworkStart.Stop();
            }
        }
    }
}
