﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System.Collections.Generic;

namespace KataSoundSynthesizer.SynthComponent
{
    class PolyVoice : IVoice
    {
        private const int NumberOfVoices = 16;
        private readonly int[] _slots = new int[NumberOfVoices];
        private readonly IVoice[] _voices = new IVoice[NumberOfVoices];

        public PolyVoice(IVoice voice)
        {
            for (var i = 0; i < NumberOfVoices; ++i)
            {
                _voices[i] = voice.MakeInstanceCopy();
            }
        }


        public void RenderSamples(int offset, int count)
        {
            for(var i = 0; i < NumberOfVoices; ++i)
            {
               _voices[i].RenderSamples(offset, count);
            }
        }

        public float[,] GetStereoBuffer()
        {
            float[,] stereoBuffer = null;
            for (var i = 0; i < NumberOfVoices; ++i)
            {
                var buffer = _voices[i].GetStereoBuffer();
                var length = buffer.Length/2;
                if (stereoBuffer == null)
                {
                    stereoBuffer = new float[2, length];
                }

                for (var j = 0; j < length; ++j)
                {
                    stereoBuffer[0, j] += buffer[0, j];
                    stereoBuffer[1, j] += buffer[1, j];
                }
            }

            return stereoBuffer;
        }

        public float Panning
        {
            get { return _voices[0].Panning; }
            set { _voices[0].Panning = value; }
        }

        public void TriggerKey(TrackedKey key)
        {
            var slot = GetSlot(_slots, 0);
            _slots[slot] = key.NoteNumber;
            //System.Console.WriteLine("#slot={0}, note={1}, dt={2}, v={3}, ch={4}", 
            //    slot, key.NoteNumber, key.DeltaTimeTicks, key.Velocity, key.Channel);

            _voices[slot].TriggerKey(key);
        }

        public void ReleaseKey(TrackedKey key)
        {
            var slot = GetSlot(_slots, key.NoteNumber);
            _slots[slot] = 0;
            //System.Console.WriteLine("reset #slot={0}, dt={1}", slot, key.DeltaTimeTicks);

            _voices[slot].ReleaseKey(key);
        }

        public IVoice MakeInstanceCopy()
        {
            return null;
        }

        private static int GetSlot(IList<int> slots, int note)
        {
            var index = 0;

            for (var i = 0; i < slots.Count; ++i)
            {
                if (slots[i] != note)
                {
                    continue;
                }

                index = i;
                break;
            }

            return index;
        }
    }
}
