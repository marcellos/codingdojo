﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Windows.Forms;
using NUnit.Framework;

namespace KataUSB
{
    [TestFixture]
    public class UsbListenerFixture
    {
        private UsbListener _listener;
        private bool _changeDetected;

        [SetUp]
        public void Setup()
        {
            _listener = new UsbListener();
            _changeDetected = false;
        }

        [TearDown]
        public void Teardown()
        {
            _listener = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(UsbListener), _listener);
        }

        [Test]
        public void UsbDeviceChangedTest()
        {
            _listener.UsbDeviceChanged += RegisterDeviceChange;
            var form = new TestForm();
            form.Listener = _listener;
            Application.Run(form);
            _listener.UsbDeviceChanged -= RegisterDeviceChange;
            Assert.IsTrue(_changeDetected);
        }

        private void RegisterDeviceChange()
        {
            _changeDetected = true;
        }
    }
}
