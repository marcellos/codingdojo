﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using NUnit.Framework;

namespace KataTubeMap
{
    [TestFixture]
    public class GraphNodeFixture
    {
        private GraphNode<string, int> _node;

        [SetUp]
        public void Setup()
        {
            _node = new GraphNode<string, int>("test");
        }

        [TearDown]
        public void Teardown()
        {
            _node = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(GraphNode<string, int>), _node);
        }
    }
}
