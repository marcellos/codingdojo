﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.IO;
using NUnit.Framework;

namespace KataUSB
{
    [TestFixture]
    public class Attack3DeviceFixture
    {
        private Attack3Device _device;
        private int _deviceRecovered;
        private static readonly object _padLock = new object();

        [TearDown]
        public void Teardown()
        {
            _device = null;
        }

        [Test]
        public void InitializeTest()
        {
            var device = UsbDevice.CreateDevice(HidDevice.ClassGuid, Attack3Device.VID, Attack3Device.PID, typeof(Attack3Device)) as Attack3Device;
            Assert.IsInstanceOf(typeof(Attack3Device), device);
            device.Attack3DeviceDataHandler = data =>
            {
                var dataString = data.DataType.ToString();
                if (data.DataType == Attack3DeviceEnum.XYMovement)
                {
                    dataString += string.Format(" x={0} y={1}", data.X.ToString("D3"), data.Y.ToString("D3"));
                }

                if (data.DataType == Attack3DeviceEnum.ThrustMovement)
                {
                    dataString += string.Format(" thrust={0}", data.Thrust.ToString("D3"));
                }

                Console.WriteLine(dataString);
            };

            Console.WriteLine("Attack3 device connected, click buttons during 10s..");
            for (var i = 0; i < 100; i++)
            {
                System.Threading.Thread.Sleep(100);
            }

            var disposable = device as IDisposable;
            disposable.Dispose();
        }

        [Test]
        public void RecoveryTest()
        {
            _device = UsbDevice.CreateDevice(HidDevice.ClassGuid, Attack3Device.VID, Attack3Device.PID, typeof(Attack3Device)) as Attack3Device;
            _device.Attack3DeviceDataHandler = data =>
            {
                var dataString = data.DataType.ToString();
                if (data.DataType == Attack3DeviceEnum.XYMovement)
                {
                    dataString += string.Format(" x={0} y={1}", data.X.ToString("D3"), data.Y.ToString("D3"));
                }

                if (data.DataType == Attack3DeviceEnum.ThrustMovement)
                {
                    dataString += string.Format(" thrust={0}", data.Thrust.ToString("D3"));
                }

                Console.WriteLine(dataString);
            };

            _device.DeviceRemovedHandler = HandleDeviceRemove;
            Console.WriteLine("Attack3 device connected, unplug/re-plug device 2 times..");
            while (_deviceRecovered < 2)
            {
                System.Threading.Thread.Sleep(100);
            }

            var disposable = _device as IDisposable;
            disposable.Dispose();
        }

        private void HandleDeviceRemove(IOException ex)
        {
            if (ex.Message.Contains("not connected"))
            {
                var disposable = _device as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }

                _device = null;
                while (_device == null)
                {
                    _device = UsbDevice.CreateDevice(HidDevice.ClassGuid, Attack3Device.VID, Attack3Device.PID, typeof(Attack3Device)) as Attack3Device;
                    System.Threading.Thread.Sleep(500);
                }

                _device.DeviceRemovedHandler = HandleDeviceRemove;
                lock (_padLock)
                {
                    _deviceRecovered++;
                }
            }
        }
    }
}
