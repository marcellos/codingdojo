USE [Rbac]
GO

BEGIN TRY
	BEGIN TRANSACTION schemaChange

	DECLARE 
		@scriptNumber INT = 1,
		@description NVARCHAR(256) = N'Create tables for role based access control',
		@author NVARCHAR(256) = N'CodingDojo'
	
	IF NOT EXISTS (
		SELECT 1 
		FROM info.SchemaVersion sv
		WHERE sv.ScriptNumber = @scriptNumber - 1
		)
	BEGIN
		RAISERROR('ScriptNumber sequence error', 16, 1)
	END
	
	IF EXISTS (
		SELECT 1
		FROM info.SchemaVersion sv 
		WHERE sv.ScriptNumber = @scriptNumber
		)
	BEGIN
		RAISERROR('Update script installed already', 16, 1)
	END

	IF NOT EXISTS (
		SELECT 1 
		FROM INFORMATION_SCHEMA.SCHEMATA
		WHERE SCHEMA_NAME = 'rbac'	
		)
	BEGIN 
		EXEC sp_executesql N'CREATE SCHEMA [rbac] AUTHORIZATION [dbo]'
	END 
	
	
	CREATE TABLE [rbac].[Operation]
	(
		OperationId INT IDENTITY(1,1) NOT NULL,
		Name NVARCHAR(256) NOT NULL,
		Code INT NOT NULL CONSTRAINT "U_Operation_Code" UNIQUE,
		CONSTRAINT [PK_OperationId] PRIMARY KEY CLUSTERED
		(
			OperationId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	) ON [PRIMARY]
	
	CREATE TABLE [rbac].[Permission]
	(
		PermissionId INT IDENTITY(1,1) NOT NULL,
		Name NVARCHAR(256) NOT NULL,		
		CONSTRAINT [PK_PermissionId] PRIMARY KEY CLUSTERED
		(
			PermissionId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	) ON [PRIMARY]

	CREATE TABLE [rbac].[PermissionOperation]
	(
		PermissionOperationId INT IDENTITY(1,1) NOT NULL,
		PermissionId INT NOT NULL,
		OperationId INT NOT NULL,
		CONSTRAINT [PK_PermissionOperationId] PRIMARY KEY CLUSTERED
		(
			PermissionOperationId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [FK_PermissionOperation_PermissionId] FOREIGN KEY (PermissionId) REFERENCES [rbac].[Permission] (PermissionId),
		CONSTRAINT [FK_PermissionOperation_OperationId] FOREIGN KEY (OperationId) REFERENCES [rbac].[Operation] (OperationId),
	) ON [PRIMARY]
	
	CREATE TABLE [rbac].[Role]
	(
		RoleId INT IDENTITY(1,1) NOT NULL,
		Name NVARCHAR(256) NOT NULL,
		CONSTRAINT [PK_RoleId] PRIMARY KEY CLUSTERED
		(
			RoleId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	) ON [PRIMARY]
	
	CREATE TABLE [rbac].[RoleTree] 
	(
		RoleTreeId INT IDENTITY(1,1) NOT NULL,
		[Left] INT NOT NULL,
		[Right] INT NOT NULL,
		Level INT NOT NULL,
		RoleId INT NOT NULL
		CONSTRAINT [PK_RoleTreeId] PRIMARY KEY CLUSTERED 
		(
			RoleTreeId ASC
		)  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [FK_RoleTree_RoleId] FOREIGN KEY (RoleId) REFERENCES [rbac].[Role] (RoleId),
	) ON [PRIMARY]
	
	CREATE TABLE [rbac].[RolePermission]
	(
		RolePermissionId INT IDENTITY(1,1) NOT NULL,
		RoleId INT NOT NULL,
		PermissionId INT NOT NULL,
		CONSTRAINT [PK_RolePermissionId] PRIMARY KEY CLUSTERED
		(
			RolePermissionId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [FK_RolePermission_RoleId] FOREIGN KEY (RoleId) REFERENCES [rbac].[Role] (RoleId),
		CONSTRAINT [FK_RolePermission_PermissionId] FOREIGN KEY (PermissionId) REFERENCES [rbac].[Permission] (PermissionId),
	) ON [PRIMARY]
		
	CREATE TABLE [rbac].[Subject]
	(
		SubjectId INT IDENTITY(1,1) NOT NULL,
		Salt NVARCHAR(10) NOT NULL CONSTRAINT "U_Subject_Salt" UNIQUE,
		Password NVARCHAR(50) NULL,
		Firstname NVARCHAR(100) NULL,
		Lastname NVARCHAR(100) NULL,
		Email NVARCHAR(100) NULL,
		CONSTRAINT [PK_SubjectId] PRIMARY KEY CLUSTERED
		(
			SubjectId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	) ON [PRIMARY]

	CREATE TABLE [rbac].[RoleSubject]
	(
		RoleSubjectId INT IDENTITY(1,1) NOT NULL,
		RoleId INT NOT NULL,
		SubjectId INT NOT NULL,
		CONSTRAINT [PK_RoleSubjectId] PRIMARY KEY CLUSTERED
		(
			RoleSubjectId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [FK_RoleSubject_RoleId] FOREIGN KEY (RoleId) REFERENCES [rbac].[Role] (RoleId),
		CONSTRAINT [FK_RoleSubject_SubjectId] FOREIGN KEY (SubjectId) REFERENCES [rbac].[Subject] (SubjectId),
	) ON [PRIMARY]

	CREATE TABLE [rbac].[Session]
	(
		SessionId INT IDENTITY(1,1) NOT NULL,
		SubjectId INT NOT NULL,
		LoginDate DATETIME NOT NULL CONSTRAINT "DF_Session_LoginDate" DEFAULT GETDATE(),
		CONSTRAINT [PK_SessionId] PRIMARY KEY CLUSTERED
		(
			SessionId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [FK_Session_SubjectId] FOREIGN KEY (SubjectId) REFERENCES [rbac].[Subject] (SubjectId),
	) ON [PRIMARY]

	CREATE TABLE [rbac].[RoleSession]
	(
		RoleSessionId INT IDENTITY(1,1) NOT NULL,
		RoleId INT NOT NULL,
		SessionId INT NOT NULL,
		CONSTRAINT [PK_RoleSessionId] PRIMARY KEY CLUSTERED
		(
			RoleSessionId ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [FK_RoleSession_RoleId] FOREIGN KEY (RoleId) REFERENCES [rbac].[Role] (RoleId),
		CONSTRAINT [FK_RoleSession_SessionId] FOREIGN KEY (SessionId) REFERENCES [rbac].[Session] (SessionId),
	) ON [PRIMARY]
	
	
	INSERT INTO [info].[SchemaVersion] (ScriptNumber, [Description], Author)
	VALUES
		(@scriptNumber, @description, @author)

	COMMIT TRANSACTION schemaChange
END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION schemaChange
	
END CATCH
GO
