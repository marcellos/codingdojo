﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace KataMotionControl
{
    [TestFixture]
    public class MotionControlCalculationFixture
    {
        private MotionControlCalculator _motionControlCalculator;

        [SetUp]
        public void Setup()
        {
            var settings = new DriveSettings();
            settings.IncrementPerMillimeterResolution = 13.888888888889;
            settings.SpeedMaximumInIncrementsPerMilliSecond = 9300;
            settings.SpeedMinimumInIncrementsPerMilliSecond = 0.1;
            settings.AccelerationMaximumInIncrementsPerMilliSecond2 = 1500;
            settings.AccelerationMinimumInIncrementsPerMilliSecond2 = 0.1;
            _motionControlCalculator = new MotionControlCalculator(
                settings.SpeedMinimumInIncrementsPerMilliSecond,
                settings.SpeedMaximumInIncrementsPerMilliSecond,
                settings.AccelerationMaximumInIncrementsPerMilliSecond2);
        }

        [TearDown]
        public void Teardown()
        {
            _motionControlCalculator = null;
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(MotionControlCalculator), _motionControlCalculator);
        }

        [Test]
        public void CanCreate_WithSpeedMinimumHigherSpeedMaximum_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new MotionControlCalculator(9300, 0.1, 1500));
        }

        [Test]
        public void CanCreate_WithAccelerationLessThenZero_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new MotionControlCalculator(0.1, 9300, -0.1));
        }

        [Test]
        public void CalculateDistanceTimeSegments_WithSpeedMaximumLessThanZero_ThrowsException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _motionControlCalculator.CalculateDistanceTimeSegments(290, -0.1));
        }

        [Test]
        public void CalculateDistanceTimeSegments_WithSpeedMaximumGreaterThaneOne_ThrowsException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _motionControlCalculator.CalculateDistanceTimeSegments(290, 1.1));
        }

        private static void PrintIntermediaryDistanceTimeSegments(IEnumerable<IntermediaryDistanceTimeSegment> segments)
        {
            segments.ToList().ForEach(
                segment =>
                Console.WriteLine("distance: {0}, time: {1}", segment.DistanceInMillimeters, segment.TimeInSeconds));
        }

        [Test]
        public void CalculateDistanceTimeSegments1Test()
        {
            var distanceTimeSegments = _motionControlCalculator.CalculateDistanceTimeSegments(290, 1.0);
            var segments = distanceTimeSegments.ToList();
            PrintIntermediaryDistanceTimeSegments(segments);
            Assert.IsNotNull(segments);
            Assert.AreEqual(2, segments.Count());
            Assert.AreEqual(145, segments.First().DistanceInMillimeters);
            Assert.AreEqual(Math.Round(0.439696865275764, 6), Math.Round(segments.First().TimeInSeconds, 6));
            Assert.AreEqual(145, segments.Last().DistanceInMillimeters);
            Assert.AreEqual(Math.Round(0.439696865275764, 6), Math.Round(segments.First().TimeInSeconds, 6));
        }

        [Test]
        public void CalculateDistanceTimeSegments2Test()
        {
            var distanceTimeSegments = _motionControlCalculator.CalculateDistanceTimeSegments(-290, 1.0);
            var segments = distanceTimeSegments.ToList();
            PrintIntermediaryDistanceTimeSegments(segments);
            Assert.IsNotNull(segments);
            Assert.AreEqual(2, segments.Count());
            Assert.AreEqual(-145, segments.First().DistanceInMillimeters);
            Assert.AreEqual(Math.Round(0.439696865275764, 6), Math.Round(segments.First().TimeInSeconds, 6));
            Assert.AreEqual(-145, segments.Last().DistanceInMillimeters);
            Assert.AreEqual(Math.Round(0.439696865275764, 6), Math.Round(segments.First().TimeInSeconds, 6));
        }

        [Test]
        public void CalculateDistanceTimeSegments3Test()
        {
            var distanceTimeSegments = _motionControlCalculator.CalculateDistanceTimeSegments(28830 * 3, 1.0);
            var segments = distanceTimeSegments.ToList();
            PrintIntermediaryDistanceTimeSegments(segments);
            Assert.IsNotNull(segments);
            Assert.AreEqual(3, segments.Count());
            Assert.AreEqual(28830, segments.First().DistanceInMillimeters);
            Assert.AreEqual(Math.Round(3.1, 6), Math.Round(segments.First().TimeInSeconds, 6));
            Assert.AreEqual(28830, segments.ElementAt(1).DistanceInMillimeters);
            Assert.AreEqual(Math.Round(3.1, 6), Math.Round(segments.ElementAt(1).TimeInSeconds, 6));
            Assert.AreEqual(28830, segments.Last().DistanceInMillimeters);
            Assert.AreEqual(Math.Round(3.1, 6), Math.Round(segments.Last().TimeInSeconds, 6));
        }

        [Test]
        public void CalculateDistanceTimeSegments4Test()
        {
            var distanceTimeSegments = _motionControlCalculator.CalculateDistanceTimeSegments(-28830 * 3, 1.0);
            var segments = distanceTimeSegments.ToList();
            PrintIntermediaryDistanceTimeSegments(segments);
            Assert.IsNotNull(segments);
            Assert.AreEqual(3, segments.Count());
            Assert.AreEqual(-28830, segments.First().DistanceInMillimeters);
            Assert.AreEqual(Math.Round(3.1, 6), Math.Round(segments.First().TimeInSeconds, 6));
            Assert.AreEqual(-28830, segments.ElementAt(1).DistanceInMillimeters);
            Assert.AreEqual(Math.Round(3.1, 6), Math.Round(segments.ElementAt(1).TimeInSeconds, 6));
            Assert.AreEqual(-28830, segments.Last().DistanceInMillimeters);
            Assert.AreEqual(Math.Round(3.1, 6), Math.Round(segments.Last().TimeInSeconds, 6));
        }
    }
}
