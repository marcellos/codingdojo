﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataBootstrapper
{
    [TestFixture]
    public class NinjectBootstrapperFixture
    {
        NinjectBootstrapper _bootstrapper;

        [SetUp]
        public void Setup()
        {
            _bootstrapper = new NinjectBootstrapper();
        }

        [TearDown]
        public void Teardown()
        {
            _bootstrapper = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(NinjectBootstrapper), _bootstrapper);
        }

        [Test, Explicit]
        public void StartupShutdownTest()
        {
            _bootstrapper.Startup();
            Console.WriteLine("press ENTER to exit");
            Console.ReadLine();
            _bootstrapper.Shutdown();
        }

        [Test]
        public void EnsureOneInstanceTest1()
        {
            Assert.IsInstanceOf(typeof(NinjectBootstrapper), _bootstrapper);
            var bootstrapper = new NinjectBootstrapper();
            bootstrapper = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        [Test]
        public void EnsureOneInstanceTest2()
        {
            Assert.IsInstanceOf(typeof(NinjectBootstrapper), _bootstrapper);
            _bootstrapper = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            var bootstrapper = new NinjectBootstrapper();
            bootstrapper = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
