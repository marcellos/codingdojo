﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace KataBarcode
{
    [TestFixture]
    public class HoughTransformTests
    {
        private const string File7Name = "7.jpg";
        private const string FileHoughName = "hough_input.jpg";
        private const string FileHough_h_Name = "hough_h_input.jpg";
        private const string FileHough_v_Name = "hough_v_input.jpg";
        private const string HoughName = "Hough.jpg";

    }
}
