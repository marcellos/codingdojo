﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataCodeGeneration
{
    public interface ITestCommandParser<in T>
    {
        ITestCommand Parse(T msg);
    }
}
