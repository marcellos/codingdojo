﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Runtime.InteropServices;

namespace KataSoundSynthesizer
{
    class WaveOutBuffer : IDisposable
    {
        private readonly WaveHeader _waveHeader;
        private readonly Int32 _bufferSize;
        private readonly byte[] _buffer;
        private readonly IWaveStream _waveStream;
        private readonly object _waveOutLock;

        private GCHandle _hBuffer;
        private IntPtr _hWaveOut;
        private GCHandle _hHeader;
        private GCHandle _hThis;

        public Int32 BufferSize { get { return _bufferSize; } }

        public WaveOutBuffer(IntPtr hWaveOut, Int32 bufferSize, IWaveStream waveStream, object waveOutLock)
        {
            _bufferSize = bufferSize;
            _buffer = new byte[bufferSize];
            _hBuffer = GCHandle.Alloc(_buffer, GCHandleType.Pinned);
            _hWaveOut = hWaveOut;
            _waveStream = waveStream;
            _waveOutLock = waveOutLock;

            _waveHeader = new WaveHeader();
            _hHeader = GCHandle.Alloc(_waveHeader, GCHandleType.Pinned);
            _waveHeader.dataBuffer = _hBuffer.AddrOfPinnedObject();
            _waveHeader.bufferLength = bufferSize;
            _waveHeader.loops = 1;
            _hThis = GCHandle.Alloc(this);
            _waveHeader.userData = (IntPtr) _hThis;

            lock (_waveOutLock)
            {
                try
                {
                    WinMultiMediaApi.waveOutPrepareHeader(hWaveOut, _waveHeader, Marshal.SizeOf(_waveHeader));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void Dispose()
        {
            if (_hHeader.IsAllocated)
            {
                _hHeader.Free();
            }

            if (_hBuffer.IsAllocated)
            {
                _hBuffer.Free();
            }

            if (_hThis.IsAllocated)
            {
                _hThis.Free();
            }

            if (_hWaveOut != IntPtr.Zero)
            {
                lock (_waveOutLock)
                {
                    WinMultiMediaApi.waveOutUnprepareHeader(_hWaveOut, _waveHeader, Marshal.SizeOf(_waveHeader));
                }
                _hWaveOut = IntPtr.Zero;
            }
        }

        public bool OnDone()
        {
            var bytes = 0;
            lock (_waveStream)
            {
                bytes = _waveStream.Read(_buffer, 0, _buffer.Length);
            }

            if (bytes == 0)
            {
                return false;
            }

            for (var i = bytes; i < _buffer.Length; ++i)
            {
                _buffer[i] = 0;
            }

            WriteToWaveOut();
            return true;
        }

        public bool InQueue
        {
            get { return (_waveHeader.flags & WaveHeaderFlags.InQueue) == WaveHeaderFlags.InQueue; }
        }

        private void WriteToWaveOut()
        {
            MmResult result;
            lock (_waveOutLock)
            {
                result = WinMultiMediaApi.waveOutWrite(_hWaveOut, _waveHeader, Marshal.SizeOf(_waveHeader));
            }

            if (result != MmResult.NoError)
            {
                throw new Exception("waveOutWrite");
            }

            GC.KeepAlive(this);
        }
    }
}
