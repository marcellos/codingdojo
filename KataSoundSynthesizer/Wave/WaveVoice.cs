﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using KataSoundSynthesizer.SynthComponent;

namespace KataSoundSynthesizer.Wave
{
    class WaveVoice : IVoice
    {
        private readonly float[] _buffer;
        private readonly bool _isRepeating;
        private int _bufferIndex;
        private bool _isRunToEnd;
        private float _volume;
        private float[,] _samples;

        public float Panning { get; set; }

        public WaveVoice(float[] buffer, bool isReversed = false, bool isRepeating = false)
        {
            _buffer = isReversed == true
                          ? WaveDataConverter.ReverseFloatBuffer(buffer)
                          : buffer;
            _isRepeating = isRepeating;
            _samples = new float[2,1];
        }

        public void RenderSamples(int offset, int count)
        {
            if (_samples == null || _samples.Length != count)
            {
                _samples = new float[2,count];
            }

            for (var i = 0; i < count; ++i)
            {
                if (_bufferIndex >= _buffer.Length - 1)
                {
                    _bufferIndex = 0;
                    _isRunToEnd = !_isRepeating;
                }

                _samples[0, i] = _volume*_buffer[_bufferIndex];
                _samples[1, i] = _volume*_buffer[_bufferIndex + 1];

                if (!_isRunToEnd)
                {
                    _bufferIndex += 2;
                }
            }
        }

        public float[] GetMonoBuffer()
        {
            return null;
        }

        public float[,] GetStereoBuffer()
        {
            return _samples;
        }
        
        public void TriggerKey(TrackedKey key)
        {
            _volume = key.Velocity;
            _bufferIndex = 0;
            _isRunToEnd = false;
        }

        public void ReleaseKey(TrackedKey key)
        {
            _bufferIndex = _buffer.Length;
        }

        public IVoice MakeInstanceCopy()
        {
            throw new NotImplementedException();
        }
    }
}
