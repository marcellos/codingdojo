﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataUSB
{
    public class Attack3DeviceData
    {
        public Attack3DeviceEnum DataType { get; set; }
        public byte X { get; set; }
        public byte Y { get; set; }
        public byte Thrust { get; set; }
    }
}
