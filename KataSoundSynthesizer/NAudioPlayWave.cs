﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Threading;
using NAudio.Wave;

namespace KataSoundSynthesizer
{
    class NAudioPlayWave
    {
        private ManualResetEvent _waitHandle; 

        public void PlaySineWave()
        {
            var sineWaveProvider = new NAudioSinewaveProvider32(440f, 0.25f);
            sineWaveProvider.SetWaveFormat(16000, 2);

            _waitHandle = new ManualResetEvent(false);

            var waveOut = new WaveOut(NAudio.Wave.WaveCallbackInfo.FunctionCallback());
            waveOut.Init(sineWaveProvider);
            waveOut.PlaybackStopped += WaveOutPlaybackStopped;
            waveOut.Play();
            var timeout = _waitHandle.WaitOne(5000);
            waveOut.Stop();
            waveOut.Dispose();
        }

        void WaveOutPlaybackStopped(object sender, StoppedEventArgs e)
        {
            Console.WriteLine("playback stopped");
            _waitHandle.Set();
        }
    }
}
