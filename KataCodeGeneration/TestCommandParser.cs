﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Globalization;
using System.Text;

namespace KataCodeGeneration
{
    class TestCommandParser : ITestCommandParser<byte[]>
    {
        private const int ControlMessageHeaderLength = 4;
        private const char StandardMessageChunkDelimiter = '-';

        public ITestCommand Parse(byte[] msg)
        {
            var messageHeaderString = DecodeControlMessageHeader(msg);
            
            if (messageHeaderString.Equals("test"))
            {
                var messageWithoutHeader = RemoveControlMessageHeader(ConvertMessageToString(msg));
                var chunks = messageWithoutHeader.Split( StandardMessageChunkDelimiter );

                var value = int.Parse(chunks[1], CultureInfo.InvariantCulture);
                return new TestCommand(value);
            }

            throw new Exception(string.Format("Could not parse TestMessage: '{0}'",
                                         ConvertMessageToString(msg)));
        }

        private static string DecodeControlMessageHeader(byte[] message)
        {
            var messageHeader = new byte[ControlMessageHeaderLength];
            Array.Copy(message, messageHeader, ControlMessageHeaderLength);
            return Encoding.Default.GetString(messageHeader);
        }

        private static string ConvertMessageToString(byte[] message)
        {
            return Encoding.Default.GetString(message);
        }

        private static string RemoveControlMessageHeader(string message)
        {
            return message.Substring(ControlMessageHeaderLength + 1);
        }
    }
}
