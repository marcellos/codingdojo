﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;

namespace KataRomanNumerals
{
    static class RomanConverter
    {
        private static int _power;
        private static int _accu;
        private static IList<Numeral> _numerals;

        public static string Convert(string input)
        {
            var convertedValue = string.Empty;
            _power = 0;
            _accu = 0;

            var conversion = SetConversion(input);
            convertedValue = EnumerateStringRightToLeft(input, conversion)
                .Aggregate(convertedValue, (current, part) => part + current);
            if (string.IsNullOrEmpty(convertedValue))
            {
                convertedValue = _accu.ToString();
            }

            return convertedValue;
        }

        private static IEnumerable<string> EnumerateStringRightToLeft(string input, Func<string, string> evaluateCall)
        {
            for (var i = 0; i < input.Length; i++)
            {
                yield return evaluateCall(input[input.Length - i - 1].ToString());
            }
        }

        private static string ArabicToRoman(string input)
        {
            var romanLetter = string.Empty;
            switch (input)
            {
                case "1":
                case "2":
                case "3":
                    romanLetter = new String(_numerals[_power].Letter[0], int.Parse(input));
                    break;
                case "4":
                    romanLetter = _numerals[_power].Letter + _numerals[_power].LeftBound;
                    break;
                case "5":
                    romanLetter = _numerals[_power].LeftBound;
                    break;
                case "6":
                case "7":
                case "8":
                    romanLetter = _numerals[_power].LeftBound;
                    romanLetter += new String(_numerals[_power].Letter[0], int.Parse(input) - 5);
                    break;
                case "9":
                    romanLetter = _numerals[_power].Letter + _numerals[_power].RightBound;
                    break;
            }

            if (_power < 3)
            {
                _power++;
            }

            return romanLetter;
        }

        private static string RomanToArabic(string input)
        {
            switch (input)
            {
                case "I":
                    _accu += (_accu == 5 || _accu >= 10) ? -1 : 1;
                    break;
                case "V":
                    _accu += 5;
                    break;
                case "X":
                    _accu += (_accu == 50 || _accu >= 100) ? -10 : 10;
                    break;
                case "L":
                    _accu += 50;
                    break;
                case "C":
                    _accu += (_accu == 500 || _accu >= 1000) ? -100 : 100;
                    break;
                case "D":
                    _accu += 500;
                    break;
                case "M":
                    _accu += 1000;
                    break;
            }

            return string.Empty;
        }

        private static Func<string, string> SetConversion(string input)
        {
            const string ARABIC_NUMERALS = "0123456789";
            const string ROMAN_NUMERALS = "IVXLCDM";

            if (_numerals == null)
            {
                _numerals = new List<Numeral>();
            }

            // convert arabic to roman
            if (ContainAnyChar(input, ARABIC_NUMERALS))
            {
                _numerals.Clear();
                _numerals.Add(new Numeral("I", "V", "X"));
                _numerals.Add(new Numeral("X", "L", "C"));
                _numerals.Add(new Numeral("C", "D", "M"));
                _numerals.Add(new Numeral("M", "M", "M"));
                return ArabicToRoman;
            }

            // convert roman to arabic
            if (ContainAnyChar(input, ROMAN_NUMERALS))
            {
                _numerals.Clear();
                return RomanToArabic;
            }

            return null;
        }

        private static bool ContainAnyChar(string input, string characters)
        {
            return characters.Any(input.Contains);
        }
    }
}
