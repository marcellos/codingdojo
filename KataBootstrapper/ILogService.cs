﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataBootstrapper
{
    public interface ILogService
    {
        void Info(string format, params object[] args);
    }
}
