﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

using NUnit.Framework;

namespace KataMachineLearning
{
    [TestFixture]
    public class MatrixTest
    {
        [Test]
        public void Matrix_WhenCtor_ThenGetInstance()
        {
            var m = new Matrix(2, 3);
            Assert.IsInstanceOf(typeof(Matrix), m);
        }

        [Test]
        public void Matrix_Indexer_SetAndGetValues()
        {
            var m = new Matrix(2, 3);
            m[0, 0] = 1.0;
            m[1, 2] = 1.0;
            Assert.AreEqual(1.0, m[0, 0]);
            Assert.AreEqual(1.0, m[1, 2]);
        }

        [Test]
        public void Matrix_MultiplicationWrongDimension_GetException()
        {
            var m1 = new Matrix(1, 3);
            var m2 = new Matrix(2, 2);
            Matrix m = null;
            
            var ex = Assert.Throws<InvalidOperationException>(() => m = m1 * m2);
        }

        [Test]
        public void Matrix_Multiplication_GetResult()
        {
            var m1 = new Matrix(2, 3);
            var m2 = new Matrix(3, 2);

            m1[0, 0] = 1;
            m1[1, 0] = 4;
            m1[0, 1] = 2;
            m1[1, 1] = 5;
            m1[0, 2] = 3;
            m1[1, 2] = 6;

            m2[0, 0] = 6;
            m2[1, 0] = 3;
            m2[2, 0] = 0;
            m2[0, 1] = -1;
            m2[1, 1] = 2;
            m2[2, 1] = -3;

            var m = m1 * m2;
            Assert.AreEqual(2, m.Rows);
            Assert.AreEqual(2, m.Columns);
            Assert.AreEqual(12, m[0, 0]);
            Assert.AreEqual(39, m[1, 0]);
            Assert.AreEqual(-6, m[0, 1]);
            Assert.AreEqual(-12, m[1, 1]);
        }

        [Test]
        public void Matrix_Apply_ResultInPlace()
        {
            var m = new Matrix(2, 2);
            m[0, 0] = 1;
            m[0, 1] = 1;
            m[1, 0] = 1;
            m[1, 1] = 1;

            m.Apply(v => 2.0*v);
            Assert.AreEqual(2.0, m[0, 0]);
            Assert.AreEqual(2.0, m[0, 1]);
            Assert.AreEqual(2.0, m[1, 0]);
            Assert.AreEqual(2.0, m[1, 1]);
        }
    }
}
