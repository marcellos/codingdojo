﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataPotter
{
    class Item : IEquatable<Item>
    {
        public string Name { get; set; }
        public decimal Price { get; set; }

        public bool Equals(Item other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Name.Equals(other.Name) && Price.Equals(other.Price);
        }

        public override int GetHashCode()
        {
            int hashName = Name == null ? 0 : Name.GetHashCode();
            int hashPrice = Price.GetHashCode();
            return hashName ^ hashPrice;
        }
    }
}
