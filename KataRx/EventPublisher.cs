﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Concurrent;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace KataRx
{
    class EventPublisher : IEventPublisher
    {
        private readonly ConcurrentDictionary<Type, object> _subjects = new ConcurrentDictionary<Type, object>();

        public void Publish<T>(T message)
        {
            object subject;
            if (_subjects.TryGetValue(typeof(T), out subject))
            {
                ((ISubject<T>)subject).OnNext(message);
            }
        }

        public IObservable<T> GetEvent<T>()
        {
            var subject = (ISubject<T>)_subjects.GetOrAdd(typeof(T), t => new Subject<T>());
            return subject.AsObservable();
        }
    }
}
