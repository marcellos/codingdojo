﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Diagnostics;
using NUnit.Framework;

namespace KataHeap
{
    [TestFixture]
    public class BinaryTreeTests
    {
        private BinaryTree<int> _binaryTree;

        [SetUp]
        public void Setup()
        {
            _binaryTree = new BinaryTree<int>();
        }

        [TearDown]
        public void Teardown()
        {
            _binaryTree = null;
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(BinaryTree<int>), _binaryTree);
        }

        [Test]
        public void Add_WithNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _binaryTree.Add(null));
        }

        [Test]
        public void Add_WithNode_CorrectCount()
        {
            _binaryTree.Add(new BinaryTreeNode<int>(100));
            Assert.AreEqual(1, _binaryTree.Count);
        }

        [Test]
        public void Add_With3Nodes_NodesAreAtCorrectPlaces()
        {
            _binaryTree.Add(new BinaryTreeNode<int>(100));
            _binaryTree.Add(new BinaryTreeNode<int>(99));
            _binaryTree.Add(new BinaryTreeNode<int>(101));
            Assert.AreEqual(3, _binaryTree.Count);
        }

        [Test]
        public void Add_With7Nodes_NodesAreAtCorrectPlaces()
        {
            _binaryTree.Add(new BinaryTreeNode<int>(4));
            _binaryTree.Add(new BinaryTreeNode<int>(2));
            _binaryTree.Add(new BinaryTreeNode<int>(6));
            _binaryTree.Add(new BinaryTreeNode<int>(1));
            _binaryTree.Add(new BinaryTreeNode<int>(3));
            _binaryTree.Add(new BinaryTreeNode<int>(5));
            _binaryTree.Add(new BinaryTreeNode<int>(7));
            Assert.AreEqual(7, _binaryTree.Count);
        }

        [Test]
        public void Add_With7Nodes_DifferentOrderAtCorrectPlaces()
        {
            _binaryTree.Add(new BinaryTreeNode<int>(4));
            _binaryTree.Add(new BinaryTreeNode<int>(3));
            _binaryTree.Add(new BinaryTreeNode<int>(5));
            _binaryTree.Add(new BinaryTreeNode<int>(2));
            _binaryTree.Add(new BinaryTreeNode<int>(1));
            _binaryTree.Add(new BinaryTreeNode<int>(6));
            _binaryTree.Add(new BinaryTreeNode<int>(7));
            Assert.AreEqual(7, _binaryTree.Count);
        }

        [Test]
        public void Remove_WithNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _binaryTree.Remove(null));
        }

        [Test]
        public void Remove_WithBlankTree_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => _binaryTree.Remove(new BinaryTreeNode<int>(99)));
        }

        [Test]
        public void Remove_WithNodeNotContained_ThrowsException()
        {
            _binaryTree.Add(new BinaryTreeNode<int>(100));
            Assert.AreEqual(1, _binaryTree.Count);

            Assert.Throws<ArgumentException>(() => _binaryTree.Remove(new BinaryTreeNode<int>(99)));
        }

        [Test]
        public void Remove_With1Node_NodeRemoved()
        {
            var node = new BinaryTreeNode<int>(99);
            _binaryTree.Add(node);
            Assert.AreEqual(1, _binaryTree.Count);
            _binaryTree.Remove(node);
            Assert.AreEqual(0, _binaryTree.Count);
        }

        [Test]
        public void Remove_With3Nodes_NodeRemoved()
        {
            var node = new BinaryTreeNode<int>(99);
            _binaryTree.Add(new BinaryTreeNode<int>(100));
            _binaryTree.Add(node);
            _binaryTree.Add(new BinaryTreeNode<int>(101));
            Assert.AreEqual(3, _binaryTree.Count);
            _binaryTree.Remove(node);
            Assert.AreEqual(2, _binaryTree.Count);
        }

        private static void AddNodes(BinaryTree<int> tree)
        {
            tree.Add(new BinaryTreeNode<int>(4));
            tree.Add(new BinaryTreeNode<int>(2));
            tree.Add(new BinaryTreeNode<int>(6));
            tree.Add(new BinaryTreeNode<int>(1));
            tree.Add(new BinaryTreeNode<int>(3));
            tree.Add(new BinaryTreeNode<int>(5));
            tree.Add(new BinaryTreeNode<int>(7));
        }

        [Test]
        public void Find_WithKey1_ReturnCorrectNode()
        {
            AddNodes(_binaryTree);
            var node = _binaryTree.Find(1);
            Assert.AreEqual(1, node.Key);
        }

        [Test]
        public void Find_WithKey7_ReturnCorrectNode()
        {
            AddNodes(_binaryTree);
            var node = _binaryTree.Find(7);
            Assert.AreEqual(7, node.Key);
        }

        [Test]
        public void Find_WithKey8_ReturnNull()
        {
            AddNodes(_binaryTree);
            Assert.IsNull(_binaryTree.Find(8));
        }

        [Test]
        public void Contains_WithFuncKeyEq7_True()
        {
            AddNodes(_binaryTree);
            Assert.IsTrue(_binaryTree.Contains(n => n.Key == 7));
        }

        [Test]
        public void Contains_WithFuncKeyEq5_True()
        {
            AddNodes(_binaryTree);
            Assert.IsTrue(_binaryTree.Contains(n => n.Key == 5));
        }

        [Test]
        public void Contains_WithFuncKeyEq2_True()
        {
            AddNodes(_binaryTree);
            Assert.IsTrue(_binaryTree.Contains(n => n.Key == 2));
        }

        [Test]
        public void Contains_WithFuncReturnsFalse_False()
        {
            AddNodes(_binaryTree);
            Assert.IsFalse(_binaryTree.Contains(n => n.Key == 0));
        }

        private class AnnotatedBinaryTreeNode<T> : BinaryTreeNode<T>
        {
            public string Annotation { get; private set; }

            public AnnotatedBinaryTreeNode(T key, string annotation)
                : base(key)
            {
                Annotation = annotation;
            }
        }

        [Test]
        public void Add_AnnotatedNode_CorrectCount()
        {
            _binaryTree.Add(new AnnotatedBinaryTreeNode<int>(0, "my annotation"));
            Assert.AreEqual(1, _binaryTree.Count);
        }

        [Test]
        public void Balance_WithOrderedNodes_BalancedTree()
        {
            _binaryTree.Add(new BinaryTreeNode<int>(1));
            _binaryTree.Add(new BinaryTreeNode<int>(2));
            _binaryTree.Add(new BinaryTreeNode<int>(3));
            _binaryTree.Add(new BinaryTreeNode<int>(4));
            _binaryTree.Add(new BinaryTreeNode<int>(5));
            _binaryTree.Add(new BinaryTreeNode<int>(6));
            _binaryTree.Add(new BinaryTreeNode<int>(7));

            _binaryTree.Balance();

            var node = _binaryTree.Find(7);
            Assert.AreEqual(7, node.Key);
        }

        [Test]
        public void Balance_With10e6orderedNodes_BalanceTree()
        {
            const int NumberOfItems = (int) 1e6;
            const int SpecificItem = NumberOfItems - 2;
            var stopWatch = new Stopwatch();

            Console.WriteLine("inserting {0} items in quasi random order..", NumberOfItems);
            stopWatch.Start();
            const int iMax = 1000;
            const int jMax = NumberOfItems/iMax;
            for (var j = 0; j < jMax; ++j)
            {
                for (var i = 0; i < iMax; ++i)
                {
                    _binaryTree.Add(new BinaryTreeNode<int>(j + i*jMax));
                }
            }
            stopWatch.Stop();
            Console.WriteLine("time [ms]: {0}", stopWatch.Elapsed.TotalMilliseconds);

            Console.WriteLine("balancing..");
            stopWatch.Reset();
            stopWatch.Start();
            _binaryTree.Balance();
            stopWatch.Stop();
            Console.WriteLine("time [ms]: {0}", stopWatch.Elapsed.TotalMilliseconds);
            Console.WriteLine("item count: {0}", _binaryTree.Count);

            Console.WriteLine("searching item {0}..", SpecificItem);
            stopWatch.Reset();
            stopWatch.Start();
            var node = _binaryTree.Find(SpecificItem);
            stopWatch.Stop();
            Console.WriteLine("time [ms]: {0}", stopWatch.Elapsed.TotalMilliseconds);
            Assert.AreEqual(SpecificItem, node.Key);
        }

        [Test]
        public void LoadAndBalance_With10e7orderedNodes_BalanceTree()
        {
            const int NumberOfItems = (int)1e7;
            const int SpecificItem = NumberOfItems - 2;
            var stopWatch = new Stopwatch();

            Console.WriteLine("load and balance {0} ordered items..", NumberOfItems);            
            var list = new BinaryTreeNode<int>[NumberOfItems];
            for (var i = 0; i < NumberOfItems; ++i)
            {
                list[i] = new BinaryTreeNode<int>(i);
            }
            Console.WriteLine("balancing..");
            stopWatch.Start();
            _binaryTree.LoadAndBalance(list);
            stopWatch.Stop();
            Console.WriteLine("time [ms]: {0}", stopWatch.Elapsed.TotalMilliseconds);
            Console.WriteLine("item count: {0}", _binaryTree.Count);

            Console.WriteLine("searching item {0}..", SpecificItem);
            stopWatch.Reset();
            stopWatch.Start();
            var node = _binaryTree.Find(SpecificItem);
            stopWatch.Stop();
            Console.WriteLine("time [ms]: {0}", stopWatch.Elapsed.TotalMilliseconds);
            Assert.AreEqual(SpecificItem, node.Key);
        }

    }
}
