﻿#region license and copyright
/*
The MIT License, Copyright (c) 2011-2020 Marcel Schneider
for details see License.txt
 */
#endregion
        
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using NUnit.Framework;

namespace KataHeap
{
    [TestFixture]
    public class SuffixTreeNodeTests
    {
        private SuffixTree _suffixTree;

        [SetUp]
        public void Setup()
        {
            _suffixTree = new SuffixTree();
        }

        [TearDown]
        public void Teardown()
        {
            _suffixTree = null;
        }

        [Test]
        public void Add_abc_Added()
        {
            AddString("abc");
        }

        [Test]
        public void Add_abcabxabcd_Added()
        {
            AddString("abcabxabcd");
        }

        [Test]
        public void Add_bananas_Added()
        {
            AddString("bananas");
        }


        private void AddString(string data)
        {
            foreach (var c in data)
            {
                _suffixTree.Add(c);
            }
        }
    }
}
