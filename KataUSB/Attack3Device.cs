﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataUSB
{
    public class Attack3Device : HidDevice
    {
        public const int VID = 0x046d;
        public const int PID = 0xc214;

        public Action<Attack3DeviceData> Attack3DeviceDataHandler = data => { };

        private byte _x, _y, _buttons1To8, _buttons9To11, _thrust;

        private enum Buttons1To8
        {
            // byte #4
            Button1Pressed = 0x01,
            Button1Released = 0xfe,
            Button2Pressed = 0x02,
            Button2Released = 0xfd,
            Button3Pressed = 0x04,
            Button3Released = 0xfb,
            Button4Pressed = 0x08,
            Button4Released = 0xf7,
            Button5Pressed = 0x10,
            Button5Released = 0xef,
            Button6Pressed = 0x20,
            Button6Released = 0xdf,
            Button7Pressed = 0x40,
            Button7Released = 0xbf,
            Button8Pressed = 0x80,
            Button8Released = 0x7f
        }

        private enum Buttons9To11
        {
            // byte #5
            Button9Pressed = 0x01,
            Button9Released = 0xfe,
            Button10Pressed = 0x02,
            Button10Released = 0xfd,
            Button11Pressed = 0x04,
            Button11Released = 0xfb
        }

        public Attack3Device()
        {
            DeviceRemovedHandler = ex => Console.WriteLine(ex.Message);
            DataReceivedHandler = DataHandler;
        }

        protected void DataHandler(byte[] buffer)
        {
            //var dataString = string.Empty;
            //foreach (var b in buffer)
            //{
            //    dataString += dataString.Length > 0 ? "," : string.Empty;
            //    dataString += string.Format("{0:x2}", b);
            //}
            //Console.WriteLine("bytes: " + dataString);

            // buttons 1-8 change
            var buttons1To8 = buffer[4];
            var deviceEnum1 = DetectButtonChanges(buttons1To8, _buttons1To8, typeof(Buttons1To8), typeof(Attack3DeviceEnum));
            if (deviceEnum1 != null)
            {
                Attack3DeviceDataHandler(new Attack3DeviceData { DataType = (Attack3DeviceEnum)deviceEnum1 });
            }

            // buttons 9-11 change
            var buttons9To11 = buffer[5];
            var deviceEnum2 = DetectButtonChanges(buttons9To11, _buttons9To11, typeof(Buttons9To11), typeof(Attack3DeviceEnum));
            if (deviceEnum2 != null)
            {
                Attack3DeviceDataHandler(new Attack3DeviceData { DataType = (Attack3DeviceEnum)deviceEnum2 });
            }

            // xy change
            var x = buffer[1];
            var y = buffer[2];
            if (_x != x || _y != y)
            {
                Attack3DeviceDataHandler(new Attack3DeviceData { DataType = Attack3DeviceEnum.XYMovement, X = x, Y = y });
            }

            // thrust change
            var thrust = buffer[3];
            if (_thrust != thrust)
            {
                Attack3DeviceDataHandler(new Attack3DeviceData { DataType = Attack3DeviceEnum.ThrustMovement, Thrust = thrust });
            }

            _x = x;
            _y = y;
            _thrust = thrust;
            _buttons1To8 = buttons1To8;
            _buttons9To11 = buttons9To11;
        }

        private object DetectButtonChanges(byte newValue, byte previousValue, Type buttonEnumType, Type deviceEnumType)
        {
            if (previousValue == newValue)
            {
                return null;
            }

            var buttons = Enum.GetValues(buttonEnumType);
            for (var i = 0; i < buttons.GetLength(0); i++)
            {
                var button = (int)buttons.GetValue(i);

                // button pressed
                if ((newValue & button) == button && (previousValue & button) == 0x00)
                {
                    var buttonEnum = Enum.ToObject(buttonEnumType, button);
                    var deviceEnum = Enum.Parse(deviceEnumType, buttonEnum.ToString());
                    return deviceEnum;
                }

                // button released
                if ((newValue & button) == 0x00 && (previousValue & button) == button)
                {
                    var buttonEnum = Enum.ToObject(buttonEnumType, 0xff ^ button);
                    var deviceEnum = Enum.Parse(deviceEnumType, buttonEnum.ToString());
                    return deviceEnum;
                }
            }

            return null;
        }
    }
}
