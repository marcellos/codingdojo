﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace KataMicrokernel
{
    public class Microkernel
    {
        private readonly Bindings _bindings;
        private readonly Scope _globalSingletonScope;
        private Binding _binding;
        private bool _hasBinding;

        public Microkernel()
        {
            _bindings = new Bindings();
            _globalSingletonScope = new Scope();
        }

        public Microkernel Bind<TAbstraction, TImplementation>()
        {
            _binding = _bindings.Add(typeof(TAbstraction), typeof(TImplementation));
            _hasBinding = true;
            return this;
        }

        public void InGlobalSingletonScope()
        {
            if (!_hasBinding)
            {
                return;
            }

            var newBinding = new Binding(_binding)
                {
                    ScopeHashCode = _globalSingletonScope.GetHashCode()
                };
            _bindings.UpdateBinding(_binding, newBinding);
            _binding = newBinding;
        }

        public IEnumerable<T> GetInstances<T>()
        {
            return GetInstances<T>(typeof(T)).Cast<T>();
        }

        private IEnumerable GetInstances<T>(Type type)
        {
            var cyclicDepedencyChecker = new CyclicDependencyChecker(_bindings);
            cyclicDepedencyChecker.CheckType(type);

            var implementationTypes = _bindings.GetImplementationTypes(type);
            return !implementationTypes.Any() ? null : Resolve<T>(implementationTypes);
        }

        private IEnumerable Resolve<T>(IEnumerable<Type> implementationTypes)
        {
            var instances = implementationTypes.Select(ResolveType<T>);
            return instances;
        }

        private object ResolveType<T>(Type implementationType)
        {
            var constructor = GetConstructorWithMostArguments(implementationType);
            if (constructor.GetParameters().Length == 0)
            {
                return CreateOrLookupInstanceInScope(_bindings, _globalSingletonScope, implementationType);
            }

            var args = new List<object>(constructor.GetParameters().Length);
            foreach (var parameterInfo in constructor.GetParameters())
            {
                if (parameterInfo.IsOptional)
                {
                    args.Add(parameterInfo.DefaultValue);
                    continue;
                }

                var parameterType = parameterInfo.ParameterType;
                var genericArgumentType = GetFirstGenericArgumentType(parameterType);

                var parameterInstances = CreateParameterInstances<T>(genericArgumentType, parameterType);
                ThrowErrorWhenParameterInstancesAreNull(parameterInstances, parameterType);
                var castInstances = parameterInstances.Cast<object>();

                if (parameterType.IsGenericType)
                {
                    var list = CreateGenericList(genericArgumentType);
                    AddItemsToList(castInstances, list);
                    args.Add(list);
                    continue;
                }

                args.Add(castInstances.Count() == 1 ? castInstances.First() : castInstances);
            }

            return CreateOrLookupInstanceInScope(_bindings, _globalSingletonScope, implementationType, args.ToArray());
        }

        private static ConstructorInfo GetConstructorWithMostArguments(Type implementationType)
        {
            return implementationType.GetConstructors()
                .OrderBy(c => c.GetParameters().Length)
                .LastOrDefault();
        }

        private static object CreateOrLookupInstanceInScope(Bindings bindings, Scope scope, Type type, params object[] args)
        {
            var binding = bindings.GetBindingForImplementationType(type);
            if (scope.GetHashCode() == binding.ScopeHashCode)
            {
                var instance = Scope.Find(scope, type);
                if (instance != null)
                {
                    return instance;
                }

                instance = CreateInstance(type, args);
                scope.Add(type, instance);
                return instance;
            }

            return CreateInstance(type, args);
        }

        private static object CreateInstance(Type type, params object[] args)
        {
            return Activator.CreateInstance(type, args);
        }

        private static Type GetFirstGenericArgumentType(Type parameterType)
        {
            Type genericArgumentType = null;
            if (parameterType.IsGenericType)
            {
                // TODO should probably constrain to IEnumerable<T>
                var genericArguments = parameterType.GetGenericArguments();
                genericArgumentType = genericArguments.First();
            }

            return genericArgumentType;
        }

        private IEnumerable CreateParameterInstances<T>(Type genericArgumentType, Type parameterType)
        {
            var parameterInstances = genericArgumentType != null
                                         ? GetInstances<T>(genericArgumentType)
                                         : GetInstances<T>(parameterType);
            return parameterInstances;
        }

        private static void ThrowErrorWhenParameterInstancesAreNull(IEnumerable parameterInstances, Type parameterType)
        {
            if (parameterInstances == null)
            {
                throw new InvalidOperationException(
                    string.Format("Not able to resolve implementation for type '{0}'", parameterType.FullName));
            }
        }

        private static IList CreateGenericList(Type type)
        {
            var listType = typeof(List<>).MakeGenericType(type);
            return (IList)Activator.CreateInstance(listType);
        }

        private static void AddItemsToList(IEnumerable<object> items, IList list)
        {
            foreach (var item in items)
            {
                list.Add(item);
            }
        }
    }
}
