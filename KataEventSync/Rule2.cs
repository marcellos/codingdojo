﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataEventSync
{
    public class Rule2 : Rule<string>
    {
        private bool _event4, _event5;

        protected override object DoEvaluate(string message)
        {
            _event4 = _event4 || message.Equals("$event4");
            _event5 = _event5 || message.Equals("$event5");
            if (_event4 || _event5)
            {
                _event4 = _event5 = false;
                return "$rule2";
            }

            return null;
        }
    }
}
