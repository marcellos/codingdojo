﻿
/* generated file, do not edit! */

namespace KataCodeGeneration
{
  public class JumpToApplicationCommand
  {
    public string Cmd {get; private set;}

    public JumpToApplicationCommand(string cmd)
    {
      Cmd = cmd;
    }
  }

  public class JumpToBootloaderCommand
  {
    public string Cmd {get; private set;}

    public JumpToBootloaderCommand(string cmd)
    {
      Cmd = cmd;
    }
  }

  public class ReportFirmwareVersionCommand
  {
    public string Type {get; private set;}

    public ReportFirmwareVersionCommand(string type)
    {
      Type = type;
    }
  }

  public class ReportMessagePayloadCommand
  {

    public ReportMessagePayloadCommand()
    {
    }
  }

  public class ApplicationDataEraseCommand
  {

    public ApplicationDataEraseCommand()
    {
    }
  }

  public class StopAliveTimeoutCommand
  {
    public int Timeout {get; private set;}

    public StopAliveTimeoutCommand(int timeout)
    {
      Timeout = timeout;
    }
  }

  public class SendDataHexCommand
  {
    public string Data {get; private set;}

    public SendDataHexCommand(string data)
    {
      Data = data;
    }
  }

  public class ReportProductNameCommand
  {

    public ReportProductNameCommand()
    {
    }
  }

  public class ReportSerialNumberCommand
  {

    public ReportSerialNumberCommand()
    {
    }
  }

  public class SetProductNameCommand
  {
    public string Data {get; private set;}

    public SetProductNameCommand(string data)
    {
      Data = data;
    }
  }

  public class SetSerialNumberCommand
  {
    public int SerialNr {get; private set;}

    public SetSerialNumberCommand(int serialNr)
    {
      SerialNr = serialNr;
    }
  }

  public class ReportFreeOtpLocationsCommand
  {

    public ReportFreeOtpLocationsCommand()
    {
    }
  }

  public class ReportOtpDataCommand
  {
    public int StartId {get; private set;}
    public int NumData {get; private set;}

    public ReportOtpDataCommand(int startId, int numData)
    {
      StartId = startId;
      NumData = numData;
    }
  }

  public class SetOtpDataCommand
  {
    public int StartId {get; private set;}
    public string Data {get; private set;}

    public SetOtpDataCommand(int startId, string data)
    {
      StartId = startId;
      Data = data;
    }
  }

  public class EnableDeviceFunctionCommand
  {
    public int FunctionId {get; private set;}

    public EnableDeviceFunctionCommand(int functionId)
    {
      FunctionId = functionId;
    }
  }

  public class ReportAllDevicesCommand
  {

    public ReportAllDevicesCommand()
    {
    }
  }

  public class ConfigureFaststopEventIdCommand
  {
    public int EventId {get; private set;}

    public ConfigureFaststopEventIdCommand(int eventId)
    {
      EventId = eventId;
    }
  }

  public class ConfigureMasterNodeIdCommand
  {
    public int NodeId {get; private set;}

    public ConfigureMasterNodeIdCommand(int nodeId)
    {
      NodeId = nodeId;
    }
  }

  public class ReportFaststopEventIdCommand
  {

    public ReportFaststopEventIdCommand()
    {
    }
  }

  public class ReportMasterNodeIdCommand
  {

    public ReportMasterNodeIdCommand()
    {
    }
  }

  public class SendEventToNodeCommand
  {
    public string TimedEvent {get; private set;}
    public int EventId {get; private set;}
    public int SendTime {get; private set;}

    public SendEventToNodeCommand(string timedEvent, int eventId, int sendTime)
    {
      TimedEvent = timedEvent;
      EventId = eventId;
      SendTime = sendTime;
    }
  }

  public class SetLowlevelInterruptCommand
  {
    public int ModuleId {get; private set;}
    public string EventType {get; private set;}
    public int EventId {get; private set;}

    public SetLowlevelInterruptCommand(int moduleId, string eventType, int eventId)
    {
      ModuleId = moduleId;
      EventType = eventType;
      EventId = eventId;
    }
  }

  public class SetSerialtunnelConfigurationCommand
  {
    public int ComId {get; private set;}
    public int BaudRate {get; private set;}
    public int WordLength {get; private set;}
    public int StopBits {get; private set;}
    public int Parity {get; private set;}

    public SetSerialtunnelConfigurationCommand(int comId, int baudRate, int wordLength, int stopBits, int parity)
    {
      ComId = comId;
      BaudRate = baudRate;
      WordLength = wordLength;
      StopBits = stopBits;
      Parity = parity;
    }
  }

  public class SetSerialtunnelOnOffCommand
  {
    public int ComId {get; private set;}
    public int OnOff {get; private set;}

    public SetSerialtunnelOnOffCommand(int comId, int onOff)
    {
      ComId = comId;
      OnOff = onOff;
    }
  }

  public class ReportComPowerCommand
  {
    public int ComId {get; private set;}

    public ReportComPowerCommand(int comId)
    {
      ComId = comId;
    }
  }

  public class ReportCanPowerCommand
  {

    public ReportCanPowerCommand()
    {
    }
  }

  public class SwitchComPowerCommand
  {
    public int ComId {get; private set;}
    public int OnOff {get; private set;}

    public SwitchComPowerCommand(int comId, int onOff)
    {
      ComId = comId;
      OnOff = onOff;
    }
  }

  public class SwitchCanPowerCommand
  {
    public int OnOff {get; private set;}

    public SwitchCanPowerCommand(int onOff)
    {
      OnOff = onOff;
    }
  }

  public class ReportVoltageLimitsCommand
  {
    public int Voltage {get; private set;}

    public ReportVoltageLimitsCommand(int voltage)
    {
      Voltage = voltage;
    }
  }

  public class SetVoltageLimitsCommand
  {
    public int Voltage {get; private set;}
    public int Min {get; private set;}
    public int Max {get; private set;}

    public SetVoltageLimitsCommand(int voltage, int min, int max)
    {
      Voltage = voltage;
      Min = min;
      Max = max;
    }
  }

  public class FileSystemControlCommand
  {
    public string Operation {get; private set;}

    public FileSystemControlCommand(string operation)
    {
      Operation = operation;
    }
  }

}
