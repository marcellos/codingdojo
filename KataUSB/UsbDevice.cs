﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace KataUSB
{
    public abstract class UsbDevice : IDisposable
    {
        protected SafeFileHandle _handle;
        protected int _inputBufferLength;
        protected int _outputBufferLength;
        protected FileStream _fs;

        public Action<IOException> DeviceRemovedHandler = ex => { };
        protected Action<byte[]> DataReceivedHandler = buffer => { };

        ~UsbDevice()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (_fs != null)
            {
                _fs.Close();
                _fs = null;
            }

            if (_handle != null && !_handle.IsClosed)
            {
                Kernel32.CloseHandle(_handle);
            }
        }

        private static string GetDevicePath(IntPtr infoSet, ref SetupAPI.DeviceInterfaceData interfaceData)
        {
            uint requiredSize = 0;
            if (!SetupAPI.SetupDiGetDeviceInterfaceDetail(infoSet, ref interfaceData, IntPtr.Zero, 0, ref requiredSize, IntPtr.Zero))
            {
                var detailData = new SetupAPI.DeviceInterfaceDetailData();
                detailData.Size = Environment.Is64BitProcess ? 8 : 5;
                if (SetupAPI.SetupDiGetDeviceInterfaceDetail(infoSet, ref interfaceData, ref detailData, requiredSize, ref requiredSize, IntPtr.Zero))
                {
                    return detailData.DevicePath;
                }
            }

            return null;
        }

        public static IEnumerable<string> Enumerate(Guid classGuid)
        {
            var devicePathList = new List<string>();
            var infoSet = SetupAPI.SetupDiGetClassDevs(ref classGuid, null, IntPtr.Zero, SetupAPI.DIGCF_PRESENT | SetupAPI.DIGCF_DEVICEINTERFACE);

            var interfaceData = new SetupAPI.DeviceInterfaceData();
            interfaceData.Size = Marshal.SizeOf(interfaceData);

            try
            {
                var index = 0;
                while (SetupAPI.SetupDiEnumDeviceInterfaces(infoSet, 0, ref classGuid, (uint)index, ref interfaceData))
                {
                    devicePathList.Add(GetDevicePath(infoSet, ref interfaceData));
                    index++;
                }
            }
            finally
            {
                SetupAPI.SetupDiDestroyDeviceInfoList(infoSet);
            }

            return devicePathList;
        }

        public static object CreateDevice(Guid classGuid, int vid, int pid, Type deviceType)
        {
            var vendorProduct = string.Format("vid_{0:x4}&pid_{1:x4}", vid, pid);
            var devicePath = Enumerate(classGuid).Where(d => d.IndexOf(vendorProduct) >= 0).FirstOrDefault();
            if (!string.IsNullOrEmpty(devicePath))
            {
                var usbDevice = (UsbDevice)Activator.CreateInstance(deviceType);
                usbDevice.Initialize(devicePath);
                return usbDevice;
            }

            return null;
        }

        protected abstract void Initialize(string devicePath);

        protected virtual void BeginAsyncRead()
        {
            var buffer = new byte[_inputBufferLength];
            _fs.BeginRead(buffer, 0, _inputBufferLength, ReadCompleted, buffer);
        }

        protected virtual void ReadCompleted(IAsyncResult result)
        {
            var buffer = result.AsyncState as byte[];
            try
            {
                _fs.EndRead(result);
                try
                {
                    DataReceivedHandler(buffer);
                }
                finally
                {
                    BeginAsyncRead();
                }
            }
            catch (IOException ex)
            {
                DeviceRemovedHandler(ex);
                Dispose();
            }
        }

        protected virtual void Write(byte[] buffer)
        {
            try
            {
                _fs.Write(buffer, 0, _outputBufferLength);
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

    }
}
