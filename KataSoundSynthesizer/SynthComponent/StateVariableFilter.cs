﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using KataSoundSynthesizer.Tone;
using KataSoundSynthesizer.Oscillators;

namespace KataSoundSynthesizer.SynthComponent
{
    class StateVariableFilter : SynthComponentBase, IFilter
    {
        private const int FmScaler = 10;
        private const int NotesPerOctave = 12;
        private const float FrequencyMax = 12543.85366f;
        private const float DriveOffset = 0.5f;
        private const float DriveScale = 5.0f;
        private const float OutputMax = 0.66666f;
        private const int MiddleCNoteIndex = 40;
        private readonly ISynthComponent _source1;
        private readonly ISynthComponent _source2;
        private readonly ISynthComponent _am;
        private readonly ISynthComponent _lfo;
        private float _output1;
        private float _output2;
        private float _frequency;
        private float _cutOffFrequency;
        private int _trackedNote;

        public override int SampleRate { get; set; }
        public float Resonance { get; set; }
        public float Drive { get; set; }
        public FilterType Filter { get; set; }
        public float FilterGain { get; set; }
        public float Amplitude { get; set; }

        public float CutOffFrequency
        {
            get { return _cutOffFrequency; }
            set
            {
                _cutOffFrequency = value > FrequencyMax
                                 ? FrequencyMax
                                 : value;
            }
        }


        public StateVariableFilter(ISynthComponent source1, ISynthComponent source2)
        {
            _source1 = source1;
            _source2 = source2;

            Resonance = 0.0f;
            CutOffFrequency = (float) Scale.A440;
            Filter = FilterType.LowPass;
            Drive = 1.0f;
            _trackedNote = 70;
        }

        public StateVariableFilter(ISynthComponent source1, ISynthComponent source2, 
            ISynthComponent am, ISynthComponent lfo)
            : this (source1, source2)
        {
            _am = am;
            _lfo = lfo;
        }

        private StateVariableFilter(StateVariableFilter filter)
        {
            _source1 = filter._source1.MakeInstanceCopy();
            _source2 = filter._source2.MakeInstanceCopy();
            _am = filter._am.MakeInstanceCopy();
            _lfo = filter._lfo.MakeInstanceCopy();
            FilterGain = filter.FilterGain;
            Amplitude = filter.Amplitude;

            Resonance = filter.Resonance;
            CutOffFrequency = filter.CutOffFrequency;
            Filter = filter.Filter;
            Drive = filter.Drive;
        }

        public override ISynthComponent MakeInstanceCopy()
        {
            return new StateVariableFilter(this);
        }


        public override void RenderSamples(int offset, int count)
        {
            InitialiseMonoBuffer(count);
            Array.Clear(_monoBuffer, 0, _monoBuffer.Length);

            var output = 0.0f;

            _am.RenderSamples(offset, count);
            var amBuffer = _am.GetMonoBuffer();
            _lfo.RenderSamples(offset, count);
            var lfoBuffer = _lfo.GetMonoBuffer();

            var f = 1f; 
            var fb = 1f;
            var fm = 1f;
            var modNote = 1f;

            _source1.RenderSamples(offset, count);
            var source1Buffer = _source1.GetMonoBuffer();
            _source2.RenderSamples(offset, count);
            var source2Buffer = _source2.GetMonoBuffer();
            var input = 0f;

            var sampleCount = offset + count;
            for (var i = offset; i < sampleCount; ++i)
            {
                fm = FilterGain*(amBuffer[i] + lfoBuffer[i]);
                modNote = _trackedNote + NotesPerOctave*FmScaler*fm;

                if (modNote < 0)
                {
                    modNote = 0f;
                }
                else if (modNote > Scale.MaxToneIndex - 1)
                {
                    modNote = Scale.MaxToneIndex - 1;
                }

                _frequency = (float)(PowerOfTwoTable.GetPower((modNote - Scale.A440ToneIndex) / NotesPerOctave) * Scale.A440);
                _frequency = ClampMinMax(_frequency, 0f, FrequencyMax);
                //_frequency = ClampMinMax(_frequency + FilterGain * amBuffer[i], 0.0f, FrequencyMax);
                f = _frequency / FrequencyMax;
                fb = Resonance + Resonance / (1.0f - f);

                input = (source1Buffer[i] + source2Buffer[i]) / 2.0f * (Drive * DriveScale + DriveOffset);
                _output1 += f * (input - _output1 + fb * (_output1 - _output2));
                _output2 += f * (_output1 - _output2);

                switch (Filter)
                {
                    case FilterType.LowPass:
                        output = _output2;
                        break;

                    case FilterType.HighPass:
                        output = input - _output2;
                        break;

                    case FilterType.BandPass:
                        output = _output1 - _output2;
                        break;

                    case FilterType.Notch:
                        output = input - (_output1 - _output2);
                        break;

                    default:
                        break;
                }

                if (output >= 1.0f)
                {
                    output = OutputMax;
                }
                else if (output <= -1.0f)
                {
                    output = -OutputMax;
                }
                else
                {
                    output = output - output * output * output / 3.0f;
                }

                _monoBuffer[i] += output*Clamp(Amplitude*amBuffer[i]);
            }
        }

        public void TriggerKey(int noteNumber)
        {
            var frequencyNote = Scale.MaxToneIndex*_cutOffFrequency;
            _trackedNote = (int) Math.Round(frequencyNote + (noteNumber - MiddleCNoteIndex));

            if (_trackedNote < 0)
            {
                _trackedNote = 0;
            }
            else if (_trackedNote >= Scale.MaxToneIndex)
            {
                _trackedNote = Scale.MaxToneIndex;
            }
        }

        private static float Clamp(float value)
        {
            return Math.Max(Math.Min(value, 1.0f), 0.0f);
        }

        private static float ClampMinMax(float value, float min, float max)
        {
            return Math.Max(Math.Min(value, max), min);
        }
    }

    public enum FilterType
    {
        LowPass,
        HighPass,
        BandPass,
        Notch
    }
}
