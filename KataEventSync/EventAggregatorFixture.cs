﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataEventSync
{
    [TestFixture]
    public class EventAggregatorFixture
    {
        IEventAggregator _events;
        Recipient _recipient;

        [SetUp]
        public void Setup()
        {
            _events = new EventAggregator();
            _recipient = new Recipient();
        }

        [TearDown]
        public void Teardown()
        {
            _events = null;
            _recipient = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(EventAggregator), _events);
        }

        [Test]
        public void SubscribeTest()
        {
            _events.Subscribe(_recipient);
            // subscribe again
            _events.Subscribe(_recipient);
        }

        [Test]
        public void UnsubscribeTest()
        {
            _events.Unsubscribe(_recipient);
        }

        [Test]
        public void SubscribeUnsubscribeTest()
        {
            _events.Subscribe(_recipient);
            _events.Unsubscribe(_recipient);
            // unsubscribe again
            _events.Unsubscribe(_recipient);
        }

        [Test]
        public void PublishTest()
        {
            _events.Subscribe(_recipient);
            _events.Publish(100);
            Assert.AreEqual(100, _recipient.Message);
            _events.Unsubscribe(_recipient);
            _events.Publish(200);
            Assert.AreEqual(100, _recipient.Message);
        }

        [Test]
        public void BroadcastTest()
        {
            var newRecipient = new Recipient();
            _events.Subscribe(_recipient);
            _events.Subscribe(newRecipient);
            _events.Publish(100);
            Assert.AreEqual(100, _recipient.Message);
            Assert.AreEqual(100, newRecipient.Message);
            _events.Unsubscribe(_recipient);
            _events.Publish(200);
            Assert.AreEqual(100, _recipient.Message);
            Assert.AreEqual(200, newRecipient.Message);
            _events.Unsubscribe(newRecipient);
        }

        [Test]
        public void NullReferenceTest()
        {
            var newRecipient = new Recipient();
            _events.Subscribe(_recipient);
            _events.Subscribe(newRecipient);
            _events.Publish(100);
            Assert.AreEqual(100, _recipient.Message);
            Assert.AreEqual(100, newRecipient.Message);
            _recipient = null;
            GC.Collect();
            _events.Publish(200);
            Assert.AreEqual(200, newRecipient.Message);
            _events.Unsubscribe(_recipient);
            _events.Unsubscribe(newRecipient);
        }

        [Test]
        public void FaultyRecipientTest()
        {
            var faultyRecipient = new FaultyRecipient();
            _events.Subscribe(faultyRecipient);
            _events.Subscribe(_recipient);
            try
            {
                _events.Publish(100);
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf(typeof(System.Reflection.TargetInvocationException), ex);
                Assert.IsInstanceOf(typeof(NotImplementedException), ex.InnerException);
            }

            Assert.AreEqual(100, _recipient.Message);
        }

        internal class Recipient : IHandle<int>, IHandle<string>
        {
            public int Message { get; private set; }
            public void Handle(int message)
            {
                Message = message;
            }

            public void Handle(string message)
            {
                throw new NotImplementedException();
            }
        }

        internal class FaultyRecipient : IHandle<int>
        {
            public void Handle(int message)
            {
                throw new NotImplementedException();
            }
        }
    }
}
