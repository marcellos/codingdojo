﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;

namespace KataMachineLearning
{
    static class Statistics
    {
        public static T Max<T>(T[] series) where T : IComparable
        {
            if (series == null || series.Length == 0)
            {
                return default (T);
            }

            var max = series[0];

            for (var i = 0; i < series.Length; ++i )
            {
                if (series[i].CompareTo(max) >= 0)
                {
                    max = series[i];
                }
            }

            return max;
        }

        public static double MeanSquaredError(double[] predictions, double[] observations)
        {
            if (predictions == null || predictions.Length == 0)
            {
                return 0.0;
            }

            if (observations == null || observations.Length == 0)
            {
                return 0.0;
            }

            if (predictions.Length != observations.Length)
            {
                throw new ArgumentException("The lists must be of the same size.");
            }

            var n = predictions.Length;
            var sum = 0.0;
            var diff = 0.0;

            for (var i = 0; i < n; ++i)
            {
                diff = predictions[i] - observations[i];
                sum += diff*diff;
            }

            return sum/n;
        }

        public static double MeanSquaredError(int[] predictions, int[] observations)
        {
            if (predictions == null || predictions.Length == 0)
            {
                return 0.0;
            }

            if (observations == null || observations.Length == 0)
            {
                return 0.0;
            }

            if (predictions.Length != observations.Length)
            {
                throw new ArgumentException("The lists must be of the same size.");
            }

            var n = predictions.Length;
            var sum = 0.0;
            var diff = 0;

            for (var i = 0; i < n; ++i)
            {
                diff = predictions[i] - observations[1];
                sum += diff*diff;
            }

            return sum/n;
        }
    }
}
