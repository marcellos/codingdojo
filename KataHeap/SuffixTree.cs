﻿#region license and copyright
/*
The MIT License, Copyright (c) 2011-2020 Marcel Schneider
for details see License.txt
 */
#endregion
        
using System.Collections.Generic;
using System.Linq;

namespace KataHeap
{
    public class SuffixTree
    {
        /*
         * Rule 1:
         * After an insertion from root and active_length > 0
         *  - active_node remains root
         *  - active_edge is set (shifted right) to the first character of the new suffix we need to insert
         *  - active_length is reduced by 1
         *  
         * Rule 2:
         * If we split an edge an insert a new node, and if that is not the first node created 
         * during the current step, we connect the previously inserted node and the new node 
         * through a special pointer, a suffix link.
         * If we create a new internal node or make an insert from an internal node, and this is 
         * not the first such internal node at the current step, then we link the previous such 
         * node with this one through a suffix link.
         * 
         * Rule 3:
         * After splitting an edge from an active_node that is not the root node, we follow the 
         * suffix link going out of that node, if there is any, and reset the active_node to 
         * the node it points to. If there is no suffix link, we set the active_node to the root.
         * Active_edge and active_length remain unchanged.
         * After an insert from the active_node which is not the root node, we must follow the 
         * suffix link and set the active_node to the node it points to. If there is no suffix 
         * link, set the active_node to the root node. Active_edge and active_length stay unchanged.
         * 
         * Observation 1:
         * When the final suffix we need to insert is found to exist in the tree already, the 
         * tree itself is not changed at all (we only update the active point and the remainder).
         * 
         * Observation 2:
         * If at some point active_length is greater or equal to the lenght of the current edge 
         * (edge_length), we move our active point down until edge_length is not strictly greater 
         * than active_length.
         * 
         * Observation 3:
         * When the symbol we want to add to the tree is already on the edge, we, according to 
         * observation 1, update only active point and remainder, leaving the tree unchanged. 
         * But if there is an internal node marked as needing suffix link, we must connect that node 
         * with our current active_node through a suffix link.
         */
        

        private const int OO = int.MaxValue/2;
        private int _position;
        private readonly SuffixTreeNode _root;
        private readonly IList<char> _text; 

        // active point 
        private SuffixTreeNode _activeNode;
        private int _activeEdge;
        private int _activeLength;
        // remaining nodes to insert
        private int _remainder;
        private SuffixTreeNode _suffixLinkNode;

        public SuffixTree()
        {
            _text = new List<char>();

            _activeEdge = 0;
            _activeLength = 0;
            _position = -1;

            _root = new SuffixTreeNode(_position, _position, '\0');
            _activeNode = _root;
        }

        public void Add(char edge)
        {
            _text.Add(edge);
            _position++;
            _suffixLinkNode = null;
            _remainder++;

            while (_remainder > 0)
            {
                if (_activeLength == 0)
                {
                    _activeEdge = _position;
                }

                if (!_activeNode.Children.ContainsKey(_text[_activeEdge]))
                {
                    var node = new SuffixTreeNode(_position, OO, _text[_activeEdge]);
                    _activeNode.AddChildNode(node.Edge, node);
                    AddSuffixLink(_activeNode); // rule 2
                }
                else
                {
                    var next = _activeNode.Children[_text[_activeEdge]].First();
                    if (Walkdown(next))
                    {
                        // observation 2
                        continue;
                    }

                    if (_text[next.Begin + _activeLength] == edge)
                    {
                        // observation 1
                        _activeLength++;
                        // observation 3
                        AddSuffixLink(_activeNode);
                        break;
                    }

                    var splitNode = new SuffixTreeNode(next.Begin, next.Begin + _activeLength, _text[_activeEdge]);
                    _activeNode.AddChildNode(splitNode.Edge, splitNode);                    
                    var leaf = new SuffixTreeNode(_position, OO, edge);
                    splitNode.AddChildNode(edge, leaf);
                    next.SetBegin(next.Begin + _activeLength);
                    splitNode.AddChildNode(_text[next.Begin], next);
                    // rule 2
                    AddSuffixLink(splitNode);
                }

                if (_activeNode == _root && _activeLength > 0)
                {
                    // rule 1
                    _activeLength--;
                    _activeEdge = _position - _remainder + 1;
                }
                else
                {
                    // rule 3
                    _activeNode = _activeNode.Link ?? _root;
                }

                _remainder--;
            }
        }

        private void AddSuffixLink(SuffixTreeNode node)
        {
            if (_suffixLinkNode != null)
            {
                _suffixLinkNode.Link = node;
            }

            _suffixLinkNode = node;
        }

        private bool Walkdown(SuffixTreeNode node)
        {
            var edgeLength = node.EdgeLength(_position);
            if (_activeLength >= edgeLength)
            {
                _activeEdge += edgeLength;
                _activeLength -= edgeLength;
                _activeNode = node;
                return true;
            }

            return false;
        }
    }
}
