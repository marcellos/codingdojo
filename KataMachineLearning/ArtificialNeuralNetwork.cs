﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KataMachineLearning
{
    class ArtificialNeuralNetwork
    {
        private Func<double, double> _function = Activation.SoftStep;
        private Activation.ActivationType _at = Activation.ActivationType.SoftStep;
        private readonly int[] _layerNodes;
        private readonly Matrix[] _w;

        public Activation.ActivationType ActivationType { get { return _at; } }
        public int[] LayerNodes { get { return _layerNodes; } }
        public Matrix[] Weights { get { return _w; } }


        public ArtificialNeuralNetwork(int[] layerNodes, Matrix[] weightMatrices)
        {
            _layerNodes = layerNodes;
            _w = new Matrix[_layerNodes.Length - 1];

            for (var i = 0; i < weightMatrices.Length; ++i)
            {
                _w[i] = weightMatrices[i];
            }
        }

        public void SetActivationType(Activation.ActivationType at)
        {
            _at = at;

            switch (at)
            {
                case Activation.ActivationType.SoftStep:
                    _function = Activation.SoftStep;
                    break;

                default:
                    break;
            }
        }

        public double[,] Calculate(double[,] values)
        {
            var x = CreateMatrix(values);
            Matrix z = null;

            for (var i = 0; i < _w.Length; ++i)
            {
                // forward progation algorithm 
                z = x*_w[i];
                z.Apply(_function);
                x = z;
            }

            var v = GetValues(z);
            return v;
        }

        private static Matrix CreateMatrix(double[,] values)
        {
            var m = new Matrix(values.GetLength(0), values.GetLength(1));

            for (var j = 0; j < m.Columns; ++j)
            {
                for (var i = 0; i < m.Rows; ++i)
                {
                    m[i, j] = values[i, j];
                }
            }

            return m;
        }

        private static double[,] GetValues(Matrix m)
        {
            var v = new double[m.Rows,m.Columns];

            for (var j = 0; j < m.Columns; ++j)
            {
                for (var i = 0; i < m.Rows; ++i)
                {
                    v[i, j] = m[i, j];
                }
            }

            return v;
        }
    }
}
