﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataMotionControl
{
    public struct DriveSettings
    {
        public double IncrementPerMillimeterResolution;
        public double SpeedMinimumInIncrementsPerMilliSecond;
        public double SpeedMaximumInIncrementsPerMilliSecond;
        public double AccelerationMinimumInIncrementsPerMilliSecond2;
        public double AccelerationMaximumInIncrementsPerMilliSecond2;
    }
}
