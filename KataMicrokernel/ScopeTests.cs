﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataMicrokernel
{
    [TestFixture]
    public class ScopeTests
    {
        private Scope _scope;

        [SetUp]
        public void Setup()
        {
            _scope = new Scope();
        }

        [TearDown]
        public void Teardown()
        {
            _scope = null;
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(Scope), _scope);
        }

        [Test]
        public void Add_WithTypeNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _scope.Add(null, 0));
        }

        [Test]
        public void Add_WithInstanceNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() =>_scope.Add(typeof(int), null));
        }

        [Test]
        public void Add_WithTypeAndInstance()
        {
            _scope.Add(typeof(int), 0);
            Assert.Pass();
        }

        [Test]
        public void Find_WithTypeAndInstance_ReturnInstance()
        {
            _scope.Add(typeof(int), 0);
            var instance = Scope.Find(_scope, typeof(int));
            Assert.IsInstanceOf(typeof(int), instance);
        }

        [Test]
        public void Find_WithNoInstanceCached_ReturnsNull()
        {
            var instance = Scope.Find(_scope, typeof(int));
            Assert.IsNull(instance);
        }

        [Test]
        public void Find_WithChildScope_ReturnInstance()
        {
            _scope.Add(typeof(int), 0);
            var childScope = new Scope(_scope);
            var instance = Scope.Find(childScope, typeof(int));
            Assert.IsInstanceOf(typeof(int), instance);
        }

        [Test]
        public void Find_With3LevelsOfScope_ReturnInstance()
        {
            _scope.Add(typeof(int), 0);
            var scope01 = new Scope(_scope);
            scope01.Add(typeof(string), string.Empty);
            var scope02 = new Scope(scope01);
            scope02.Add(typeof(bool), true);
            var instance = Scope.Find(scope02, typeof (int));
            Assert.IsInstanceOf(typeof(int), instance);
        }

    }
}
