﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Linq;
using NUnit.Framework;

namespace KataMicrokernel
{
    [TestFixture]
    public class BindingsTests
    {
        private Bindings _bindings;

        [SetUp]
        public void Setup()
        {
            _bindings = new Bindings();
        }

        [TearDown]
        public void Teardown()
        {
            _bindings = null;
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(Bindings), _bindings);
        }

        [Test]
        public void Add_WithAbstractTypeNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _bindings.Add(null, typeof(TestImplementation)));
        }

        [Test]
        public void Add_WithImplementationTypeNull_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _bindings.Add(typeof(ITestInterface), null));
        }

        [Test]
        public void Add_WithAbstractTypeAndImplementationTypeAreSame_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() =>_bindings.Add(typeof(ITestInterface), typeof(ITestInterface)));
        }

        [Test]
        public void Add_WithAbstractTypeAndImplementationTypeIsNotAssignable_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => _bindings.Add(typeof(ITestInterface), typeof(ConcreteClass)));
        }

        [Test]
        public void Add_WithAbstractTypeAndImplementationTypeSwapped_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => _bindings.Add(typeof(TestImplementation), typeof(ITestInterface)));
        }

        [Test]
        public void Add_WithInterfaceTypeAndImplementation_ReturnsCorrectType()
        {
            _bindings.Add(typeof(ITestInterface), typeof(TestImplementation));
            var implementationTypes = _bindings.GetImplementationTypes(typeof(ITestInterface));
            Assert.AreEqual(1, implementationTypes.Count());
            Assert.AreEqual(typeof(TestImplementation), implementationTypes.First());
        }

        [Test]
        public void Add_WithAbstractTypeAndConcreteType_ReturnsCorrectType()
        {
            _bindings.Add(typeof(AbstractBase), typeof(ConcreteClass));
            var implementationTypes = _bindings.GetImplementationTypes(typeof(AbstractBase));
            Assert.AreEqual(1, implementationTypes.Count());
            Assert.AreEqual(typeof(ConcreteClass), implementationTypes.First());
        }

        [Test]
        public void Add_WithInterfaceTypeAndImplementationTwice_ReturnsCorrectType()
        {
            _bindings.Add(typeof(ITestInterface), typeof(TestImplementation));
            _bindings.Add(typeof(ITestInterface), typeof(TestImplementation));
            var implementationTypes = _bindings.GetImplementationTypes(typeof(ITestInterface));
            Assert.AreEqual(1, implementationTypes.Count());
            Assert.AreEqual(typeof(TestImplementation), implementationTypes.First());
        }

        [Test]
        public void Add_WithInterfaceAndTwoImplementations_ReturnsCorrectTypes()
        {
            _bindings.Add(typeof(ITestInterface), typeof(TestImplementation));
            _bindings.Add(typeof(ITestInterface), typeof(ConcreteTestImplementation));
            var implementationTypes = _bindings.GetImplementationTypes(typeof(ITestInterface));
            Assert.AreEqual(2, implementationTypes.Count());
            Assert.AreEqual(typeof(TestImplementation), implementationTypes.ElementAt(0));
            Assert.AreEqual(typeof(ConcreteTestImplementation), implementationTypes.ElementAt(1));
        }

        [Test]
        public void Add_WithDerivedType_ReturnsCorrectType()
        {
            _bindings.Add(typeof(ITestInterface), typeof(DerivedConcreteTestImplementation));
            var implementationTypes = _bindings.GetImplementationTypes(typeof(ITestInterface));
            Assert.AreEqual(1, implementationTypes.Count());
            Assert.AreEqual(typeof(DerivedConcreteTestImplementation), implementationTypes.First());
        }

        [Test]
        public void Add_WithAbstractClassAsImplementation_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => _bindings.Add(typeof(ITestInterface), typeof(AbstractTestInterface)));
        }

        [Test]
        public void Add_WithInterfaceAsImplementation_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => _bindings.Add(typeof(ITestInterface), typeof(ICombinedInterface)));
        }

        [Test]
        public void UpdateBinding_WithModifiedBinding()
        {
            var binding = _bindings.Add(typeof(ITestInterface), typeof(TestImplementation));
            var newBinding = new Binding(binding);
            newBinding.ScopeHashCode = 1234;
            _bindings.UpdateBinding(binding, newBinding);
            Assert.Pass();
        }

        [Test]
        public void GetBindingForImplementationType_CorrectBinding()
        {
            var binding1 = _bindings.Add(typeof(ITestInterface), typeof(TestImplementation));
            var binding2 = _bindings.Add(typeof(ITestInterface), typeof(ConcreteTestImplementation));
            var binding3 = _bindings.GetBindingForImplementationType(typeof(TestImplementation));
            Assert.AreEqual(binding1, binding3);
            var binding4 = _bindings.GetBindingForImplementationType(typeof(ConcreteTestImplementation));
            Assert.AreEqual(binding2, binding4);
        }
    }

    public interface ITestInterface
    {
        string Hello();
    }

    public abstract class AbstractBase
    {
        public abstract string Hello();
    }

    public class TestImplementation : ITestInterface
    {
        public string Hello()
        {
            return "Hello World!";
        }
    }

    public class ConcreteClass : AbstractBase
    {
        public override string Hello()
        {
            return "Hello World!";
        }
    }

    public class ConcreteTestImplementation : AbstractBase, ITestInterface
    {
        public override string Hello()
        {
            return "Hello World!";
        }
    }

    public class DerivedConcreteTestImplementation : ConcreteTestImplementation
    {
    }

    public abstract class AbstractTestInterface : ITestInterface
    {
        public virtual string Hello()
        {
            throw new NotImplementedException();
        }
    }

    public interface ICombinedInterface : ITestInterface
    {
        string World();
    }
}
