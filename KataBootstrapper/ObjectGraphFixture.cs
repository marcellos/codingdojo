﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Ninject;
using Ninject.Activation;

namespace KataBootstrapper
{
    [TestFixture]
    public class ObjectGraphFixture
    {
        private IKernel _kernel;

        [SetUp]
        public void Setup()
        {
            _kernel = new StandardKernel();
        }

        [TearDown]
        public void Teardown()
        {
            _kernel.Dispose();
            _kernel = null;
        }

        private static int idCount;
        [Test]
        public void InstanceTest()
        {
            _kernel.Bind<int>().ToMethod(_ => idCount++);
            _kernel.Bind<Child>().ToSelf();
            _kernel.Bind<IParent>().To<Parent>()
                .WithConstructorArgument("children",
                                        _ =>
                                        {
                                            if (idCount == 1)
                                                return new List<Child>() { new Child(3), new Child(4) };
                                            return new List<Child>() { _kernel.Get<Child>() };
                                        });

            //_kernel.Bind<Child>().ToProvider<ChildProvider>();
            //_kernel.Bind<IParent>().To<Parent>();

            var parent = _kernel.Get<IParent>();
            Console.WriteLine("parent.Id: {0}", parent.Id);
            foreach (var child in parent.Children)
            {
                Console.WriteLine("\tchild.Id: {0}", child.Id);
            }

            var parent1 = _kernel.Get<IParent>();
            Console.WriteLine("parent.Id: {0}", parent1.Id);
            foreach (var child in parent1.Children)
            {
                Console.WriteLine("\tchild.Id: {0}", child.Id);
            }
        }

        interface IHasId
        {
            int Id { get; }
        }

        abstract class IdAware : IHasId
        {
            public int Id { get; private set; }

            protected IdAware(int id)
            {
                Id = id;
            }
        }

        interface IParent : IHasId
        {
            IEnumerable<Child> Children { get; }
        }

        class Parent : IdAware, IParent
        {
            private readonly IEnumerable<Child> _children;
            public IEnumerable<Child> Children { get { return _children.ToList(); } }

            public Parent(int id, IEnumerable<Child> children)
                : base(id)
            {
                _children = children;
            }
        }

        class Child : IdAware
        {
            public Child(int id)
                : base(id)
            {
            }
        }

        //class ChildProvider : Provider<Child>
        //{
        //    protected override Child CreateInstance(IContext context)
        //    {
        //        return new Child(42);
        //    }
        //}

    }
}
