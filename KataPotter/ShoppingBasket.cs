﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;

namespace KataPotter
{
    class ShoppingBasket : ICloneable
    {
        private readonly IList<Item> _items = new List<Item>();
        public IEnumerable<Item> Items { get { return _items; } }

        public void AddItem(Item item)
        {
            _items.Add(item);
        }

        public void RemoveItem(Item item)
        {
            _items.Remove(item);
        }

        public object Clone()
        {
            var clone = new ShoppingBasket();
            foreach (var item in Items)
            {
                clone.AddItem(new Item { Name = item.Name, Price = item.Price });
            }

            return clone;
        }
    }
}
