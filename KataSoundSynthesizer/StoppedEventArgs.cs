﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataSoundSynthesizer
{
    class StoppedEventData : EventArgs
    {
        public Exception Error { get; private set; }

        public StoppedEventData(Exception error = null)
        {
            Error = error;
        }
    }
}
