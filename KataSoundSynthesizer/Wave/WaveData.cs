﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

namespace KataSoundSynthesizer.Wave
{
    class WaveData
    {
        public short Channels { get; private set; }
        public int SampleRate { get; private set; }
        public short BitsPerSample { get; private set; }
        public byte[] Data { get; private set; }

        public WaveData(short channels, int sampleRate, short bitsPerSample, byte[] data)
        {
            Channels = channels;
            SampleRate = sampleRate;
            BitsPerSample = bitsPerSample;
            Data = data;
        }
    }
}
