﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace KataCarcassonne
{
    [TestFixture]
    public class TileTestFixture
    {
        private Tile _tile;

        [SetUp]
        public void Setup()
        {
            _tile = new Tile();
        }

        [TearDown]
        public void Teardown()
        {
            _tile = null;
        }

        [Test]
        public void TestTileInstance()
        {
            Assert.IsInstanceOf<Tile>(_tile);
        }

        [Test]
        public void TestTileDirection()
        {
            Assert.AreEqual(DirectionEnum.Up, _tile.Direction);
        }

        [Test]
        public void TestSetNeighbourUp()
        {
            var tile = new Tile();
            Assert.IsFalse(_tile.IsConnected);
            Tile.SetNeighbour(_tile, tile, DirectionEnum.Up);
            Assert.AreEqual(tile, _tile.Up);
            Assert.AreEqual(_tile, tile.Down);
            Assert.IsTrue(_tile.IsConnected);
        }

        [Test]
        public void TestSetNeighbourRight()
        {
            var tile = new Tile();
            Assert.IsFalse(_tile.IsConnected);
            Tile.SetNeighbour(_tile, tile, DirectionEnum.Right);
            Assert.AreEqual(tile, _tile.Right);
            Assert.AreEqual(_tile, tile.Left);
            Assert.IsTrue(_tile.IsConnected);
        }

        [Test]
        public void TestSetNeighbourDown()
        {
            var tile = new Tile();
            Assert.IsFalse(_tile.IsConnected);
            Tile.SetNeighbour(_tile, tile, DirectionEnum.Down);
            Assert.AreEqual(tile, _tile.Down);
            Assert.AreEqual(_tile, tile.Up);
            Assert.IsTrue(_tile.IsConnected);
        }

        [Test]
        public void TestSetNeighbourLeft()
        {
            var tile = new Tile();
            Assert.IsFalse(_tile.IsConnected);
            Tile.SetNeighbour(_tile, tile, DirectionEnum.Left);
            Assert.AreEqual(tile, _tile.Left);
            Assert.AreEqual(_tile, tile.Right);
            Assert.IsTrue(_tile.IsConnected);
        }

        [Test]
        public void TestAddPropertiesUp()
        {
            var areas = new Dictionary<int, TileArea>();
            areas.Add(1, new TileArea { Name = "1" });
            areas.Add(2, new TileArea { Name = "2" });
            _tile.AddAreas(areas, DirectionEnum.Up);
            Assert.AreEqual(areas, _tile.SideUp);
            var propsList = areas.Select(p => p.Value).ToList();
            Assert.AreEqual(propsList, _tile.TileAreas);
        }

        [Test]
        public void TestAddPropertiesUpRight()
        {
            var areaUp = new Dictionary<int, TileArea>();
            areaUp.Add(1, new TileArea { Name = "1" });
            areaUp.Add(2, new TileArea { Name = "2" });
            var areaRight = new Dictionary<int, TileArea>();
            areaRight.Add(1, new TileArea { Name = "1" });
            _tile.AddAreas(areaUp, DirectionEnum.Up);
            Assert.AreEqual(areaUp, _tile.SideUp);
            _tile.AddAreas(areaRight, DirectionEnum.Right);
            Assert.AreEqual(areaRight, _tile.SideRight);
            var areaList = areaUp.Select(p => p.Value).ToList();
            Assert.AreEqual(areaList, _tile.TileAreas);
        }

        [Test]
        public void TestAddPropertiesUpRightRotate()
        {
            var areaUp = new Dictionary<int, TileArea>();
            areaUp.Add(1, new TileArea { Name = "1" });
            areaUp.Add(2, new TileArea { Name = "2" });
            var areaRight = new Dictionary<int, TileArea>();
            areaRight.Add(1, new TileArea { Name = "1" });
            _tile.AddAreas(areaUp, DirectionEnum.Up);
            _tile.AddAreas(areaRight, DirectionEnum.Right);
            _tile.Rotate(DirectionEnum.Right);
            Assert.AreEqual(DirectionEnum.Right, _tile.Direction);
            Assert.AreEqual(areaUp, _tile.SideRight);
            Assert.AreEqual(areaRight, _tile.SideDown);
            var areaList = areaUp.Select(p => p.Value).ToList();
            Assert.AreEqual(areaList, _tile.TileAreas);
        }

        [Test]
        public void TestIsNeighbourMatchUpDownTrue()
        {
            // prepare tile 1
            var areaUp = new Dictionary<int, TileArea>();
            areaUp.Add(1, new TileArea { Name = "1" });
            areaUp.Add(2, new TileArea { Name = "2" });
            _tile.AddAreas(areaUp, DirectionEnum.Up);

            // prepare tile 2
            var tile = new Tile();
            var areaDown = new Dictionary<int, TileArea>();
            areaDown.Add(1, new TileArea { Name = "2" });
            areaDown.Add(2, new TileArea { Name = "11" });
            tile.AddAreas(areaDown, DirectionEnum.Down);

            Assert.IsTrue(Tile.IsNeighbourMatch(_tile, tile, DirectionEnum.Up));
        }

        [Test]
        public void TestIsNeighbourMatchUpDownFalse()
        {
            // prepare tile 1
            var areaUp = new Dictionary<int, TileArea>();
            areaUp.Add(1, new TileArea { Name = "1" });
            areaUp.Add(2, new TileArea { Name = "2" });
            _tile.AddAreas(areaUp, DirectionEnum.Up);

            // prepare tile 2
            var tile = new Tile();
            var areaDown = new Dictionary<int, TileArea>();
            areaDown.Add(1, new TileArea { Name = "1" });
            areaDown.Add(2, new TileArea { Name = "2" });
            tile.AddAreas(areaDown, DirectionEnum.Down);

            Assert.IsFalse(Tile.IsNeighbourMatch(_tile, tile, DirectionEnum.Up));
        }

        [Test]
        public void TestIsNeighbourMatchRightLeftTrue()
        {
            // prepare tile 1
            var areaRight = new Dictionary<int, TileArea>();
            areaRight.Add(1, new TileArea { Name = "1" });
            areaRight.Add(2, new TileArea { Name = "2" });
            _tile.AddAreas(areaRight, DirectionEnum.Right);

            // prepare tile 2
            var tile = new Tile();
            var areaLeft = new Dictionary<int, TileArea>();
            areaLeft.Add(1, new TileArea { Name = "2" });
            areaLeft.Add(2, new TileArea { Name = "1" });
            tile.AddAreas(areaLeft, DirectionEnum.Left);

            Assert.IsTrue(Tile.IsNeighbourMatch(_tile, tile, DirectionEnum.Right));
        }

        [Test]
        public void TestIsPropertyEndpointTrue()
        {
            var areaUp = new Dictionary<int, TileArea>();
            areaUp.Add(1, new TileArea { Name = "1" });
            areaUp.Add(2, new TileArea { Name = "2" });
            _tile.AddAreas(areaUp, DirectionEnum.Up);

            Assert.IsTrue(Tile.IsAreaEndpoint(_tile, areaUp[1]));
        }

        [Test]
        public void TestIsPropertyEndpointFalse()
        {
            var areaUp = new Dictionary<int, TileArea>();
            areaUp.Add(1, new TileArea { Name = "1" });
            areaUp.Add(2, new TileArea { Name = "2" });
            _tile.AddAreas(areaUp, DirectionEnum.Up);

            var areaRight = new Dictionary<int, TileArea>();
            areaRight.Add(1, new TileArea { Name = "1" });
            _tile.AddAreas(areaRight, DirectionEnum.Right);

            Assert.IsFalse(Tile.IsAreaEndpoint(_tile, areaUp[1]));
            Assert.IsTrue(Tile.IsAreaEndpoint(_tile, areaUp[2]));
        }
    }
}
