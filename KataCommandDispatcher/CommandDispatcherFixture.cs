﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace KataCommandDispatcher
{
    [TestFixture]
    public class CommandDispatcherFixture
    {
        private IDependencyResolver _dependencyResolver;
        private ICommandQueue _commandQueue;
        private ICommandDispatcher _dispatcher;

        [SetUp]
        public void Setup()
        {
            _dependencyResolver = new MockResolver();
            _commandQueue = new CommandQueue<ICommand>(_dependencyResolver);
            _dispatcher = new CommandDispatcher(_commandQueue);
        }

        [TearDown]
        public void Teardown()
        {
            try
            {
                ((CommandDispatcher)_dispatcher).Shutdown();
            }
            catch (AggregateException ex)
            {
                Console.WriteLine(ex);
            }

            _dispatcher = null;
            _commandQueue = null;
            _dispatcher = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(CommandQueue<ICommand>), _commandQueue);
            Assert.IsInstanceOf(typeof(CommandDispatcher), _dispatcher);
            System.Threading.Thread.Sleep(50);
        }

        [Test]
        public void QueueNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => new CommandDispatcher(null));
        }

        [Test]
        public void EnqueueCommandTest()
        {
            var command = new ConsoleCommand("console command test");
            var resultSuccess = false;
            Exception resultException = null;
            command.ResultHandler = result => resultSuccess = result.Success;
            command.ExceptionHandler = ex => resultException = ex;
            _commandQueue.Enqueue(command);
            System.Threading.Thread.Sleep(50);
            Assert.IsTrue(resultSuccess);
            Assert.IsNull(resultException);
        }

        [Test]
        public void BadCommandHandlerTest()
        {
            var q = new CommandQueue<ICommand>(new BadCommandHandlerResolver());
            var d = new CommandDispatcher(q);
            var command = new TestCommand();
            var resultSuccess = true;
            Exception resultException = null;
            command.ResultHandler = result => resultSuccess = result.Success;
            command.ExceptionHandler = ex => resultException = ex;
            q.Enqueue(command);
            System.Threading.Thread.Sleep(50);
            d.Shutdown();
            Assert.IsTrue(resultSuccess);
            Assert.IsInstanceOf(typeof(NotImplementedException), resultException);
        }

        [Test]
        public void NullResultTest()
        {
            var q = new CommandQueue<ICommand>(new NullResultCommandHandlerResolver());
            var d = new CommandDispatcher(q);
            var command = new TestCommand();
            var resultSuccess = true;
            Exception resultException = null;
            command.ResultHandler = result => resultSuccess = result.Success;
            command.ExceptionHandler = ex => resultException = ex;
            q.Enqueue(command);
            System.Threading.Thread.Sleep(50);
            d.Shutdown();
            Assert.IsFalse(resultSuccess);
            Assert.IsNull(resultException);
        }

        private class MockResolver : IDependencyResolver
        {
            public IEnumerable<object> GetAll<T>()
            {
                return new List<object>() { new ConsoleCommandHandler() };
            }
        }

        private class TestCommand : Command
        {
        }

        private class BadCommandHandler : ICommandHandler<TestCommand>
        {
            public ICommandResult Execute(ICommand command)
            {
                throw new NotImplementedException();
            }
        }

        private class BadCommandHandlerResolver : IDependencyResolver
        {
            public IEnumerable<object> GetAll<T>()
            {
                return new List<object>() { new BadCommandHandler() };
            }
        }

        private class NullResultCommandHandler : ICommandHandler<TestCommand>
        {
            public ICommandResult Execute(ICommand command)
            {
                return null;
            }
        }

        private class NullResultCommandHandlerResolver : IDependencyResolver
        {
            public IEnumerable<object> GetAll<T>()
            {
                return new List<object>() { new NullResultCommandHandler() };
            }
        }
    }
}
