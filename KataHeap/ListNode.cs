﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataHeap
{
    public class ListNode<T>
    {
        public ListNode<T> Next { get; set; }

        public T Key { get; private set; }

        public ListNode(T key)
        {
            Key = key;
        }
    }
}
