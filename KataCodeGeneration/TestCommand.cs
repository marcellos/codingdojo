﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataCodeGeneration
{
    public class TestCommand : ITestCommand
    {
        public int Value { get; private set; }

        public TestCommand(int value)
        {
            if (value < -10 || value > 30)
            {
                throw new ArgumentOutOfRangeException("value", value, "valid range: [-10,30]");
            }

            Value = value;
        }
    }
}
