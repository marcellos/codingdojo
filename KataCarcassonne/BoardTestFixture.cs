﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace KataCarcassonne
{
    [TestFixture]
    public class BoardTestFixture
    {
        private Board _board;

        [SetUp]
        public void Setup()
        {
            _board = new Board();
        }

        [TearDown]
        public void Teardown()
        {
            _board = null;
        }

        [Test]
        public void TestInstance()
        {
            Assert.IsInstanceOf<Board>(_board);
        }

        [Test]
        public void TestAddTileSingle()
        {
            var areaUp = new Dictionary<int, TileArea>();
            areaUp.Add(1, new TileArea { Name = "1" });
            areaUp.Add(2, new TileArea { Name = "2" });
            var areaRight = new Dictionary<int, TileArea>();
            areaRight.Add(1, new TileArea { Name = "1" });
            var tile = new Tile();
            tile.AddAreas(areaUp, DirectionEnum.Up);
            tile.AddAreas(areaRight, DirectionEnum.Right);
            _board.AddTile(0, 0, tile);
            Assert.AreEqual(1, _board.Tiles.Count());
            Assert.AreEqual(2, _board.AreaTileMaps.Count());
        }

        [Test]
        public void TestAddTileSameCoordinates()
        {
            var tile1 = new Tile();
            _board.AddTile(0, 0, tile1);
            _board.AddTile(0, 0, tile1);
            Assert.AreEqual(1, _board.Tiles.Count());
        }

        [Test]
        public void TestAddTileTwo()
        {
            var areaUp1 = new Dictionary<int, TileArea>();
            areaUp1.Add(1, new TileArea { Name = "1" });
            areaUp1.Add(2, new TileArea { Name = "2" });
            var areaRight1 = new Dictionary<int, TileArea>();
            areaRight1.Add(1, new TileArea { Name = "1" });
            var tile1 = new Tile();
            tile1.AddAreas(areaUp1, DirectionEnum.Up);
            tile1.AddAreas(areaRight1, DirectionEnum.Right);
            _board.AddTile(0, 0, tile1);

            var areaUp2 = new Dictionary<int, TileArea>();
            areaUp2.Add(1, new TileArea { Name = "2" });
            areaUp2.Add(2, new TileArea { Name = "1" });
            var areaLeft2 = new Dictionary<int, TileArea>();
            areaLeft2.Add(1, new TileArea { Name = "1" });
            var tile2 = new Tile();
            tile2.AddAreas(areaUp2, DirectionEnum.Up);
            tile2.AddAreas(areaLeft2, DirectionEnum.Left);
            _board.AddTile(1, 0, tile2);

            Assert.AreEqual(2, _board.Tiles.Count());
            Assert.AreEqual(2, _board.AreaTileMaps.Count());
        }
    }
}
