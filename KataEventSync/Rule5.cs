﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataEventSync
{
    public class Rule5 : Rule<User>
    {
        private bool _isFredOnline, _isMikeOnline;

        protected override object DoEvaluate(User message)
        {
            if (message.Name.Equals("Fred"))
            {
                _isFredOnline = message.IsOnline;
            }

            if (message.Name.Equals("Mike"))
            {
                _isMikeOnline = message.IsOnline;
            }

            if (_isFredOnline && _isMikeOnline)
            {
                return "$rule5";
            }

            return null;
        }
    }
}
