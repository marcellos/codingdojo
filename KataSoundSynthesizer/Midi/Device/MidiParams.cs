﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;

namespace KataSoundSynthesizer.Midi.Device
{
    public struct MidiParams
    {
        public readonly IntPtr Param1;
        public readonly IntPtr Param2;

        public MidiParams(IntPtr param1, IntPtr param2)
        {
            Param1 = param1;
            Param2 = param2;
        }
    }
}
