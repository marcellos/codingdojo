﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataCommandDispatcher
{
    public class CommandResult : ICommandResult
    {
        public bool Success { get; protected set; }

        public CommandResult(bool success = true)
        {
            Success = success;
        }
    }
}
