﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Threading;
using NAudio.Wave;

namespace KataSoundSynthesizer
{
    class NAudioPlayWaveFile
    {
        private ManualResetEvent _waitHandle;

        public void PlayWaveFile(string filename)
        {
            _waitHandle = new ManualResetEvent(false);

            var wfr = new WaveFileReader(filename);
            var waveOut = new WaveOut(NAudio.Wave.WaveCallbackInfo.FunctionCallback());
            waveOut.Init(wfr);
            waveOut.PlaybackStopped += WaveOutPlaybackStopped;
            waveOut.Play();
            var timeout = _waitHandle.WaitOne(5000);
            waveOut.Stop();
            waveOut.Dispose();
            wfr.Dispose();
        }

        private void WaveOutPlaybackStopped(object sender, StoppedEventArgs e)
        {
            Console.WriteLine("playback stopped");
            _waitHandle.Set();
        }
    }
}
