﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataEventSync
{
    public class Rule3 : Rule<string>
    {
        private bool _rule1, _rule2;

        protected override object DoEvaluate(string message)
        {
            _rule1 = _rule1 || message.Equals("$rule1");
            _rule2 = _rule2 || message.Equals("$rule2");
            if (_rule1 && _rule2)
            {
                _rule1 = _rule2 = false;
                return "$rule3";
            }

            return null;
        }
    }
}
