﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System.Collections.Generic;
using System.Linq;
using System;

namespace KataSoundSynthesizer.SynthComponent
{
    class CompositeAdsrEnvelope : SynthComponentBase, IAdsrEnvelope
    {
        private readonly IList<IAdsrEnvelope> _adsrEnvelopes = new List<IAdsrEnvelope>(); 

        public void Trigger(int noteNumber, float velocity)
        {
            foreach (var adsrEnvelope in _adsrEnvelopes)
            {
                adsrEnvelope.Trigger(noteNumber, velocity);
            }
        }

        public void Release(int noteNumber)
        {
            foreach (var adsrEnvelope in _adsrEnvelopes)
            {
                adsrEnvelope.Release(noteNumber);
            }
        }


        public override void RenderSamples(int offset, int count)
        {
            InitialiseMonoBuffer(count);
            Array.Clear(_monoBuffer, 0, _monoBuffer.Length);

            foreach (var adsrEnvelope in _adsrEnvelopes)
            {
                adsrEnvelope.RenderSamples(offset, count);
                var buffer = adsrEnvelope.GetMonoBuffer();
                for (var i = 0; i < buffer.Length; ++i)
                {
                    _monoBuffer[i] += buffer[i];
                }
            }
        }

        public override int SampleRate
        {
            get { return _adsrEnvelopes.First().SampleRate; }
            set
            {
                foreach (var adsrEnvelope in _adsrEnvelopes)
                {
                    adsrEnvelope.SampleRate = value;
                }
            }
        }

        public float AttackTime
        {
            get { return _adsrEnvelopes.First().AttackTime; }
            set
            {
                foreach (var adsrEnvelope in _adsrEnvelopes)
                {
                    adsrEnvelope.AttackTime = value;
                }
            }
        }

        public float DecayTime
        {
            get { return _adsrEnvelopes.First().DecayTime; } 
            set
            {
                foreach (var adsrEnvelope in _adsrEnvelopes)
                {
                    adsrEnvelope.AttackTime = value;
                }
            }
        }

        public float SustainLevel
        {
            get { return _adsrEnvelopes.First().SustainLevel; }
            set
            {
                foreach (var adsrEnvelope in _adsrEnvelopes)
                {
                    adsrEnvelope.SustainLevel = value;
                }
            }
        }

        public float ReleaseTime
        {
            get { return _adsrEnvelopes.First().ReleaseTime; }
            set
            {
                foreach (var adsrEnvelope in _adsrEnvelopes)
                {
                    adsrEnvelope.ReleaseTime = value;
                }
            }
        }

        public float VelocitySensitivity
        {
            get { return _adsrEnvelopes.First().VelocitySensitivity; }
            set
            {
                foreach (var adsrEnvelope in _adsrEnvelopes)
                {
                    adsrEnvelope.VelocitySensitivity = value;
                }
            }
        }

        public TriggerModeEnum TriggerMode
        {
            get { return _adsrEnvelopes.First().TriggerMode; } 
            set
            {
                foreach (var adsrEnvelope in _adsrEnvelopes)
                {
                    adsrEnvelope.TriggerMode = value;
                }
            }
        }

        public float SlewTime
        {
            get { return _adsrEnvelopes.First().SlewTime; }
            set
            {
                foreach (var adsrEnvelope in _adsrEnvelopes)
                {
                    adsrEnvelope.SlewTime = value;
                }
            }
        }

        public override ISynthComponent MakeInstanceCopy()
        {
            var cae = new CompositeAdsrEnvelope();

            foreach (var env in _adsrEnvelopes)
            {
                cae.AddAdsrEnvelope((IAdsrEnvelope)env.MakeInstanceCopy());
            }

            return cae;
        }

        public void AddAdsrEnvelope(IAdsrEnvelope adsrEnvelope)
        {
            if (_adsrEnvelopes.Contains(adsrEnvelope))
            {
                return;
            }

            _adsrEnvelopes.Add(adsrEnvelope);
        }
    }
}
