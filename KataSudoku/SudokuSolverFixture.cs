﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace KataSudoku
{
    [TestFixture]
    public class SudokuSolverFixture
    {
        private SudokuSolver _solver;

        [SetUp]
        public void Setup()
        {
            _solver = new SudokuSolver();
        }

        [TearDown]
        public void Teardown()
        {
            _solver = null;
        }

        [Test]
        public void SolveTest()
        {
            var puzzle = CreatePuzzle1();
            PrintPuzzle(puzzle);
            var solvedPuzzles = _solver.Solve(puzzle);
            //PrintPuzzle(CreateSolution1());
            foreach (var solvedPuzzle in solvedPuzzles)
            {
                PrintPuzzle(solvedPuzzle);
            }

            Assert.AreEqual(CreateSolution1(), solvedPuzzles.FirstOrDefault());
        }

        [Test]
        public void GetRowFromIndexTest()
        {
            Assert.AreEqual(1, SudokuSolver.GetRowFromIndex(10));
            Assert.AreEqual(8, SudokuSolver.GetRowFromIndex(78));
        }

        [Test]
        public void GetColumnFromIndexTest()
        {
            Assert.AreEqual(1, SudokuSolver.GetColumnFromIndex(1));
            Assert.AreEqual(7, SudokuSolver.GetColumnFromIndex(70));
        }

        [Test]
        public void GetSquareFromIndexTest()
        {
            Assert.AreEqual(0, SudokuSolver.GetSquareFromIndex(18));
            Assert.AreEqual(4, SudokuSolver.GetSquareFromIndex(41));
        }

        [Test]
        public void ColumnContainsNumberTest()
        {
            var puzzle = CreatePuzzle1();
            Assert.IsTrue(SudokuSolver.ColumnContainsNumber(puzzle, 4, 2));
            Assert.IsTrue(SudokuSolver.ColumnContainsNumber(puzzle, 7, 7));
            Assert.IsFalse(SudokuSolver.ColumnContainsNumber(puzzle, 3, 5));
        }

        [Test]
        public void RowContainsNumberTest()
        {
            var puzzle = CreatePuzzle1();
            Assert.IsTrue(SudokuSolver.RowContainsNumber(puzzle, 3, 6));
            Assert.IsTrue(SudokuSolver.RowContainsNumber(puzzle, 6, 2));
            Assert.IsFalse(SudokuSolver.RowContainsNumber(puzzle, 2, 1));
        }

        [Test]
        public void SquareContainsNumberTest()
        {
            var puzzle = CreatePuzzle1();
            Assert.IsTrue(SudokuSolver.SquareContainsNumber(puzzle, 0, 9));
            Assert.IsTrue(SudokuSolver.SquareContainsNumber(puzzle, 5, 1));
            Assert.IsFalse(SudokuSolver.SquareContainsNumber(puzzle, 8, 3));
        }

        [Test]
        public void GetNextFreeIndexTest()
        {
            //var current = 0;
            //while (current < (current = GetNextFreeIndex(puzzle, current)))
            //    Console.WriteLine(current + " ");

            var puzzle = CreatePuzzle1();
            Assert.AreEqual(15, SudokuSolver.GetNextFreeIndex(puzzle, 11));
            Assert.AreEqual(0, SudokuSolver.GetNextFreeIndex(puzzle, -1)); // edge case 0
            Assert.AreEqual(80, SudokuSolver.GetNextFreeIndex(puzzle, 80)); // edge case 80
        }


        private static void PrintPuzzle(IEnumerable<int> puzzle)
        {
            for (var i = 0; i < 9; i++)
            {
                for (var j = 0; j < 9; j++)
                {
                    Console.Write(puzzle.ElementAt(i * 9 + j) + " ");
                }

                Console.Write("\r\n");
            }

            Console.WriteLine();
        }

        private static IEnumerable<int> CreatePuzzle1()
        {
            var puzzle = new List<int>(9 * 9);
            for (var i = 0; i < 9 * 9; i++)
            {
                puzzle.Add(0);
            }

            puzzle[1] = 3;
            puzzle[12] = 1;
            puzzle[13] = 9;
            puzzle[14] = 5;
            puzzle[19] = 9;
            puzzle[20] = 8;
            puzzle[25] = 6;
            puzzle[27] = 8;
            puzzle[31] = 6;
            puzzle[36] = 4;
            puzzle[41] = 3;
            puzzle[44] = 1;
            puzzle[49] = 2;
            puzzle[55] = 6;
            puzzle[60] = 2;
            puzzle[61] = 8;
            puzzle[66] = 4;
            puzzle[67] = 1;
            puzzle[68] = 9;
            puzzle[71] = 5;
            puzzle[79] = 7;
            return puzzle;
        }

        private static IEnumerable<int> CreateSolution1()
        {
            var puzzle = new List<int>(9 * 9);
            for (var i = 0; i < 9 * 9; i++)
            {
                puzzle.Add(0);
            }

            puzzle[0] = 5;
            puzzle[1] = 3;
            puzzle[2] = 4;
            puzzle[3] = 6;
            puzzle[4] = 7;
            puzzle[5] = 8;
            puzzle[6] = 9;
            puzzle[7] = 1;
            puzzle[8] = 2;

            puzzle[9] = 6;
            puzzle[10] = 7;
            puzzle[11] = 2;
            puzzle[12] = 1;
            puzzle[13] = 9;
            puzzle[14] = 5;
            puzzle[15] = 3;
            puzzle[16] = 4;
            puzzle[17] = 8;

            puzzle[18] = 1;
            puzzle[19] = 9;
            puzzle[20] = 8;
            puzzle[21] = 3;
            puzzle[22] = 4;
            puzzle[23] = 2;
            puzzle[24] = 5;
            puzzle[25] = 6;
            puzzle[26] = 7;

            puzzle[27] = 8;
            puzzle[28] = 5;
            puzzle[29] = 9;
            puzzle[30] = 7;
            puzzle[31] = 6;
            puzzle[32] = 1;
            puzzle[33] = 4;
            puzzle[34] = 2;
            puzzle[35] = 3;

            puzzle[36] = 4;
            puzzle[37] = 2;
            puzzle[38] = 6;
            puzzle[39] = 8;
            puzzle[40] = 5;
            puzzle[41] = 3;
            puzzle[42] = 7;
            puzzle[43] = 9;
            puzzle[44] = 1;

            puzzle[45] = 7;
            puzzle[46] = 1;
            puzzle[47] = 3;
            puzzle[48] = 9;
            puzzle[49] = 2;
            puzzle[50] = 4;
            puzzle[51] = 8;
            puzzle[52] = 5;
            puzzle[53] = 6;

            puzzle[54] = 9;
            puzzle[55] = 6;
            puzzle[56] = 1;
            puzzle[57] = 5;
            puzzle[58] = 3;
            puzzle[59] = 7;
            puzzle[60] = 2;
            puzzle[61] = 8;
            puzzle[62] = 4;

            puzzle[63] = 2;
            puzzle[64] = 8;
            puzzle[65] = 7;
            puzzle[66] = 4;
            puzzle[67] = 1;
            puzzle[68] = 9;
            puzzle[69] = 6;
            puzzle[70] = 3;
            puzzle[71] = 5;

            puzzle[72] = 3;
            puzzle[73] = 4;
            puzzle[74] = 5;
            puzzle[75] = 2;
            puzzle[76] = 8;
            puzzle[77] = 6;
            puzzle[78] = 1;
            puzzle[79] = 7;
            puzzle[80] = 9;
            return puzzle;
        }

    }
}
