﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace KataCarcassonne
{
    [TestFixture]
    public class BoardAreaFixture
    {
        private Board _board;

        [SetUp]
        public void Setup()
        {
            _board = new Board();
        }

        [TearDown]
        public void Teardown()
        {
            _board = null;
        }

        [Test]
        public void DetectTwoTilesClosed()
        {
            _board.AddTile(0, 0, CreateTileCityRight());
            _board.AddTile(1, 0, CreateTileCityLeft());

            var firstOrDefault = _board.Tiles.Where(t => t.Item1.Equals(0) && t.Item2.Equals(0)).FirstOrDefault();
            if (firstOrDefault != null)
            {
                var tile = firstOrDefault.Item3;
                var area = tile.TileAreas.Where(p => p.Name.Equals("city")).FirstOrDefault();
                Assert.IsTrue(Board.IsAreaClosed(_board, tile, area));
            }
        }

        [Test]
        public void DetectTwoTilesOpen()
        {
            _board.AddTile(0, 0, CreateTileCityRight());
            _board.AddTile(1, 0, CreateTileCityLeftRight());

            var firstOrDefault = _board.Tiles.Where(t => t.Item1.Equals(0) && t.Item2.Equals(0)).FirstOrDefault();
            if (firstOrDefault != null)
            {
                var tile = firstOrDefault.Item3;
                var area = tile.TileAreas.Where(p => p.Name.Equals("city")).FirstOrDefault();
                Assert.IsFalse(Board.IsAreaClosed(_board, tile, area));
            }
        }

        [Test]
        public void DetectTwoTilesPropNotConnected()
        {
            _board.AddTile(0, 0, CreateTileCityLeft());
            _board.AddTile(1, 0, CreateTileCityRight());

            var firstOrDefault = _board.Tiles.Where(t => t.Item1.Equals(0) && t.Item2.Equals(0)).FirstOrDefault();
            if (firstOrDefault != null)
            {
                var tile = firstOrDefault.Item3;
                var area = tile.TileAreas.Where(p => p.Name.Equals("city")).FirstOrDefault();
                Assert.IsFalse(Board.IsAreaClosed(_board, tile, area));
            }
        }

        [Test]
        public void DetectTwoTilesOnePropClosed()
        {
            _board.AddTile(0, 0, CreateTileCityRight());
            _board.AddTile(1, 0, CreateTileCityLeftRightNoJunction());

            var firstOrDefault = _board.Tiles.Where(t => t.Item1.Equals(0) && t.Item2.Equals(0)).FirstOrDefault();
            if (firstOrDefault != null)
            {
                var tile = firstOrDefault.Item3;
                var area = tile.TileAreas.Where(p => p.Name.Equals("city")).FirstOrDefault();
                Assert.IsTrue(Board.IsAreaClosed(_board, tile, area));
            }
        }

        [Test]
        public void DetectThreeTilesClosed()
        {
            _board.AddTile(0, 0, CreateTileCityRight());
            _board.AddTile(1, 0, CreateTileCityLeftRight());
            _board.AddTile(2, 0, CreateTileCityLeft());

            var firstOrDefault = _board.Tiles.Where(t => t.Item1.Equals(0) && t.Item2.Equals(0)).FirstOrDefault();
            if (firstOrDefault != null)
            {
                var tile = firstOrDefault.Item3;
                var area = tile.TileAreas.Where(p => p.Name.Equals("city")).FirstOrDefault();
                Assert.IsTrue(Board.IsAreaClosed(_board, tile, area));
            }
        }


        private static Tile CreateTileCityRight()
        {
            var areaLawn = new Dictionary<int, TileArea>();
            areaLawn.Add(1, new TileArea { Name = "lawn" });
            var areaCity = new Dictionary<int, TileArea>();
            areaCity.Add(1, new TileArea { Name = "city" });
            var tile = new Tile();
            tile.AddAreas(areaLawn, DirectionEnum.Up);
            tile.AddAreas(areaLawn, DirectionEnum.Left);
            tile.AddAreas(areaLawn, DirectionEnum.Down);
            tile.AddAreas(areaCity, DirectionEnum.Right);
            return tile;
        }

        private static Tile CreateTileCityLeft()
        {
            var areaLawn = new Dictionary<int, TileArea>();
            areaLawn.Add(1, new TileArea { Name = "lawn" });
            var areaCity = new Dictionary<int, TileArea>();
            areaCity.Add(1, new TileArea { Name = "city" });
            var tile = new Tile();
            tile.AddAreas(areaLawn, DirectionEnum.Up);
            tile.AddAreas(areaLawn, DirectionEnum.Right);
            tile.AddAreas(areaLawn, DirectionEnum.Down);
            tile.AddAreas(areaCity, DirectionEnum.Left);
            return tile;
        }

        private static Tile CreateTileCityLeftRight()
        {
            var areaLawn = new Dictionary<int, TileArea>();
            areaLawn.Add(1, new TileArea { Name = "lawn" });
            var areaCity = new Dictionary<int, TileArea>();
            areaCity.Add(1, new TileArea { Name = "city" });
            var tile = new Tile();
            tile.AddAreas(areaLawn, DirectionEnum.Up);
            tile.AddAreas(areaLawn, DirectionEnum.Down);
            tile.AddAreas(areaCity, DirectionEnum.Left);
            tile.AddAreas(areaCity, DirectionEnum.Right);
            return tile;
        }

        private static Tile CreateTileCityLeftRightNoJunction()
        {
            var areaLawn = new Dictionary<int, TileArea>();
            areaLawn.Add(1, new TileArea { Name = "lawn" });
            var areaCity1 = new Dictionary<int, TileArea>();
            areaCity1.Add(1, new TileArea { Name = "city1" });
            var areaCity2 = new Dictionary<int, TileArea>();
            areaCity2.Add(1, new TileArea { Name = "city2" });
            var tile = new Tile();
            tile.AddAreas(areaLawn, DirectionEnum.Up);
            tile.AddAreas(areaLawn, DirectionEnum.Down);
            tile.AddAreas(areaCity1, DirectionEnum.Left);
            tile.AddAreas(areaCity2, DirectionEnum.Right);
            return tile;
        }

    }
}
