﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataCompiler.Vm
{
    [TestFixture]
    public class PrimitivePackerTests
    {
        [Test, Ignore("because")]
        public void Byte_Packer()
        {
            Console.WriteLine("sizeof int:{0}, float:{1}, double:{2}, bool:{3}, char:{4}",
                sizeof(int), sizeof(float), sizeof(double), sizeof(bool), sizeof(char));

            var bytes = BitConverter.GetBytes(22.0 / 7.0);
            foreach (var b in bytes)
            {
                Console.WriteLine(b);
            }

            var d = BitConverter.ToDouble(bytes, 0);
            Console.WriteLine(d);

            var i0 = BitConverter.ToInt32(bytes, 0);
            var i1 = BitConverter.ToInt32(bytes, 4);
            Console.WriteLine("{0}, {1}", i0, i1);

            var ba0 = BitConverter.GetBytes(i0);
            var ba1 = BitConverter.GetBytes(i1);
            var ba = new byte[ba0.Length + ba1.Length];

            for (var i = 0; i < ba0.Length; ++i)
            {
                ba[i] = ba0[i];
            }

            for (var i = 0; i < ba1.Length; ++i)
            {
                ba[ba0.Length + i] = ba1[i];
            }

            var d1 = BitConverter.ToDouble(ba, 0);
            Console.WriteLine(d1);
        }

        [Test]
        public void PrimitivePacker_double_byte()
        {
            var p = new PrimitivePacker();
            p.d0 = 22.0 / 7.0;

            // 73 146 36 73 146 36 9 64
            Assert.AreEqual(73, p.b0);
            Assert.AreEqual(146, p.b1);
            Assert.AreEqual(36, p.b2);
            Assert.AreEqual(73, p.b3);
            Assert.AreEqual(146, p.b4);
            Assert.AreEqual(36, p.b5);
            Assert.AreEqual(9, p.b6);
            Assert.AreEqual(64, p.b7);
        }

        [Test]
        public void PrimitivePacker_uint_double()
        {
            // 1227133513 1074341010
            var p = new PrimitivePacker();
            p.ui0 = 1227133513;
            p.ui1 = 1074341010;
            Assert.AreEqual(3.1428571428571428, p.d0);
        }

        [Test]
        public void PrimitivePacker_int_uint()
        {
            // -33
            var p = new PrimitivePacker();
            p.i0 = -33;
            //Console.WriteLine("0x{0:x}", p.ui0);
            Assert.AreEqual(4294967263, p.ui0);
        }

    }
}
