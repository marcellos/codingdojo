﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using NUnit.Framework;

namespace KataSoundSynthesizer.Tone
{
    [TestFixture]
    public class ScaleTest
    {
        [Test]
        public void ToneFrequency_WhenProvidedBaseTone_ThenGetNewTone()
        {
            var scale = new Scale();
            var middleC = scale.ToneFrequency(Scale.A440, Scale.A440ToneIndex, 40);
            Assert.AreEqual(261.626, Math.Round(middleC, 3));
        }

        [Test]
        public void Ctor_WhenNew_ThenGetAllTones()
        {
            var scale = new Scale();
            Assert.AreEqual(220.0, Math.Round(scale.Tones[Scale.A440ToneIndex - 12]));
        }
    }
}
