﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataSoundSynthesizer.SynthComponent
{
    class VoiceWaveSample
    {
        public string WaveSampleFilepath { get; private set; }
        public int NoteNumber { get; private set; }

        public VoiceWaveSample(string waveSampleFilepath, int noteNumber)
        {
            WaveSampleFilepath = waveSampleFilepath;
            NoteNumber = noteNumber;
        }
    }
}
