﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using System.Collections.Generic;
using KataSoundSynthesizer.SynthComponent;

namespace KataSoundSynthesizer.Oscillators
{
    class CompositeOscillator : SynthComponentBase, IOscillator
    {
        private readonly IList<IOscillator> _oscillators = new List<IOscillator>();

        public WaveFormEnum WaveForm
        {
            get { return _oscillators[0].WaveForm; }
            set { _oscillators[0].WaveForm = value; }
        }

        public override int SampleRate
        {
            get { return _oscillators[0].SampleRate; }
            set
            {
                foreach (var oscillator in _oscillators)
                {
                    oscillator.SampleRate = value;
                }
            }
        }

        public float Amplitude
        {
            get { return _oscillators[0].Amplitude; }
            set
            {
                foreach (var oscillator in _oscillators)
                {
                    oscillator.Amplitude = value;
                }
            }
        }

        public float Frequency
        {
            get { return _oscillators[0].Frequency; }
            set
            {
                foreach (var oscillator in _oscillators)
                {
                    oscillator.Frequency = value;
                }
            }
        }

        public float Phase
        {
            get { return _oscillators[0].Phase; }
            set
            {
                foreach (var oscillator in _oscillators)
                {
                    oscillator.Phase = value;
                }
            }
        }

        public float DutyCycle
        {
            get { return _oscillators[0].DutyCycle; }
            set
            {
                foreach(var oscillator in _oscillators)
                {
                    oscillator.DutyCycle = value;
                }
            }
        }

        public float CutOff
        {
            get { return _oscillators[0].CutOff; }
            set
            {
                foreach (var oscillator in _oscillators)
                {
                    oscillator.CutOff = value;
                }
            }
        }

        public float Detune
        {
            get { return _oscillators[0].Detune; }
            set
            {
                foreach (var oscillator in _oscillators)
                {
                    oscillator.Detune = value;
                }
            }
        }

        public float FmLevel
        {
            get { return _oscillators[0].FmLevel; }
            set
            {
                foreach (var oscillator in _oscillators)
                {
                    oscillator.FmLevel = value;
                }
            }
        }

        public OctaveMultiplier Multiplier
        {
            get { return _oscillators[0].Multiplier; }
            set
            {
                foreach(var oscillator in _oscillators)
                {
                    oscillator.Multiplier = value;
                }
            }
        }

        public IEnumerable<IOscillator> Components { get { return _oscillators; } }

        public override ISynthComponent MakeInstanceCopy()
        {
            var co = new CompositeOscillator();

            foreach (var osc in _oscillators)
            {
                co.AddOscillator((IOscillator)osc.MakeInstanceCopy());
            }

            return co;
        }

        public void AddOscillator(IOscillator oscillator)
        {
            if (_oscillators.Contains(oscillator))
            {
                return;
            }

            _oscillators.Add(oscillator);
        }

        public override void RenderSamples(int offset, int count)
        {
            InitialiseMonoBuffer(count);
            Array.Clear(_monoBuffer, 0, _monoBuffer.Length);

            foreach (var oscillator in _oscillators)
            {
                oscillator.RenderSamples(offset, count);
                var buffer = oscillator.GetMonoBuffer();
                for (var i = 0; i < buffer.Length; ++i)
                {
                    _monoBuffer[i] += buffer[i];
                }
            }
        }
    }
}
