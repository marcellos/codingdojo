﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataFixedPointArithmetic
{
    [TestFixture]
    public class FixedPointTests
    {
        private FixedPoint _fixedPoint;

        [SetUp]
        public void Setup()
        {
            _fixedPoint = new FixedPoint();
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(FixedPoint), _fixedPoint);
        }

        [Test]
        public void CanCreate_InstanceWithIntValue()
        {
            var f = new FixedPoint(123);
            Assert.IsInstanceOf(typeof(FixedPoint), f);
        }

        [Test]
        public void CanCreate_InstanceWithDoubleValue()
        {
            var f = new FixedPoint(12.345);
            Assert.IsInstanceOf(typeof(FixedPoint), f);
        }

        [Test]
        public void ToInt_WithPositiveValue_CorrectValue()
        {
            var f = new FixedPoint(123);
            var fint = f.ToInt();
            Assert.AreEqual(123, fint);
        }

        [Test]
        public void ToInt_WithZero_CorrectValue()
        {
            var f = new FixedPoint(0);
            var fint = f.ToInt();
            Assert.AreEqual(0, fint);
        }

        [Test]
        public void ToInt_WithNegativeValue_CorrectValue()
        {
            var f = new FixedPoint(-123);
            var fint = f.ToInt();
            Assert.AreEqual(-123, fint);
        }

        [Test]
        public void ToDouble_WithPositiveValue_CorrectValue()
        {
            var f = new FixedPoint(12.345);
            var fdouble = f.ToDouble();
            Assert.AreEqual(12.345, Math.Round(fdouble, 3));
        }

        [Test]
        public void ToDouble_WithZero_CorrectValue()
        {
            var f = new FixedPoint(0.0);
            var fdouble = f.ToDouble();
            Assert.AreEqual(0.0, Math.Round(fdouble, 1));
        }

        [Test]
        public void ToDouble_WithNegativeValue_CorrectValue()
        {
            var f = new FixedPoint(-12.345);
            var fdouble = f.ToDouble();
            Assert.AreEqual(-12.345, Math.Round(fdouble, 3));
        }

        [Test]
        public void Inverse_WithPositiveValue_CorrectValue()
        {
            var f = new FixedPoint(12.345);
            var finverse = f.Inverse;
            Assert.AreEqual(-12.345, Math.Round(finverse.ToDouble(), 3));
        }

        [Test]
        public void Inverse_WithZero_CorrectValue()
        {
            var f = new FixedPoint(0.0);
            var finverse = f.Inverse;
            Assert.AreEqual(0.0, Math.Round(finverse.ToDouble(), 1));
        }

        [Test]
        public void Inverse_WithNegativeValue_CorrectValue()
        {
            var f = new FixedPoint(-12.345);
            var finverse = f.Inverse;
            Assert.AreEqual(12.345, Math.Round(finverse.ToDouble(), 3));
        }

        [Test]
        public void ExplicitDoubleConversion_CorrectValue()
        {
            var f = new FixedPoint(12.345);
            var fexplicitdouble = (double)f;
            Assert.AreEqual(12.345, Math.Round(fexplicitdouble, 3));
        }

        [Test]
        public void ImplicitDoubleConversion_CorrectValue()
        {
            FixedPoint f = 12.345;
            Assert.AreEqual(12.345, Math.Round(f.ToDouble(), 3));
        }

        [Test]
        public void UnaryPlusOperator_CorrectValue()
        {
            var f = new FixedPoint(12.345);
            var fplus = +f;
            Assert.AreEqual(12.345, Math.Round(fplus.ToDouble(), 3));
        }

        [Test]
        public void BinaryPlusOperator_CorrectValue()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var fsum = f1 + f2;
            Assert.AreEqual(35.801, Math.Round(fsum.ToDouble(), 3));
        }

        [Test]
        public void UnaryMinusOperator_CorrectValue()
        {
            var f = new FixedPoint(12.345);
            var fminus = -f;
            Assert.AreEqual(-12.345, Math.Round(fminus.ToDouble(), 3));
        }

        [Test]
        public void BinaryMinusOperator_CorrectValue()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var fdifference = f1 - f2;
            Assert.AreEqual(-11.111, Math.Round(fdifference.ToDouble(), 3));
        }

        [Test]
        public void BinaryMultiplicationOperator_CorrectValue()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var fmultiplication = f1 * f2;
            Assert.AreEqual(289.564, Math.Round(fmultiplication.ToDouble(), 3));
        }

        [Test]
        public void BinaryDivisionOperator_CorrectValue()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var fdivision = f1 / f2;
            Assert.AreEqual(0.5263, Math.Round(fdivision.ToDouble(), 4));
        }

        [Test]
        public void BinaryModuloOperator_CorrectValue()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var fmodulo = f2 % f1;
            Assert.AreEqual(11.111, Math.Round(fmodulo.ToDouble(), 3));
        }

        [Test]
        public void EqualsOperator_WithTwoEqualValues_True()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(12.345);
            var fequal = f1 == f2;
            Assert.IsTrue(fequal);
        }

        [Test]
        public void EqualsOperator_WithTwoNotEqualValues_False()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var fequal = f1 == f2;
            Assert.IsFalse(fequal);
        }

        [Test]
        public void GreaterThanOperator_WithF1GreaterThanF2_True()
        {
            var f1 = new FixedPoint(23.456);
            var f2 = new FixedPoint(12.345);
            var fgreaterthan = f1 > f2;
            Assert.IsTrue(fgreaterthan);
        }

        [Test]
        public void GreaterThanOperator_WithF2GreaterThanF1_False()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var fgreaterthan = f1 > f2;
            Assert.IsFalse(fgreaterthan);
        }

        [Test]
        public void LessThanOperator_WithF1LessThanF2_True()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var flessthan = f1 < f2;
            Assert.IsTrue(flessthan);
        }

        [Test]
        public void LessThanOperator_WithF2LessThanF1_False()
        {
            var f1 = new FixedPoint(23.456);
            var f2 = new FixedPoint(12.345);
            var flessthan = f1 < f2;
            Assert.IsFalse(flessthan);
        }

        [Test]
        public void GreaterOrEqualThanOperator_WithF1GreaterThanF2_True()
        {
            var f1 = new FixedPoint(23.456);
            var f2 = new FixedPoint(12.345);
            var fgreaterorequal = f1 >= f2;
            Assert.IsTrue(fgreaterorequal);
        }

        [Test]
        public void GreaterOrEqualThanOperator_WithF1EqualF2_True()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(12.345);
            var fgreaterorequal = f1 >= f2;
            Assert.IsTrue(fgreaterorequal);
        }

        [Test]
        public void GreaterOrEqualThanOperator_WithF2GreaterF1_False()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var fgreaterorequal = f1 >= f2;
            Assert.IsFalse(fgreaterorequal);
        }

        [Test]
        public void LessOrEqualThanOperator_WithF1LessThanF2_True()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            var flessorequal = f1 <= f2;
            Assert.IsTrue(flessorequal);
        }

        [Test]
        public void LessOrEqualThanOperator_WithF1EqualF2_True()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(12.345);
            var flessorequal = f1 <= f2;
            Assert.IsTrue(flessorequal);
        }

        [Test]
        public void LessOrEqualThanOperator_WithF2LessThanF1_False()
        {
            var f1 = new FixedPoint(23.456);
            var f2 = new FixedPoint(12.345);
            var flessorequal = f1 <= f2;
            Assert.IsFalse(flessorequal);
        }

        [Test]
        public void Equals_WithSameObject_True()
        {
            var f = new FixedPoint(12.345);
            Assert.IsTrue(f.Equals(f));
        }

        [Test]
        public void Equals_WithSameValue_True()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(12.345);
            Assert.IsTrue(f1.Equals(f2));
        }

        [Test]
        public void Equals_WithDifferentObject_False()
        {
            var f1 = new FixedPoint(12.345);
            var f2 = new FixedPoint(23.456);
            Assert.IsFalse(f1.Equals(f2));
        }

        [Test]
        public void Equals_WithSameDoubleValue_False()
        {
            var f = new FixedPoint(12.345);
            Assert.IsFalse(f.Equals(12.345));
        }

        [Test]
        public void GetHashCode_809042()
        {
            var f = new FixedPoint(12.345);
            Assert.AreEqual(809042, f.GetHashCode());
        }

        [Test]
        public void ToString_809042()
        {
            var f = new FixedPoint(12.345);
            Assert.AreEqual("809042", f.ToString());
        }

        [Test]
        public void MaxValue_32768()
        {
            Assert.AreEqual(32768, FixedPoint.MaxValue);
        }

        [Test]
        public void MinValue_neg32768()
        {
            Assert.AreEqual(-32768, FixedPoint.MinValue);
        }

        [Test]
        public void ReachMaxInt_32768()
        {
            var max = 0L;
            while (true)
            {
                max++;
                var f = new FixedPoint(max);
                if (Math.Abs(Convert.ToDouble(max) - f.ToDouble()) > double.Epsilon)
                {
                    break;
                }
            }

            Assert.AreEqual(32768, max);
        }

        [Test]
        public void ReachMinInt_neg32769()
        {
            var min = 0L;
            while (true)
            {
                min--;
                var f = new FixedPoint(min);
                if (Math.Abs(Convert.ToDouble(min) - f.ToDouble()) > double.Epsilon)
                {
                    break;
                }
            }

            Assert.AreEqual(-32769, min);
        }

        [Test]
        public void ReachMaxDouble_32768()
        {
            var max = 0.0;
            while (true)
            {
                max += 0.5;
                var f = new FixedPoint(max);
                if (Math.Abs(max - f.ToDouble()) > double.Epsilon)
                {
                    break;
                }
            }

            Assert.AreEqual(32768.0, max);
        }

        [Test]
        public void ReachMinDouble_neg32768Point5()
        {
            var min = 0.0;
            while (true)
            {
                min -= 0.5;
                var f = new FixedPoint(min);
                if (Math.Abs(min - f.ToDouble()) > double.Epsilon)
                {
                    break;
                }
            }

            Assert.AreEqual(-32768.5, min);
        }

        [Test]
        public void ReachEpsilon()
        {
            var one = new FixedPoint(1);
            const int factor = 10;
            var divisorMagnitude = 1;
            var epsilon = new FixedPoint(0);
            var previous = new FixedPoint(2);
            while (true)
            {
                epsilon = one / new FixedPoint(divisorMagnitude);
                if (epsilon >= previous)
                {
                    break;
                }

                divisorMagnitude *= factor;
                previous = epsilon;
            }

            Assert.AreEqual(1.0 / 1000000, 1.0 / divisorMagnitude);
        }
    }
}
