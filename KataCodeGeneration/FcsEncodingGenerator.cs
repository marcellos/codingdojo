﻿
/* generated file, do not edit! */

namespace KataCodeGeneration
{
  public static partial class FcsEncoding
  {
	public const string JumpToApplication = "JMP";
	public const string JumpToBootloader = "JMP";
	public const string ReportFirmwareVersion = "RFV";
	public const string ReportMessagePayload = "RMP";
	public const string ApplicationDataErase = "ADE";
	public const string StopAliveTimeout = "SAT";
	public const string SendDataHex = "SDH";
	public const string ReportProductName = "RPN";
	public const string ReportSerialNumber = "RSN";
	public const string SetProductName = "SPN";
	public const string SetSerialNumber = "SSN";
	public const string ReportFreeOtpLocations = "RFO";
	public const string ReportOtpData = "ROD";
	public const string SetOtpData = "SOD";
	public const string EnableDeviceFunction = "EDF";
	public const string ReportAllDevices = "RAD";
	public const string ConfigureFaststopEventId = "CFE";
	public const string ConfigureMasterNodeId = "CMI";
	public const string ReportFaststopEventId = "RFE";
	public const string ReportMasterNodeId = "RMI";
	public const string SendEventToNode = "SEE";
	public const string SetLowlevelInterrupt = "SLI";
	public const string SetSerialtunnelConfiguration = "SSE";
	public const string SetSerialtunnelOnOff = "SSO";
	public const string ReportComPower = "RCP";
	public const string ReportCanPower = "RIO";
	public const string SwitchComPower = "SCP";
	public const string SwitchCanPower = "SIO";
	public const string ReportVoltageLimits = "RVL";
	public const string SetVoltageLimits = "SVL";
	public const string FileSystemControl = "FSC";

  }
}
