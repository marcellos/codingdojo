﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace KataRx
{
    class EventSynchronizer
    {
        private TimeSpan _timeOut;
        private IObservable<EventPattern<TestEventArgs>> _eventStream;

        public EventSynchronizer(TimeSpan timeout)
        {
            _timeOut = timeout;
        }

        public void MergeAction(ITestEventSource eventSource)
        {
            if (eventSource == null)
            {
                return;
            }

            var newEvent = Observable.FromEventPattern<TestEventArgs>(
                h => eventSource.TestEvent += h, 
                h => eventSource.TestEvent -= h).Take(1);

            if (_eventStream == null)
            {
                _eventStream = newEvent;
                return;
            }

            _eventStream = _eventStream.Merge(newEvent);
        }

        public IObservable<EventPattern<TestEventArgs>> GetMergedEventStream()
        {
            return _eventStream.Timeout(_timeOut);
        }
    }
}
