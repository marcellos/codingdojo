﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataCarcassonne
{
    class TileArea : IEquatable<TileArea>
    {
        public string Name { get; set; }

        public bool Equals(TileArea other)
        {
            return Name.StartsWith(other.Name) || other.Name.StartsWith(Name);
        }
    }
}
