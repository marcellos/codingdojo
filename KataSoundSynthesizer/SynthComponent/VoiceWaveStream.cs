﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;

namespace KataSoundSynthesizer.SynthComponent
{
    class VoiceWaveStream : WaveStreamBase
    {
        private readonly IVoice _voice;
        private readonly int _durationInSeconds;
        private long _totalSampleCount;
        private readonly TrackedKey[] _trackedKeys;
        private readonly int _trackedKeysLength;
        private TrackedKey _trackedKey;
        private int _trackedKeyIndex;
        private float[,] _stereoVoiceSamples;
        private bool _abortRendering = false;

        private readonly float[,] _vessel = new float[2, 64 * 1024]; // 64kB
        private int _vesselSize;
        private int _served;

        public VoiceWaveStream(WaveFormat waveFormat, IVoice voice, int durationInSeconds, TrackedKey[] trackedKeys) 
            : base(waveFormat)
        {
            _voice = voice;
            _durationInSeconds = durationInSeconds;
            _trackedKeys = trackedKeys;
            _trackedKeysLength = _trackedKeys.Length - 1;
            _trackedKey = _trackedKeys[_trackedKeyIndex];
        }


        protected override int Read(float[] samples, int offset, int count)
        {
            var sampleRate = WaveFormat.sampleRate;
            var channels = WaveFormat.channels;
            var sampleCount = count / channels;
            var index = 0;

            // buffer rendering
            if (_stereoVoiceSamples == null || _stereoVoiceSamples.Length / 2 != sampleCount)
            {
                _stereoVoiceSamples = new float[2,sampleCount];
            }

            var result = Buffering(_stereoVoiceSamples, sampleCount, offset);

            // serve requested sample count
            for (var i = 0; i < sampleCount; ++i)
            {
                samples[index + offset] = _stereoVoiceSamples[0, i];
                if (channels == 2)
                {
                    samples[index + offset + 1] = _stereoVoiceSamples[1, i];
                }

                index += channels;
            }

            _totalSampleCount += sampleCount;
            var returnValue = count;
            if (_totalSampleCount/sampleRate > _durationInSeconds 
                || result == 0)
            {             
                returnValue = 0;
            }

            return returnValue;
        }


        private static void CueTrackedKey(IVoice voice, TrackedKey trackedKey)
        {
            if (voice == null)
            {
                return;
            }

            if (trackedKey.Mode == TrackedKey.KeyMode.Trigger)
            {
                voice.TriggerKey(trackedKey);
            }

            if (trackedKey.Mode == TrackedKey.KeyMode.Release)
            {
                voice.ReleaseKey(trackedKey);
            }            
        }

        private int Buffering(float[,] buffer, int count, int offset)
        {         
            // serve
            while ((_vesselSize - _served) >= count)
            {
                ArrayCopy(_vessel, _served, buffer, 0, count);
                _served += count;
                return count;
            }

            // fill 
            while ((_vesselSize - _served) < count)
            {
                // render samples 
                var data = RenderSamples(offset); 
                if (data.Length <= 0)
                {
                    return 0;
                }

                ArrayCopy(data, 0, _vessel, _vesselSize, data.Length/2);
                _vesselSize += data.Length/2;
            }

            // re-arrange
            _vesselSize = _vesselSize - _served;
            ArrayCopy(_vessel, _served, _vessel, 0, _vesselSize);
            _served = 0;

            // serve
            ArrayCopy(_vessel, _served, buffer, 0, count);
            _served += count;
            return count;
        }

        private float[,] RenderSamples(int offset)
        {
            if (_trackedKeyIndex <= _trackedKeysLength)
            {
                CueTrackedKey(_voice, _trackedKey);
                _trackedKeyIndex++;
                _trackedKey = _trackedKeys[_trackedKeyIndex];
            }

            while (_trackedKey.DeltaTimeTicks == 0
                   && _trackedKeyIndex <= _trackedKeysLength)
            {
                CueTrackedKey(_voice, _trackedKey);

                if (_trackedKeyIndex >= _trackedKeysLength)
                {
                    continue;
                }

                _trackedKeyIndex++;
                _trackedKey = _trackedKeys[_trackedKeyIndex];
            }

            if (_abortRendering)
            {
                return new float[0,0];
            }

            _abortRendering = (_trackedKeyIndex == _trackedKeysLength);
            _voice.RenderSamples(offset, (int) _trackedKey.DeltaTimeTicks);
            return _voice.GetStereoBuffer();
        }

        private static void ArrayCopy(float[,] source, int sourceIndex, float[,] destination, int destinationIndex, int length)
        {
            for (var i = 0; i < length; ++i)
            {
                destination[0, destinationIndex + i] = source[0, sourceIndex + i];
                destination[1, destinationIndex + i] = source[1, sourceIndex + i];
            }
        }
    }
}
