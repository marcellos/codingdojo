﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;

namespace KataCommandDispatcher
{
    public interface IDependencyResolver
    {
        IEnumerable<object> GetAll<T>();
    }
}
