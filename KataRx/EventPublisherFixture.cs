﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataRx
{
    [TestFixture]
    public class EventPublisherFixture
    {
        private IEventPublisher _eventPublisher;
        private Recipient _recipient;

        [SetUp]
        public void Setup()
        {
            _eventPublisher = new EventPublisher();
            _recipient = new Recipient(_eventPublisher);
        }

        [TearDown]
        public void Teardown()
        {
            _recipient = null;
            _eventPublisher = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(EventPublisher), _eventPublisher);
        }

        [Test]
        public void PublishMessageTest()
        {
            _recipient.SubscribeMessage();
            _eventPublisher.Publish(new Message());
            Assert.IsInstanceOf(typeof(Message), _recipient.ReceivedMessage);
            _recipient.Unsubscribe();
        }

        [Test]
        public void PublishNullTest()
        {
            _recipient.SubscribeMessage();
            Message msg = null;
            _eventPublisher.Publish(msg);
            Assert.IsNull(_recipient.ReceivedMessage);
            _recipient.Unsubscribe();
        }

        [Test]
        public void PublishMessageNoSubscriptionTest()
        {
            _eventPublisher.Publish(new Message());
            Assert.IsNull(_recipient.ReceivedMessage);
        }

        [Test]
        public void SubscribeBaseTypeTest()
        {
            _recipient.SubscribeMessage();
            _eventPublisher.Publish(new MessageA() as Message);
            Assert.IsInstanceOf(typeof(MessageA), _recipient.ReceivedMessage);
            _recipient.Unsubscribe();
        }

        [Test]
        public void BroadcastMessageTest()
        {
            var newRecipient = new Recipient(_eventPublisher);
            _recipient.SubscribeMessage();
            newRecipient.SubscribeMessage();
            _eventPublisher.Publish(new Message());
            Assert.IsInstanceOf(typeof(Message), _recipient.ReceivedMessage);
            Assert.IsInstanceOf(typeof(Message), newRecipient.ReceivedMessage);
            _recipient.Unsubscribe();
            newRecipient.Unsubscribe();
        }

        [Test]
        public void RecipientNullTest()
        {
            var newRecipient = new Recipient(_eventPublisher);
            _recipient.SubscribeMessage();
            newRecipient.SubscribeMessage();
            _eventPublisher.Publish(new Message());
            Assert.IsInstanceOf(typeof(Message), _recipient.ReceivedMessage);
            Assert.IsInstanceOf(typeof(Message), newRecipient.ReceivedMessage);
            newRecipient.ReceivedMessage = null;
            _recipient = null;
            GC.Collect();
            _eventPublisher.Publish(new Message());
            Assert.IsInstanceOf(typeof(Message), newRecipient.ReceivedMessage);
            newRecipient.Unsubscribe();
        }

        [Test]
        public void FaultyRecipientTest()
        {
            var faultyRecipient = new FaultyRecipient(_eventPublisher);
            faultyRecipient.SubscribeMessage();
            _recipient.SubscribeMessage();
            try
            {
                _eventPublisher.Publish(new Message());
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf(typeof(InvalidCastException), ex);
            }

            Assert.IsInstanceOf(typeof(Message), _recipient.ReceivedMessage);
            _recipient.Unsubscribe();
            faultyRecipient.Unsubscribe();
        }

        class Message
        { }

        class MessageA : Message
        { }

        class Recipient
        {
            private readonly IEventPublisher _eventPublisher;
            private IDisposable _disposable;
            public Message ReceivedMessage { get; set; }

            public Recipient(IEventPublisher eventPublisher)
            {
                _eventPublisher = eventPublisher;
            }

            public void SubscribeMessage()
            {
                var msg = _eventPublisher.GetEvent<Message>();
                _disposable = msg.Subscribe(
                    m => ReceivedMessage = m,
                    e => Console.WriteLine("error: {0}", e.Message),
                    () => Console.WriteLine("completed"));
            }

            public void Unsubscribe()
            {
                _disposable.Dispose();
            }
        }

        class FaultyRecipient
        {
            private readonly IEventPublisher _eventPublisher;
            private IDisposable _disposable;
            public Message ReceivedMessage { get; set; }

            public FaultyRecipient(IEventPublisher eventPublisher)
            {
                _eventPublisher = eventPublisher;
            }

            public void SubscribeMessage()
            {
                var msg = _eventPublisher.GetEvent<Message>();
                _disposable = msg.Subscribe(
                    m => ((MessageA)m).ToString(),
                    e => Console.WriteLine("error: {0}", e.Message),
                    () => Console.WriteLine("completed"));
            }

            public void Unsubscribe()
            {
                _disposable.Dispose();
            }
        }
    }
}
