﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using NUnit.Framework;

namespace KataSoundSynthesizer.Midi
{
    [TestFixture]
    public class FileReaderTest
    {
        private const string Ex1FilePath = @"Midi\ex1.mid";
        private const string TheEntertainerFilePath = @"Midi\the_entertainer.mid";
        private const string DebClaiFilePath = @"Midi\deb_clai.mid";
        private const string DrumSampleFilePath = @"Midi\drum_sample.mid";
        private const string DjangoFilePath = @"Midi\django.mid";

        private static string CurrentDirectory = TestContext.CurrentContext.TestDirectory;

        [Test]
        public void Read_WithEx1_ThenFileIsRead()
        {
            MidiInfo midiInfo;
            var tracks = FileReader.Read(Path.Combine(CurrentDirectory, Ex1FilePath), out midiInfo);
            Assert.AreEqual(1, tracks.Count());
        }

        [Test]
        public void Read_WithEntertainer_ThenFileIsRead()
        {
            MidiInfo midiInfo;
            var tracks = FileReader.Read(Path.Combine(CurrentDirectory, TheEntertainerFilePath), out midiInfo);
            Assert.AreEqual(2, tracks.Count());
            Assert.AreEqual(FormatType.MultiTrack, midiInfo.Format);
            Assert.AreEqual(2, midiInfo.NumberOfTracks);
            Assert.AreEqual(TempoType.Metrical, midiInfo.Tempo);
            Assert.AreEqual(256, midiInfo.PulsesPerQuarterNote);
        }

        [Test]
        public void Read_WithDebClai_ThenFileIsRead()
        {
            MidiInfo midiInfo;
            var tracks = FileReader.Read(Path.Combine(CurrentDirectory, DebClaiFilePath), out midiInfo);
            Assert.AreEqual(FormatType.MultiTrack, midiInfo.Format);
            Assert.AreEqual(7, midiInfo.NumberOfTracks);
            Assert.AreEqual(TempoType.Metrical, midiInfo.Tempo);
            Assert.AreEqual(480, midiInfo.PulsesPerQuarterNote);
        }

        [Test]
        public void Read_WithDrumSample_ThenFileIsRead()
        {
            MidiInfo midiInfo;
            var tracks = FileReader.Read(Path.Combine(CurrentDirectory, DrumSampleFilePath), out midiInfo);
            Assert.AreEqual(FormatType.MultiTrack, midiInfo.Format);
            Assert.AreEqual(2, midiInfo.NumberOfTracks);
            Assert.AreEqual(TempoType.Metrical, midiInfo.Tempo);
            Assert.AreEqual(480, midiInfo.PulsesPerQuarterNote);
        }

        [Test, Explicit]
        public void Read_WithEntertainer_ThenPrintTrackInformation()
        {
            MidiInfo midiInfo;
            var tracks = FileReader.Read(Path.Combine(CurrentDirectory, TheEntertainerFilePath), out midiInfo);
            PrintTrackInformation(tracks);
        }

        private static void PrintTrackInformation(IEnumerable<Track> tracks)
        {
            foreach (var track in tracks)
            {
                PrintEventInformation(track.Events);
            }
        }

        private static void PrintEventInformation(IEnumerable<MidiEventBase> midiEvents)
        {
            foreach (var midiEvent in midiEvents)
            {
                PrintEvent(midiEvent);
            }
        }

        private static void PrintEvent(MidiEventBase midiEvent)
        {
            var metaEvent = midiEvent as MidiMetaEvent;
            if (metaEvent != null)
            {
                if (metaEvent.EventType == MidiMetaEventType.SetTempo)
                {
                    Console.WriteLine("microseconds={0}", metaEvent.MicrosecondsPerQuarterNote);
                }
            }

            var noteEvent = midiEvent as MidiEvent;
            if (noteEvent != null)
            {
                if (noteEvent.CommandType == MidiEventCommandType.NoteOn
                    || noteEvent.CommandType == MidiEventCommandType.NoteOff)
                {
                    Console.WriteLine("cmd={0}, ch={1}, key={2}, vel={3},  dt={4}", 
                        noteEvent.CommandType, noteEvent.Channel, noteEvent.Param1, noteEvent.Param2, noteEvent.DeltaTime);
                }
            }
        }
    }
}
