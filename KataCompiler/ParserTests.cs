﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace KataCompiler
{
    [TestFixture]
    public class ParserTests
    {
        [Test]
        public void Parse_WhenCalled_ReturnExpression()
        {
            // (4.0+5.0)*7.0
            var tokens = new List<TokenValue>
                {
                    new TokenValue {TokenId = Token.LeftBracket},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "4.0"},
                    new TokenValue {TokenId = Token.Plus},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "5.0"},
                    new TokenValue {TokenId = Token.RightBracket},
                    new TokenValue {TokenId = Token.Asterisk},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "7.0"}
                };

            var parser = new ExpressionParser();
            var expr = parser.Parse(tokens);
            var value = expr.Evaluate();
            Console.WriteLine("value:{0}", value);
            PrintExpression(expr, 0);

            //      *
            //     / \
            //    +   7
            //   / \
            //  4   5

            Assert.AreEqual(63, value);
        }

        [Test]
        public void Parse_WhenStreamContainsPower_ReturnExpression()
        {
            // 4.0+5.0*7.0^2.0
            var tokens = new List<TokenValue>
                {
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "4.0"},
                    new TokenValue {TokenId = Token.Plus},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "5.0"},
                    new TokenValue {TokenId = Token.Asterisk},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "7.0"},
                    new TokenValue {TokenId = Token.Carot},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "2.0"}
                };

            var parser = new ExpressionParser();
            var expr = parser.Parse(tokens);
            var value = expr.Evaluate();
            Console.WriteLine("value:{0}", value);
            PrintExpression(expr, 0);

            //       +
            //      / \
            //     4   *
            //        / \
            //       5   ^
            //          / \
            //         7   2

            Assert.AreEqual(249, value);
        }

        [Test]
        public void Parse_WhenStreamContainsUnaryMinus_ReturnExpression()
        {
            // 4.0+5.0*7.0^-2.0
            var tokens = new List<TokenValue>
                {
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "4.0"},
                    new TokenValue {TokenId = Token.Plus},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "5.0"},
                    new TokenValue {TokenId = Token.Asterisk},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "7.0"},
                    new TokenValue {TokenId = Token.Carot},
                    new TokenValue {TokenId = Token.Minus},
                    new TokenValue {TokenId = Token.NumberLiteral, Literal = "2.0"}
                };

            var parser = new ExpressionParser();
            var expr = parser.Parse(tokens);
            var value = expr.Evaluate();
            Console.WriteLine("value:{0}", value);
            PrintExpression(expr, 0);

            //        +
            //       / \
            //      4   *
            //         / \
            //        5   ^
            //           / \
            //          7   *
            //             / \
            //           -1   2

            Assert.AreEqual(4.10204, Math.Round(value, 5));
        }

        private static void PrintExpression(Expression expr, int level)
        {
            var indent = new string('.', level);
            if (expr is BinaryOperator)
            {
                var op = expr as BinaryOperator;
                Console.WriteLine(indent + op.OpCode);
                PrintExpression(op.Left, level + 1);
                PrintExpression(op.Right, level + 1);
            }

            if (expr is Constant)
            {
                var c = expr as Constant;
                Console.WriteLine(indent + c.Value);
            }
        }

    }

    public class ExpressionParser
    {
        // E = T Eopt .
        // Eopt = "+" T Eopt | "-" T Eopt | .
        // T = F Topt .
        // Topt = "*" F Topt | "/" F Topt | .
        // F = Real | "(" E ")" .

        // E = T Eopt .
        // Eopt = "+" T Eopt | "-" T Eopt | .
        // T = F Topt .
        // Topt = "*" P Topt | "/" P Topt | .
        // P = F Popt .
        // Popt = "^" F P .
        // F = Real | "(" E ")" | "-" T .

        // E --> T {("+"|"-") T}
        // T --> F {("*"|"/") F}
        // F --> P ["^" F]
        // P --> v | "(" E ")" | "-" T

        public Expression Parse(IEnumerable<TokenValue> tokens)
        {
            var stream = tokens.GetEnumerator();
            stream.MoveNext();
            return E(stream);
        }

        private Expression E(IEnumerator<TokenValue> stream)
        {
            return Eopt(T(stream), stream);
        }

        private Expression T(IEnumerator<TokenValue> stream)
        {
            return Topt(F(stream), stream);
        }

        private Expression Eopt(Expression expr, IEnumerator<TokenValue> stream)
        {
            switch (stream.Current.TokenId)
            {
                case Token.Plus:
                    stream.MoveNext();
                    expr = Eopt(new BinaryOperator(expr, T(stream), "+"), stream);
                    break;

                case Token.Minus:
                    stream.MoveNext();
                    expr = Eopt(new BinaryOperator(expr, T(stream), "-"), stream);
                    break;

                default:
                    break;
            }

            return expr;
        }

        private Expression F(IEnumerator<TokenValue> stream)
        {
            Expression e = null;
            switch (stream.Current.TokenId)
            {
                case Token.NumberLiteral:
                    e = new Constant(double.Parse(stream.Current.Literal));
                    stream.MoveNext();
                    break;

                case Token.LeftBracket:
                    stream.MoveNext();
                    e = E(stream);
                    if (stream.Current.TokenId != Token.RightBracket)
                    {
                        throw new ApplicationException("parse error: ')' expected");
                    }
                    stream.MoveNext();
                    break;

                case Token.Minus:
                    stream.MoveNext();
                    e = new BinaryOperator(new Constant(-1d), F(stream), "*");
                    break;

                default:
                    break;
            }

            return e;
        }

        private Expression Topt(Expression expr, IEnumerator<TokenValue> stream)
        {
            switch (stream.Current.TokenId)
            {
                case Token.Asterisk:
                    stream.MoveNext();
                    expr = Topt(new BinaryOperator(expr, P(stream), "*"), stream);
                    break;

                case Token.Slash:
                    stream.MoveNext();
                    expr = Topt(new BinaryOperator(expr, P(stream), "/"), stream);
                    break;

                default:
                    break;
            }

            return expr;
        }

        private Expression P(IEnumerator<TokenValue> stream)
        {
            return Popt(F(stream), stream);
        }

        private Expression Popt(Expression expr, IEnumerator<TokenValue> stream)
        {
            switch (stream.Current.TokenId)
            {
                case Token.Carot:
                    stream.MoveNext();
                    expr = Popt(new BinaryOperator(expr, F(stream), "^"), stream);
                    break;

                default:
                    break;
            }

            return expr;
        }
    }

    public abstract class Expression
    {
        public abstract double Evaluate();
    }

    class BinaryOperator : Expression
    {
        public Expression Left { get; private set; }
        public Expression Right { get; private set; }
        public string OpCode { get; private set; }

        public BinaryOperator(Expression left, Expression right, string opCode)
        {
            Left = left;
            Right = right;
            OpCode = opCode;
        }

        public override double Evaluate()
        {
            var value = 0d;
            switch (OpCode)
            {
                case "+":
                    value = Left.Evaluate() + Right.Evaluate();
                    break;

                case "-":
                    value = Left.Evaluate() - Right.Evaluate();
                    break;

                case "*":
                    value = Left.Evaluate() * Right.Evaluate();
                    break;

                case "/":
                    value = Left.Evaluate() / Right.Evaluate();
                    break;
                case "^":
                    value = Math.Pow(Left.Evaluate(), Right.Evaluate());
                    break;

                default:
                    break;
            }

            return value;
        }
    }

    class Constant : Expression
    {
        public double Value { get; private set; }

        public Constant(double value)
        {
            Value = value;
        }

        public override double Evaluate()
        {
            return Value;
        }
    }
}
