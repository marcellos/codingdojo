﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataRx.Coroutines3
{
    public class ContinuationResultCompletionEventArgs : EventArgs
    {
        public Exception Error { get; set; }
        public bool WasCancelled { get; set; }
    }
}
