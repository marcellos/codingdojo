﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace KataRx.Coroutines2
{
    public class QueryDiskResult : BaseResult<object>
    {
        public QueryDiskResult(IObserver<ISequentialResult> observer)
            : base(observer)
        {
            StartQuery();
        }

        public void StartQuery()
        {
            Value = 0;
            var t = Start();
            Console.WriteLine("result: {0}", t.Result);
            PrintThreadId();
        }

        private Task<int> Start()
        {
            var location = Assembly.GetExecutingAssembly().Location;
            var fi = new FileInfo(location);
            var fs = new FileStream(location, FileMode.Open, FileAccess.Read);
            var buffer = new byte[fi.Length];
            var task = Task<int>.Factory.FromAsync(fs.BeginRead, fs.EndRead, buffer, 0, buffer.Length, null);
            Console.WriteLine(task.Status);
            PrintThreadId();

            return task.ContinueWith(result =>
                    {
                        fs.Close();
                        //Console.WriteLine(buffer.Length);
                        //System.Threading.Thread.Sleep(500);
                        Console.WriteLine(task.Status);
                        PrintThreadId();
                        Observer.OnNext(this);
                        Observer.OnCompleted();
                        Value = buffer.Length;
                        return buffer.Length;
                    });
        }

        private static void PrintThreadId()
        {
            Console.WriteLine(" on thread: {0}", System.Threading.Thread.CurrentThread.ManagedThreadId);
        }

    }
}
