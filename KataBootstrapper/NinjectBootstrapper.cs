﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using Ninject;

namespace KataBootstrapper
{
    public class NinjectBootstrapper : Bootstrapper<FrameworkRoot>
    {
        IKernel _kernel;

        static NinjectBootstrapper()
        {
            LogManager.GetLog = type => new DebugKernelLog();
        }

        protected override void Configure()
        {
            _kernel = new StandardKernel();

            // register kernel services
            _kernel.Bind<ILogService>().To<ConsoleLogService>().InSingletonScope();

            // register modules
            _kernel.Load(new[] { "KataBootstrapper.dll" });
        }

        protected override object IocGetInstance(Type service, string key)
        {
            var instance = string.IsNullOrEmpty(key) ? _kernel.GetService(service) : _kernel.Get(service, key);
            if (instance == null)
            {
                throw new NullReferenceException(string.Format("could locate any instance of contract: {0}{1}", service.Name, key));
            }

            return instance;
        }

        protected override IEnumerable<object> IocGetAllInstances(Type service)
        {
            return _kernel.GetAll(service);
        }

        protected override void IocBuildUp(object instance)
        {
            _kernel.Inject(instance);
        }

        protected override void IocRegisterService(Type interfaceType, Type service)
        {
            _kernel.Bind(interfaceType).To(service);
        }

        protected override void IocRegisterServiceSingleton(Type interfaceType, Type service)
        {
            _kernel.Bind(interfaceType).To(service).InSingletonScope();
        }
    }
}
