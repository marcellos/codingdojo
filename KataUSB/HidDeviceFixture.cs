﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using NUnit.Framework;

namespace KataUSB
{
    [TestFixture]
    public class HidDeviceFixture
    {
        [Test]
        public void InitializeTest()
        {
            var device = UsbDevice.CreateDevice(HidDevice.ClassGuid, 0x046d, 0xc214, typeof(HidDevice)) as UsbDevice;
            Assert.IsInstanceOf(typeof(HidDevice), device);

            var disposable = device as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }
    }
}
