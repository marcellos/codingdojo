﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using System.Linq;
using KataSoundSynthesizer.Effect;
using KataSoundSynthesizer.Oscillators;
using KataSoundSynthesizer.Tone;

namespace KataSoundSynthesizer.SynthComponent
{
    class Voice : IVoice
    {
        private readonly IFilter _filter;
        private readonly IOscillator _voiceOscillator;
        private readonly IAdsrEnvelope _adsr;
        private readonly IOscillator _fmModulator;
        private float _frequency;
        private readonly float _amplitudeGain;
        private readonly float _frequencyGain;
        private readonly float _filterGain;
        private readonly IEffectComponent _effect;
        private readonly Scale _scale;
        private readonly bool _autoPanning;
        private float _panning = 0.5f;
        //private float[] _samples;
        //private readonly float[] _samplesNoEffect = new float[2];
        //private readonly float[] _silence = new float[2];
        private bool _isTriggered;
        private readonly MonoToStereoConverter _converter;

        public float Panning
        {
            get { return _panning; }
            set
            {
                if (value < 0.0f || value > 1.0f)
                {
                    throw new ArgumentOutOfRangeException("Panning", "allowed range: 0.0 - 1.0");
                }

                _panning = value;
            }
        }

        public Voice(
            IFilter filter,
            IOscillator voiceOscillator, 
            IAdsrEnvelope adsr, 
            IOscillator fmModulator, 
            float amplitudeGain, 
            float frequencyGain,
            float filterGain,
            IEffectComponent effect,
            bool autoPanning)
        {
            _filter = filter;
            _voiceOscillator = voiceOscillator;
            _adsr = adsr;
            _fmModulator = fmModulator;
            _amplitudeGain = amplitudeGain;
            _frequencyGain = frequencyGain;
            _filterGain = filterGain;
            _effect = effect;
            _autoPanning = autoPanning;
            _frequency = _voiceOscillator.Frequency;
            _scale = new Scale();
            _converter = new MonoToStereoConverter(_filter);
        }

        private Voice(Voice voice)
        {
            _voiceOscillator = (IOscillator) voice._voiceOscillator.MakeInstanceCopy();
            _adsr = (IAdsrEnvelope) voice._adsr.MakeInstanceCopy();
            _fmModulator = (IOscillator) voice._fmModulator.MakeInstanceCopy();

            var compOsc = (CompositeOscillator)_voiceOscillator;
            _filter = new StateVariableFilter(compOsc.Components.ElementAt(0), compOsc.Components.ElementAt(1), _adsr, _fmModulator)
                {
                    Resonance = voice._filter.Resonance,
                    CutOffFrequency = voice._filter.CutOffFrequency,
                    Filter = voice._filter.Filter,
                    Drive = voice._filter.Drive,
                    FilterGain = voice._filter.FilterGain,
                    Amplitude = voice._filter.Amplitude
                };

            _amplitudeGain = voice._amplitudeGain;
            _frequencyGain = voice._frequencyGain;
            _filterGain = voice._filterGain;

            if (voice._effect != null)
            {
                _effect = voice._effect.MakeInstanceCopy();
            }

            _frequency = voice._frequency;
            _scale = new Scale();
            _panning = voice._panning;
            _autoPanning = voice._autoPanning;
            _converter = new MonoToStereoConverter(_filter);
        }

        public IVoice MakeInstanceCopy()
        {
            return new Voice(this);
        }

        //public float[] GetSamples(long t)
        //{
        //    if (!_isTriggered)
        //    {
        //        return _silence;
        //    }

        //    var amplitude = _amplitudeGain;

        //    if (_adsr != null)
        //    {
        //        amplitude = Clamp(_amplitudeGain*_adsr.GetSample(t));
        //        _filter.Frequency = ClampNegative(_filter.Frequency + _filterGain * _adsr.GetSample(t));
        //    }

        //    if (_fmModulator != null)
        //    {
        //        _voiceOscillator.Frequency = ClampNegative(_frequency + _frequencyGain*_fmModulator.GetSample(t));
        //    }

        //    var sample = amplitude*_filter.GetSample(t);

        //    if (_effect == null)
        //    {
        //        _samplesNoEffect[0] = sample;
        //        _samplesNoEffect[1] = sample;
        //        return _samplesNoEffect;
        //    }

        //    _samples = _effect.GetSamples(sample, sample);

        //    _samples[0] = _samples[0]*(1.0f - Panning);
        //    _samples[1] = _samples[1]*Panning;

        //    return _samples;
        //}

        public void RenderSamples(int offset, int count)
        {
            if (!_isTriggered)
            {
                return;
            }

            _voiceOscillator.Frequency = ClampNegative(_frequency);

            _filter.RenderSamples(offset, count);
            _converter.RenderSamples(offset, count);

            if (_effect != null)
            {
                _effect.Apply(_converter.GetStereoBuffer());
            }
        }

        public float[,] GetStereoBuffer()
        {
            return _converter.GetStereoBuffer();
        }

        private static float Clamp(float value)
        {
            return Math.Max(Math.Min(value, 1.0f), 0.0f);
        }

        private static float ClampNegative(float value)
        {
            return Math.Max(value, 0.0f);
        }

        public void TriggerKey(TrackedKey key)
        {
            _isTriggered = true;

            if (_adsr == null)
            {
                return;
            }

            _frequency = (float) _scale.Tones[key.NoteNumber];
            _adsr.Trigger(key.NoteNumber, key.Velocity);
            _filter.TriggerKey(key.NoteNumber);

            if (_autoPanning)
            {
                Panning = GetAutoPanningValue(key.NoteNumber, Scale.MaxToneIndex);
            }
        }

        public void ReleaseKey(TrackedKey key)
        {
            if (_adsr == null)
            {
                return;
            }

            _adsr.Release(key.NoteNumber); 
        }

        private static float GetAutoPanningValue(int noteNumber, int maxNoteNumber)
        {
            return 1.0f/((float) maxNoteNumber/noteNumber);
        }
    }
}
