﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace KataRx.Coroutines3
{
    [TestFixture]
    public class CoroutineTests
    {
        private ContinuationExecutionContext _continuationExecutionContext;

        [SetUp]
        public void Setup()
        {
            _continuationExecutionContext = new ContinuationExecutionContext();
        }

        [TearDown]
        public void Teardown()
        {
            _continuationExecutionContext = null;
        }

        [Test]
        public void CoroutineTest()
        {
            _continuationExecutionContext.Target = this;
            _continuationExecutionContext.Method = GetType().GetMethod("TestMethod");

            var returnValue = _continuationExecutionContext.Method.Invoke(_continuationExecutionContext.Target, null);
            var result = returnValue as IContinuationResult;
            if (result != null)
            {
                returnValue = new[] { result };
            }

            var enumerable = returnValue as IEnumerable<IContinuationResult>;
            if (enumerable != null)
            {
                Coroutine.BeginExecute(enumerable.GetEnumerator(), _continuationExecutionContext);
            }

        }

        [Test]
        public void CoroutineAsyncTest()
        {
            _continuationExecutionContext.Target = this;
            _continuationExecutionContext.Method = GetType().GetMethod("DiskTestMethod");

            var returnValue = _continuationExecutionContext.Method.Invoke(_continuationExecutionContext.Target, null);
            var result = returnValue as IContinuationResult;
            if (result != null)
            {
                returnValue = new[] {result};
            }

            var enumerable = returnValue as IEnumerable<IContinuationResult>;
            if (enumerable != null)
            {
                Coroutine.BeginExecute(enumerable.GetEnumerator(), _continuationExecutionContext);
            }
        }

        public IEnumerable<IContinuationResult> TestMethod()
        {
            const string message = "Print these messages to Console.";
            var words = message.Split(' ');
            return words.Select(PrintResult.WithMessage);
        }

        public IEnumerable<IContinuationResult> DiskTestMethod()
        {
            yield return DiskResult.WithExecutingAssembly();
            yield return DiskResult.WithExecutingAssembly();

            // this is a demonstration how to retrieve the context during execution
            // while the result is executed async
            var diskResult = DiskResult.WithExecutingAssembly();
            var localValue = 0;
            diskResult.SetContextCallback(context => localValue = (int)context.State);
            Console.WriteLine("> localValue is {0}", localValue);
            yield return diskResult;
            Console.WriteLine("> localValue is now {0}", localValue);

            yield return DiskResult.WithExecutingAssembly();
        }

        public class PrintResult : IContinuationResult
        {
            private readonly string _message = string.Empty;

            private PrintResult(string message)
            {
                _message = message;
            }

            public void Execute(ContinuationExecutionContext context)
            {
                Exception error = null;
                var cancel = false;
                try
                {
                    Console.WriteLine(_message);
                    System.Threading.Thread.Sleep(30); // simulate some cpu activity
                    cancel = _message.Equals("cancel");
                    //throw new Exception("there is a problem");
                }
                catch (Exception ex)
                {
                    error = ex;
                }
                finally
                {
                    Completed(this, new ContinuationResultCompletionEventArgs {Error = error, WasCancelled = cancel});
                }
            }

            public event EventHandler<ContinuationResultCompletionEventArgs> Completed = delegate { };
            public void SetContextCallback(Action<ContinuationExecutionContext> callback)
            {
                throw new NotImplementedException();
            }

            public static IContinuationResult WithMessage(string message)
            {
                return new PrintResult(message);
            }
        }

        public class DiskResult : IContinuationResult
        {
            private readonly string _location;
            private ReadState _state;
            private int _bufferOffset;
            private Stopwatch _stopwatch;
            private ContinuationExecutionContext _context;
            private Action<ContinuationExecutionContext> _callback;

            public DiskResult(string location)
            {
                _location = location;
                _stopwatch = new Stopwatch();
            }

            public void Execute(ContinuationExecutionContext context)
            {
                // exchange information between calls via context state
                _context = context;
                if (context.State != null)
                {
                    Console.WriteLine("state is: '{0}'", (int)context.State);
                }

                _stopwatch.Start();
                _bufferOffset = 0;
                var fs = new FileStream(_location, FileMode.Open, FileAccess.Read);
                var fi = new FileInfo(_location);
                _state = new ReadState(fs, (int)fi.Length);
                var asyncResult = fs.BeginRead(_state.Buffer, _bufferOffset, (int)fi.Length, AsyncReadCallback, _state);
            }

            private void AsyncReadCallback(IAsyncResult asyncResult)
            {
                var state = (ReadState) asyncResult.AsyncState;
                var bytesRead = state.Stream.EndRead(asyncResult);
                Console.WriteLine("bytes read: '{0}'", bytesRead);
                state.Stream.Close();
                state.Stream.Dispose();

                _stopwatch.Stop();
                Console.WriteLine("time elapsed [ms]: {0}", _stopwatch.ElapsedMilliseconds);

                // change context state
                if (_context.State != null)
                {
                    _context.State = ((int)_context.State) + 1;
                    // call into emitting method
                    if (_callback != null)
                    {
                        _callback(_context);
                    }
                }
                else
                {
                    _context.State = 0;
                }

                Completed(this, new ContinuationResultCompletionEventArgs());
            }

            public event EventHandler<ContinuationResultCompletionEventArgs> Completed = delegate { };
            public void SetContextCallback(Action<ContinuationExecutionContext> callback)
            {
                _callback = callback;
            }

            public static IContinuationResult WithExecutingAssembly()
            {
                return new DiskResult(Assembly.GetExecutingAssembly().Location);
            }

            private class ReadState
            {
                private readonly FileStream _fs;
                private readonly byte[] _buffer;

                public FileStream Stream { get { return _fs; } }
                public byte[] Buffer { get { return _buffer; } }

                public ReadState(FileStream fs, int bufferSize)
                {
                    _fs = fs;
                    _buffer = new byte[bufferSize];
                }
            }
        }
    }
}
