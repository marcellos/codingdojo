﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using KataSoundSynthesizer.SynthComponent;

namespace KataSoundSynthesizer.Oscillators
{
    class Oscillator2 : SynthComponentBase, IOscillator
    {
        public const float PI2 = (float) (2*Math.PI);
        private readonly Random _random = new Random();
        private readonly IOscillator _fmModulator;
        private readonly IOscillator _dutyCycleModulator;
        private WaveFormEnum _waveForm;
        private float _amplitude = 1f;
        private float _sine = 0f;
        private float _cosine = 1f;
        private float _increment;
        private float _accumulator;
        private float _frequency = 1f;
        private float _cutoff = 1f;
        private float _phase = 0f;
        private float _dutyCycle = (float)Math.PI;
        private float _sample;
        private float _outputSample;
        private int _sampleRate;
        private Func<float> _waveFormFunc;

        public WaveFormEnum WaveForm
        {
            get { return _waveForm; }
            set
            {
                _waveForm = value;
                _sine = 0;
                _cosine = 1;

                switch (_waveForm)
                {
                    case WaveFormEnum.Sine:
                        _waveFormFunc = GetSineSample;
                        break;

                    case WaveFormEnum.Square:
                        _waveFormFunc = GetSquareSample;
                        break;

                    case WaveFormEnum.Noise:
                        _waveFormFunc = GetNoiseSample;
                        _accumulator = 0;                        
                        break;

                    case WaveFormEnum.Triangle:
                        _waveFormFunc = GetTriangleSample;
                        _accumulator = 0.25f*PI2;
                        break;

                    case WaveFormEnum.Sawtooth:
                        _waveFormFunc = GetSawtoothSample;
                        _accumulator = (float) Math.PI;
                        break;

                    default:
                        break;
                }
            }
        }

        public float Amplitude
        {
            get { return _amplitude; }
            set
            {
                if (value < 0f || value > 1f)
                {
                    throw new ArgumentOutOfRangeException("Amplitude", "allowed range: 0 - 1");
                }

                _amplitude = value;
            }
        }

        public float Frequency
        {
            get { return _frequency; }
            set
            {
                if (value < 0f)
                {
                    throw new ArgumentOutOfRangeException("Frequency", "must be > 0");
                }

                _frequency = value;
                CalculateIncrement();
            }
        }

        public float Phase
        {
            get { return _phase; }
            set
            {
                if (value < 0f || value > PI2)
                {
                    throw new ArgumentOutOfRangeException("Phase", "allowed range: 0 - PI2");
                }

                _phase = value;
            }
        }

        public float DutyCycle
        {
            get { return _dutyCycle; }
            set
            {
                if (value < 0f || value > PI2)
                {
                    throw new ArgumentOutOfRangeException("DutyCycle", "allowed range: 0 - PI2");
                }

                _dutyCycle = value;
            }
        }

        public float CutOff
        {
            get { return _cutoff; }
            set
            {
                if (value < 0f || value > 1f)
                {
                    throw new ArgumentOutOfRangeException("CutOff", "allowed range: 0 - 1");
                }

                _cutoff = value;
            }
        }

        public float Detune { get; set; }
        public float FmLevel { get; set; }

        public OctaveMultiplier Multiplier { get; set; }

        public override int SampleRate
        {
            get { return _sampleRate; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("SampleRate", "must be >= 1");
                }

                _sampleRate = value;
                CalculateIncrement();
            }
        }

        public Oscillator2()
        {       
            WaveForm = WaveFormEnum.Sine;
            Multiplier = OctaveMultiplier.x1;
        }

        public Oscillator2(IOscillator fmModulator, bool useFmModulator)
            : this()
        {
            _fmModulator = fmModulator;
        }

        public Oscillator2(IOscillator fmModulator, IOscillator dutyCycleModulator)
            : this()
        {
            _fmModulator = fmModulator;
            _dutyCycleModulator = dutyCycleModulator;
        }

        public Oscillator2(IOscillator osc)
        {
            Detune = osc.Detune;
            WaveForm = osc.WaveForm;
            Amplitude = osc.Amplitude;
            Frequency = osc.Frequency;
            Phase = osc.Phase;
            DutyCycle = osc.DutyCycle;
            CutOff = osc.CutOff;
            SampleRate = osc.SampleRate;
            Multiplier = osc.Multiplier;
            _fmModulator = ((Oscillator2) osc)._fmModulator;
        }


        public override void RenderSamples(int offset, int count)
        {
            InitialiseMonoBuffer(count);
            var fmBuffer = new float[count];
            if (_fmModulator != null)
            {
                _fmModulator.RenderSamples(offset, count);
                fmBuffer = _fmModulator.GetMonoBuffer();
            }

            var dutyCycleBuffer = new float[count];
            if (_dutyCycleModulator != null)
            {
                _dutyCycleModulator.RenderSamples(offset, count);
                dutyCycleBuffer = _dutyCycleModulator.GetMonoBuffer();
            }

            var sampleCount = offset + count;
            for (var i = offset; i < sampleCount; ++i)
            {
                Frequency += fmBuffer[i]*FmLevel;
                DutyCycle += dutyCycleBuffer[i];
                _outputSample = _waveFormFunc();
                _outputSample *= _amplitude;
                _monoBuffer[i] = _outputSample;
            }
        }

        public override ISynthComponent MakeInstanceCopy()
        {
            return new Oscillator2(this);
        }

        private void CalculateIncrement()
        {
            _increment = (PI2 - _phase) * (_frequency * (int)Multiplier + Detune) / SampleRate;
        }

        private void IncrementPhase()
        {
            _sine = _sine + _increment*_cosine;
            _cosine = _cosine - _increment*_sine;

            _accumulator += _increment;
            if (_accumulator >= PI2)
            {
                _accumulator -= PI2;
            }
        }

        private float GetSineSample()
        {
            _sample = _sine;
            IncrementPhase();
            return _sample;
        }

        private float GetSquareSample()
        {
            if (_accumulator < _dutyCycle)
            {
                _sample = 1;
            }
            else
            {
                _sample = -1;
            }

            IncrementPhase();
            return _sample;
        }

        private float GetTriangleSample()
        {
            if (_accumulator < Math.PI)
            {
                _sample = 1 - 4*_accumulator/PI2;
            }
            else
            {
                _sample = 1 - 4*(1 - _accumulator/PI2);
            }

            IncrementPhase();
            return _sample;
        }

        private float GetSawtoothSample()
        {
            _sample = 1 - 2*_accumulator/PI2;
            IncrementPhase();
            return _sample;
        }

        private float GetNoiseSample()
        {
            _sample = (float) (1 - 2*_random.NextDouble());
            return _sample;
        }

        private float Cutoff(float sample)
        {
            return (Math.Abs(sample) > _cutoff)
                       ? Math.Sign(sample)*_cutoff
                       : sample;
        }
    }
}
