﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System.Runtime.InteropServices;

namespace KataSoundSynthesizer
{
    [StructLayout(LayoutKind.Explicit, Pack = 2)]
    class WaveBuffer
    {
        [FieldOffset(0)]
        private int _numberOfBytes;

        [FieldOffset(8)] 
        private readonly byte[] _byteBuffer;

        [FieldOffset(8)] 
        private readonly float[] _floatBuffer;

        public WaveBuffer(byte[] byteBuffer)
        {
            _byteBuffer = byteBuffer;
            _numberOfBytes = 0;
        }

        public byte[] ByteBuffer { get { return _byteBuffer; } }

        public float[] FloatBuffer { get { return _floatBuffer; } }
    }
}
