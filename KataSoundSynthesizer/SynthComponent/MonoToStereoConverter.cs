﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion


namespace KataSoundSynthesizer.SynthComponent
{
    class MonoToStereoConverter
    {
        private float[,] _stereoBuffer;
        private readonly ISynthComponent _input;

        public MonoToStereoConverter(ISynthComponent input)
        {
            _stereoBuffer = new float[2,1];
            _input = input;
        }

        public void RenderSamples(int offset, int count)
        {
            if (_stereoBuffer != null && _stereoBuffer.Length/2 != count)
            {
                _stereoBuffer = new float[2, count];
            }

            var buffer = _input.GetMonoBuffer();

            var sampleCount = offset + count;
            for (var i = 0; i < sampleCount; ++i)
            {
                _stereoBuffer[0, i] += buffer[i];
                _stereoBuffer[1, i] += buffer[i];
            }
        }

        public float[,] GetStereoBuffer()
        {
            return _stereoBuffer;
        }
    }
}
