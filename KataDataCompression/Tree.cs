﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KataDataCompression
{
    class Tree
    {
        private readonly int _bufferSize;
        private readonly int _upperMatchLength;
        private readonly int None;
        private readonly int[] _parent;
        private readonly int[] _leftChild;
        private readonly int[] _rightChild;

        public Tree(int bufferSize, int upperMatchLength)
        {
            // check buffersize > 0

            _bufferSize = bufferSize;
            _upperMatchLength = upperMatchLength;
            None = _bufferSize;
            _parent = new int[_bufferSize + 1];
            _leftChild = new int[_bufferSize + 1];
            _rightChild = new int[_bufferSize + 257];
        }

        public void Init()
        {
            for (var i = _bufferSize + 1; i <= _bufferSize + 256; i++)
            {
                _rightChild[i] = None;
            }

            for (var i = 0; i < _bufferSize; i++)
            {
                _parent[i] = None;
            }
        }

        public void InsertNode(int r)
        {
        }

        public void DeleteNode(int p)
        {
        }
    }
}
