﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using NUnit.Framework;

namespace KataEventSync
{
    [TestFixture]
    public class UserRulesEngineFixture
    {
        private IEventAggregator _events;
        private RulesEngine<User> _engine;

        [SetUp]
        public void Setup()
        {
            _events = new EventAggregator();
            _engine = new RulesEngine<User>(_events);
        }

        [TearDown]
        public void Teardown()
        {
            _events = null;
            _engine = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(RulesEngine<User>), _engine);
        }

        [Test]
        public void Rule4Test()
        {
            var recipient = new Recipient();
            _events.Subscribe(recipient);
            var rule = new Rule4();
            _engine.AddRule(rule);
            _events.Publish(new User("Fred"));
            _events.Publish(new User("Mike"));
            Assert.AreEqual("$rule4", recipient.Message);
            _engine.RemoveRule(rule);
            _events.Unsubscribe(recipient);
        }

        [Test]
        public void Rule5Test()
        {
            var recipient = new Recipient();
            _events.Subscribe(recipient);
            var rule = new Rule5();
            _engine.AddRule(rule);
            _events.Publish(new User("Fred", true));
            Assert.AreEqual(null, recipient.Message);
            _events.Publish(new User("Fred"));
            Assert.AreEqual(null, recipient.Message);
            _events.Publish(new User("Mike", true));
            Assert.AreEqual(null, recipient.Message);
            _events.Publish(new User("Fred", true));
            Assert.AreEqual("$rule5", recipient.Message);
            _engine.RemoveRule(rule);
            _events.Unsubscribe(recipient);
        }

        internal class Recipient : IHandle<string>
        {
            public string Message { get; private set; }

            public void Handle(string message)
            {
                Message = message;
            }
        }
    }
}
