﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;

namespace KataRx.Coroutines2
{
    public interface ISequentialResult
    {
        List<ISequentialResult> Results { get; }
        object Value { get; }
    }
}
