﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using NUnit.Framework;

namespace KataCodeGeneration
{
    [TestFixture]
    public class ScratchPad
    {
        private static string CurrentDirectory = TestContext.CurrentContext.TestDirectory;

        [Test]
        public void PrintTokens()
        {
            var doc = XDocument.Load(Path.Combine(CurrentDirectory, "FCS.xml"));
            var root = doc.Root;

            // enumerate all tokens
            const string Command = "Command";
            const string Token = "token";
            var tokens =
                (from c in root.Descendants(Command).AsParallel()
                 where c.Attributes(Token).Any()
                 orderby c.Attribute(Token).Value
                 select c.Attribute(Token).Value).Distinct();

            Console.WriteLine("-= tokens =-");
            foreach (var token in tokens)
            {
                Console.WriteLine(token);
            }
        }

        [Test]
        public void PrintEncoding()
        {
            var doc = XDocument.Load(Path.Combine(CurrentDirectory, "FCS.xml"));
            var root = doc.Root;

            const string Command = "Command";
            const string Name = "name";
            const string Token = "token";

            var consts = root.Descendants(Command).Select(
                cmd => string.Format("public const string {0} = \"{1}\";",
                                     cmd.Attribute(Name).Value,
                                     cmd.Attribute(Token).Value));
            foreach (var @const in consts)
            {
                Console.WriteLine(@const);
            }
        }

        [Test]
        public void PrintEncoding2()
        {
            var path = Path.Combine(CurrentDirectory, "FCS.xml");
            var doc = new XmlDocument();
            doc.Load(path);
            var commands = doc.SelectNodes("//Commands/Command");

            var sb = new StringBuilder();
            foreach (XmlNode command in commands)
            {
                sb.AppendFormat("public const string {0} = \"{1}\";", command.Attributes["name"].Value, command.Attributes["token"].Value);
                sb.AppendLine();
            }

            Console.WriteLine(sb.ToString());
        }

        [Test]
        public void PrintAck()
        {
            var doc = XDocument.Load(Path.Combine(CurrentDirectory, "FCS.xml"));
            var root = doc.Root;

            // enumerate acknowledge
            const string Command = "Command";
            const string Ack = "ack";
            var acks =
                (from c in root.Descendants(Command).AsParallel()
                 where c.Attributes(Ack).Any()
                 orderby c.Attribute(Ack).Value
                 select c.Attribute(Ack).Value).Distinct();

            Console.WriteLine("-= acks =-");
            foreach (var ack in acks)
            {
                Console.WriteLine(ack);
            }
        }

        [Test]
        public void PrintParameters()
        {
            var doc = XDocument.Load(Path.Combine(CurrentDirectory, "FCS.xml"));
            var root = doc.Root;

            // enumerate parameters
            const string Command = "Command";
            const string Name = "name";
            //const string SSC = "SetSerialtunnelConfiguration";
            const string SOD = "SetOtpData";
            var attributeLists =
                from c in root.Descendants(Command).AsParallel()
                where c.Attribute(Name).Value == SOD
                select c.Attributes();

            const string Param = "param";
            var paramAttribute =
                from l in attributeLists
                select l.Where(a => a.Name.ToString().StartsWith(Param));

            Console.WriteLine("-= params =-");
            //foreach (var a in paramAttribute.SelectMany(param => param))
            //{
            //    Console.WriteLine(a);
            //}
            var paramList = Enumerable.ToList(
                paramAttribute.SelectMany(param => param)
                .Select(a => a.Value + " " + a.Name.ToString().Replace(Param, string.Empty))
                );
            Console.WriteLine(string.Join(", ", paramList));
        }

        [Test]
        public void FormatTestCommand()
        {
            ITestCommandFormatter<byte[]> formatter = new TestCommandFormatter();
            var cmd = new TestCommand(23);
            var formattedString = Encoding.Default.GetString(formatter.Format(cmd));
            Console.WriteLine("formatted cmd: '{0}'", formattedString);
        }

        [Test]
        public void ParseTestCommand()
        {
            ITestCommandParser<byte[]> parser = new TestCommandParser();
            var msg = Encoding.Default.GetBytes("test-cmd-23");
            var cmd = parser.Parse(msg);
            Assert.IsInstanceOf<TestCommand>(cmd);
            Console.WriteLine((cmd as TestCommand).Value);
        }

        [Test]
        public void FormatCommandParameter()
        {
            foreach (XmlNode node in GetNodes())
            {
                var attrs = GetParams(node);
                var parameterDeclarationList = ParameterDeclarationList(attrs);
                Console.WriteLine(parameterDeclarationList);
            }

            foreach (XmlNode node in GetNodes())
            {
                var attrs = GetParams(node);
                var propertyDeclaration = PropertyDeclarationList(attrs);
                Console.WriteLine(propertyDeclaration);
            }

            foreach (XmlNode node in GetNodes())
            {
                var attrs = GetParams(node);
                var propertyAssignment = PropertyAssignmentList(attrs);
                Console.WriteLine(propertyAssignment);
            }
        }

        static XmlNodeList GetNodes()
        {
            var path = Path.Combine(CurrentDirectory, "FCS.xml");
            var doc = new XmlDocument();
            doc.Load(path);
            var commands = doc.SelectNodes("//Commands/Command");
            return commands;
        }

        static XmlAttribute[] GetParams(XmlNode node)
        {
            var attrs = new XmlAttribute[node.Attributes.Count];

            var i = 0;
            foreach (XmlAttribute attr in node.Attributes)
            {
                if (attr.Name.StartsWith("param"))
                {
                    attrs[i++] = attr;
                }
            }

            return attrs;
        }

        static string ParameterDeclarationList(XmlAttribute[] attrs)
        {
            var sb = new StringBuilder();
            foreach (var attr in attrs)
            {
                if (attr == null) continue;
                if (sb.Length > 0) sb.Append(", ");
                sb.Append(FormatParameterDeclaration(attr.Name, attr.Value));
            }

            return sb.ToString();
        }

        static string FormatParameterDeclaration(string name, string type)
        {
            var strippedName = StripParamName(name);
            var lowerCaseName = LowerCaseName(strippedName);
            return string.Format("{0} {1}", type.ToLower(), lowerCaseName);
        }

        static string StripParamName(string name)
        {
            return name.Replace("param", string.Empty);
        }

        static string LowerCaseName(string name)
        {
            return name.Substring(0, 1).ToLower() 
                + name.Substring(1, name.Length - 1);
        }

        static string PropertyDeclarationList(XmlAttribute[] attrs)
        {
            var sb = new StringBuilder();
            foreach (var attr in attrs)
            {
                if (attr == null) continue;
                sb.AppendLine(FormatPropertyDeclaration(attr.Name, attr.Value));
            }

            return sb.ToString();
        }

        static string FormatPropertyDeclaration(string name, string type)
        {
            var strippedName = StripParamName(name);
            return string.Format("public {0} {1} {2}", type.ToLower(), strippedName, "{get; private set;}");
        }

        static string PropertyAssignmentList(XmlAttribute[] attrs)
        {
            var sb = new StringBuilder();
            foreach (var attr in attrs)
            {
                if (attr == null) continue;
                sb.AppendLine(FormatPropertyAssignment(attr.Name));
            }

            return sb.ToString();
        }

        static string FormatPropertyAssignment(string name)
        {
            var strippedName = StripParamName(name);
            var lowerCaseName = LowerCaseName(strippedName);
            return string.Format("{0} = {1};", strippedName, lowerCaseName);
        }

        [Test]
        public void FormatterTest()
        {
            var encoding = GetEncodingClass();
            Console.WriteLine(encoding);
            Console.WriteLine();

            foreach (XmlNode node in GetNodes())
            {
                var name = node.Attributes["name"].Value;
                Console.WriteLine(name);
                var lines = RenderFormattingLines(encoding, name);
                Console.WriteLine(lines);
                Console.WriteLine();
            }
        }

        static string RenderFormattingLines(string encoding, string name)
        {
            var sb = new StringBuilder();
            var formattingLines = GetFormattingLines(name);
            foreach (var formattingLine in formattingLines)
            {
                var line = CleanCrLf(formattingLine);
                if (line.Length == 0) continue;
                line = ExpandEncoding(encoding, name, line);
                sb.AppendLine(StringBuilderAppend(line));
            }

            return sb.ToString();
        }

        static string GetEncodingClass()
        {
            var path = Path.Combine(CurrentDirectory, "FCS.xml");
            var doc = new XmlDocument();
            doc.Load(path);
            var formatting = doc.SelectNodes("//Formatting");
            return formatting.Item(0).Attributes["encoding"].Value;
        }

        static string[] GetFormattingLines(string name)
        {
            var path = Path.Combine(CurrentDirectory, "FCS.xml");
            var doc = new XmlDocument();
            doc.Load(path);
            var format = doc.SelectNodes(string.Format("//Formatting/Format[@name='{0}']", name));
            return format.Count == 0 
                ? new string[0] 
                : format.Item(0).InnerText.Split('\n');
        }

        static string CleanCrLf(string line)
        {
            return line.Replace("\r", string.Empty).Replace("\n", string.Empty).Trim();
        }

        static string ExpandEncoding(string encoding, string name, string line)
        {
            const string nameKey = "%name%";
            if (line.Contains(nameKey))
            {
                return encoding + "." + line.Replace(nameKey, name);
            }
            return line;
        }

        static string StringBuilderAppend(string line)
        {
            return string.Format("sb.Append({0});", line);
        }

        [Test]
        public void FormatterInterfaceTest()
        {
            var formatCalls = RenderFormatCalls(GetNodes());
            Console.WriteLine(formatCalls);
            Console.WriteLine();
        }

        static string RenderFormatCalls(XmlNodeList nodes)
        {
            var sb = new StringBuilder();
            foreach (XmlNode node in nodes)
            {
                var name = node.Attributes["name"].Value;
                sb.AppendLine(FormatCall(name));
            }

            return sb.ToString();
        }

        static string FormatCall(string name)
        {
            return string.Format("    T Format({0}Command cmd);", name);
        }
    }
}
