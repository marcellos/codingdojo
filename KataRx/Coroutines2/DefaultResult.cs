﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataRx.Coroutines2
{
    public class DefaultResult : BaseResult<object>
    {
        public DefaultResult(IObserver<ISequentialResult> observer)
            : base(observer)
        {
        }
    }
}
