/*

JavaScript for Beginners

*/

function Sum1to10() {
    var total = 0, count = 1;
    while (count <= 10) {
        total += count;
        count += 1;
    }
    return total;
}
//alert(Sum1to10());

function TwoByPowerOf10() {
    var power = 1, counter = 0;
    while (counter < 10) {
        power *= 2;
        counter++;
    }
    return power;
}
//alert(TwoByPowerOf10());

function Guess2Plus2() {
    var answer = prompt("What is the value of 2 + 2?", "");
    if (answer === "4") // === means no automatic type conversion
        alert("You genious.");
    else if (answer == "3" || answer == "5")
        alert("Almost!");
    else
        alert("What an embarrassment!");
}
//Guess2Plus2();

// divide a by b
function Division(a, b) {
  var result = 0.0e1;
  if (b === 0) {
    throw "Division by zero!";
  }
  else {
    result = a / b;
  }
  return result;
}
//console.log(Division(10, 5));

// for loop
function ForLoop(count) {
    var result = "";

    if (count < 0)
        throw "count must be positive!";

    for (i = 0; i < count; i++)
        result += i.toString() + " ";
    return result.trim();
}

try {
    console.log(ForLoop(-1));
} catch(ex) {
    console.log(ex);
} finally {
    console.log("exception was handled.");
}


function AssignArgumentsAndVariables(a, b) {
    var a, b;

//    a = b;
//    b = a;

    console.log("a: " + a.toString() + "; b: " + b.toString());
}
//AssignArgumentsAndVariables(6, 7);

var MyObject = {
    MyOtherObject : {
        MyValue : 23
    }
};
MyObject.MyOtherObject.MyValue = 67;
console.log(MyObject.MyOtherObject.MyValue);