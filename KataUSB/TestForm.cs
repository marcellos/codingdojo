﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Windows.Forms;

namespace KataUSB
{
    public partial class TestForm : Form
    {
        public UsbListener Listener { private get; set; }

        public TestForm()
        {
            InitializeComponent();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            if (Listener != null)
                Listener.RegisterHID();
        }

        private void TestForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Listener != null)
                Listener.UnregisterHID();
        }
    }
}
