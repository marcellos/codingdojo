#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataRx
{
    internal interface ITestEventSource
    {
        event EventHandler<TestEventArgs> TestEvent;
        void FireOff();
    }
}