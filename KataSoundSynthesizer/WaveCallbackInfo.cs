﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataSoundSynthesizer
{
    class WaveCallbackInfo
    {
        private WaveWindow _waveOutWindow;
        private WaveWindowNative _waveOutWindowNative;

        public WaveCallbackStrategy Strategy { get; private set; }
        public IntPtr Handle { get; private set; }

        private WaveCallbackInfo(WaveCallbackStrategy strategy, IntPtr handle)
        {
            Strategy = strategy;
            Handle = handle;
        }

        public static WaveCallbackInfo FunctionCallback()
        {
            return new WaveCallbackInfo(WaveCallbackStrategy.FunctionCallback, IntPtr.Zero);
        }

        public static WaveCallbackInfo NewWindow()
        {
            return new WaveCallbackInfo(WaveCallbackStrategy.NewWindow, IntPtr.Zero);
        }

        public static WaveCallbackInfo ExistingWindow(IntPtr handle)
        {
            if (handle == IntPtr.Zero)
            {
                throw new ArgumentNullException("handle");
            }

            return new WaveCallbackInfo(WaveCallbackStrategy.ExistingWindow, handle);
        }

        public void Connect(WaveCallback waveCallback)
        {
            if (Strategy == WaveCallbackStrategy.NewWindow)
            {
                _waveOutWindow = new WaveWindow(waveCallback);
                _waveOutWindow.CreateControl();
                Handle = _waveOutWindow.Handle;
            }
            else if (Strategy == WaveCallbackStrategy.ExistingWindow)
            {
                _waveOutWindowNative = new WaveWindowNative(waveCallback);
                _waveOutWindowNative.AssignHandle(Handle);
            }
        }

        public MmResult WaveOutOpen(out IntPtr waveOutHandle, int deviceNumber, WaveFormat waveFormat,
                                    WaveCallback waveCallback)
        {
            MmResult result;
            if (Strategy == WaveCallbackStrategy.FunctionCallback)
            {
                result = WinMultiMediaApi.waveOutOpen(out waveOutHandle, (IntPtr) deviceNumber, waveFormat, waveCallback,
                                                      IntPtr.Zero, WaveInOutOpenFlags.CallbackFunction);
            }
            else
            {
                result = WinMultiMediaApi.waveOutOpenWindow(out waveOutHandle, (IntPtr) deviceNumber, waveFormat, Handle,
                                                            IntPtr.Zero, WaveInOutOpenFlags.CallbackWindow);
            }

            return result;
        }

        public void Disconnect()
        {
            if (_waveOutWindow != null)
            {
                _waveOutWindow.Close();
                _waveOutWindow = null;
            }

            if (_waveOutWindowNative != null)
            {
                _waveOutWindowNative.ReleaseHandle();
                _waveOutWindowNative = null;
            }
        }
    }

    public enum WaveCallbackStrategy
    {
        FunctionCallback,
        NewWindow,
        ExistingWindow,
        Event
    }
}
