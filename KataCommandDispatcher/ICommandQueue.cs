﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Threading;

namespace KataCommandDispatcher
{
    public interface ICommandQueue
    {
        void Enqueue<TCommand>(TCommand command) where TCommand : class, ICommand;
        CommandBindings Dequeue(CancellationToken token);
    }
}
