﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace KataMachineLearning
{
    [TestFixture]
    public class StatisticsTest
    {
        [Test]
        public void Max_WhenTypeInt_ThenGetMaxValue()
        {
            var series = new[] {1, 3, 4, 2};
            var max = Statistics.Max(series);
            Assert.AreEqual(4, max);
        }

        [Test]
        public void Max_WhenSeriesIsNull_ThenGetDefault()
        {
            var max = Statistics.Max<int>(null);
            Assert.AreEqual(0, max);
        }

        [Test]
        public void Max_WhenSeriesIsEmpty_ThenGetDefault()
        {
            var series = new int[0];
            var max = Statistics.Max(series);
            Assert.AreEqual(0, max);
        }


        [Test]
        public void Max_WhenTypeDouble_ThenGetMaxValue()
        {
            var series = new[] {1.1, 3.4, 4.5, 2.3};
            var max = Statistics.Max(series);
            Assert.AreEqual(4.5, max);
        }
    }
}
