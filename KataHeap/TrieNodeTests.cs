﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using NUnit.Framework;

namespace KataHeap
{
    [TestFixture]
    public class TrieNodeTests
    {
        private TrieNode<int> _trieNode;

        [SetUp]
        public void Setup()
        {
            _trieNode = new TrieNode<int>('A');
        }

        [TearDown]
        public void Teardown()
        {
            _trieNode = null;
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(TrieNode<int>), _trieNode);
        }

        [Test]
        public void Key_ReturnsA()
        {
            Assert.AreEqual('A', _trieNode.Key);
        }

        [Test]
        public void AddChildNode_WithKey_ThenChildNodesContainsNode()
        {
            const char key = 'B';
            Assert.IsNull(_trieNode.GetChildNode(key));
            var node = _trieNode.AddChildNode(key);
            Assert.AreEqual(node, _trieNode.GetChildNode(key));
        }

        [Test]
        public void AddChildNode_WithSameKey_ThenReturnSameNode()
        {
            var node = _trieNode.AddChildNode('B');
            Assert.AreEqual(node, _trieNode.AddChildNode('B'));
        }

        [Test]
        public void ContainsChildNode_WithNoChildNodes_ThenReturnFalse()
        {
            Assert.IsFalse(_trieNode.ContainsChildNode('B'));
        }

        [Test]
        public void ContainsChildNode_WithOneNode_ThenReturnTrue()
        {
            const char key = 'B';
            _trieNode.AddChildNode(key);
            Assert.IsTrue(_trieNode.ContainsChildNode(key));
        }

        [Test]
        public void ContainsChildNode_WithOtherNode_ThenReturnFalse()
        {
            _trieNode.AddChildNode('B');
            Assert.IsFalse(_trieNode.ContainsChildNode('C'));
        }

        [Test]
        public void GetChildNode_WithNoChildNodes_ThenReturnNull()
        {
            Assert.IsNull(_trieNode.GetChildNode('B'));
        }

        [Test]
        public void GetChildNode_WithOneNode_ThenReturnNode()
        {
            const char key = 'B';
            _trieNode.AddChildNode(key);
            Assert.IsInstanceOf(typeof(TrieNode<int>), _trieNode.GetChildNode(key));
        }

        [Test]
        public void GetChildNode_WithOtherNode_ThenReturnNull()
        {
            _trieNode.AddChildNode('B');
            Assert.IsNull(_trieNode.GetChildNode('C'));
        }
    }
}
