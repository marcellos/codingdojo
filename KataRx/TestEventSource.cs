﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace KataRx
{
    class TestEventSource : ITestEventSource
    {
        private int _fireOffEventInSeconds;

        public event EventHandler<TestEventArgs> TestEvent;

        public TestEventSource(int fireOffEventInSeconds)
        {
            _fireOffEventInSeconds = fireOffEventInSeconds;
        }

        public void FireOff()
        {
            ThreadPool.QueueUserWorkItem(DoFire);
        }

        private void DoFire(object context)
        {
            // blocking call on thread
            Thread.Sleep(_fireOffEventInSeconds * 1000);
            if (TestEvent != null)
            {
                TestEvent(this, new TestEventArgs());
            }
        }
    }
}
