﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System.Collections.Generic;
using System.Linq;

namespace KataPotter
{
    class GrandTotal
    {
        private readonly ShoppingBasket _basket;
        private readonly IEnumerable<Discount> _discounts;

        public decimal ItemTotal { get; set; }
        public decimal DiscountTotal { get; set; }
        public decimal Total { get; set; }

        public GrandTotal(ShoppingBasket basket, IEnumerable<Discount> discounts)
        {
            _basket = basket;
            _discounts = discounts;
        }

        public void Calculate()
        {
            CalculateItemTotal();
            CalculateDiscountTotal();
            Total = ItemTotal - DiscountTotal;
        }

        private void CalculateItemTotal()
        {
            ItemTotal = _basket.Items.Sum(item => item.Price);
        }

        private void CalculateDiscountTotal()
        {
            foreach (var discount in _discounts)
            {
                discount.Basket = _basket.Clone() as ShoppingBasket;
                DiscountTotal += discount.Calculate();
            }
        }

    }
}
