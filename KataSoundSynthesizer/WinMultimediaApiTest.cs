﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Threading;
using NUnit.Framework;

namespace KataSoundSynthesizer
{
    [TestFixture]
    public class WinMultimediaApiTest
    {
        private ManualResetEvent _waitHandle = null;

        [Test]
        public void WaveOutSynth_GetDeviceCount()
        {
            Console.WriteLine("device count: " + WaveOutSynth.GetDeviceCount());
        }

        [Test, Explicit]
        public void WaveOutSynth_Play()
        {
            _waitHandle = new ManualResetEvent(false);
            var waveFormat = WaveFormat.MakeIeeeFloatWaveFormat(16000, 2);
            var waveStream = new SweepWaveStream(waveFormat);
            var waveOutSynth = new WaveOutSynth();
            waveOutSynth.Stopped += OnWaveOutSynthStopped;

            waveOutSynth.Init(waveStream);
            waveOutSynth.Play();

            var timeout = _waitHandle.WaitOne(5000);
            waveOutSynth.Stop();
            waveOutSynth.Dispose();
        }

        void OnWaveOutSynthStopped(object sender, StoppedEventData e)
        {
            Console.WriteLine("playback stopped");

            if (_waitHandle != null)
            {
                _waitHandle.Set();
            }
        }
    }
}
