﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using KataCompiler.Ast;
using KataCompiler.Parser;
using NUnit.Framework;

namespace KataCompiler.Compiler
{
    [TestFixture]
    public class XrefBuilderTests
    {
        private Scope _scope;

        [SetUp]
        public void Setup()
        {
            _scope = new Scope();
        }

        [TearDown]
        public void Teardown()
        {
            _scope = null;
        }

        [Test]
        public void BuildXref_CollectConstants()
        {
            var input = "var a, b, c, d, e, f;" + Environment.NewLine
                        + "a = 1 + 2;" + Environment.NewLine
                        + "b = true || true;" + Environment.NewLine
                        + "c = \"foo\" + \"bar\"" + Environment.NewLine
                        + "d = 7 - 4;" + Environment.NewLine
                        + "e = !false;" + Environment.NewLine
                        + "f = \"fo\" + \"ob\" + \"ar\";";

            var exprs = BuildXref(input);
            Assert.AreEqual(3, _scope.Constants.Count());
            var constant = _scope.Constants.ElementAt(0);
            Assert.AreEqual(0, constant.Key);
            Assert.AreEqual("3", constant.Value.Value.ToString());
            Assert.AreEqual(ConstantType.Number, constant.Value.Type);

            constant = _scope.Constants.ElementAt(1);
            Assert.AreEqual(1, constant.Key);
            Assert.AreEqual("True", constant.Value.Value.ToString());
            Assert.AreEqual(ConstantType.Boolean, constant.Value.Type);

            constant = _scope.Constants.ElementAt(2);
            Assert.AreEqual(2, constant.Key);
            Assert.AreEqual("foobar", constant.Value.Value);
            Assert.AreEqual(ConstantType.String, constant.Value.Type);
        }

        [Test, Ignore("because")]
        public void BuildXref_jQuery_extract()
        {
            var input = Properties.Resources.jquery_extract;

            var exprs = BuildXref(input);
            var renderedExprs = RenderExpressions(exprs);
            Console.WriteLine(renderedExprs);
        }

        [Test, Ignore("because")]
        public void BuildXref_jQuery()
        {
            var input = Properties.Resources.jquery_1_10_2;

            var exprs = BuildXref(input);
            var renderedExprs = RenderExpressions(exprs);
            Console.WriteLine(renderedExprs);
        }


        private IEnumerable<IExpression> BuildXref(string text)
        {
            IList<IExpression> xrefExpr = null;
            var bytes = Encoding.Default.GetBytes(text);

            using (var ms = new MemoryStream(bytes))
            {
                var sr = new StreamReader(ms);
                var parser = new EcmaScriptParser(new Morpher(new Lexer(sr)));
                var exprs = parser.ParseModule();
                var resolver = new ExpressionResolver();

                IList<IExpression> resolvedExpr = new List<IExpression>();
                foreach (var expr in exprs)
                {
                    resolvedExpr.Add(resolver.Evaluate(expr, _scope));
                }

                if (resolver.ErrorReporter.NumberOfErrors > 0
                    || resolver.ErrorReporter.NumberOfWarnings > 0)
                {
                    var errorReport = resolver.ErrorReporter.Render();
                    Console.WriteLine(errorReport);
                }

                var xrefBuilder = new XrefBuilder();
                xrefExpr = new List<IExpression>();
                foreach (var expr in resolvedExpr)
                {
                    xrefExpr.Add(xrefBuilder.Evaluate(expr, _scope));
                }
            }

            return xrefExpr;
        }


        private static string RenderExpressions(IEnumerable<IExpression> exprs)
        {
            var sb = new StringBuilder();
            foreach (var expr in exprs)
            {
                expr.AppendTo(sb);
            }

            return sb.ToString();
        }
    }
}
