﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataMachineLearning
{
    class Matrix
    {
        private readonly int _rows, _columns;
        private readonly double[,] _element;

        public int Rows { get { return _rows; } }
        public int Columns { get { return _columns; } }

        public Matrix(int rows, int columns)
        {
            _rows = rows;
            _columns = columns;
            _element = new double[_rows, _columns];
        }

        public double this[int row, int column]
        {
            get
            {
                if (row < 0 || row > Rows
                    || column < 0 || column > Columns)
                {
                    throw new IndexOutOfRangeException();
                }

                return _element[row, column];
            }

            set
            {
                if (row < 0 || row > Rows
                    || column < 0 || column > Columns)
                {
                    throw new IndexOutOfRangeException();
                }

                _element[row, column] = value;
            }
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1.Columns != m2.Rows)
            {
                throw new InvalidOperationException(string.Format("m1.Columns must equal m2.Rows ({0}, {1})", m1.Rows,
                                                                  m2.Columns));
            }

            var m = new Matrix(m1.Rows, m2.Columns);
            for (var row = 0; row < m.Rows; ++row)
            {
                for (var column = 0; column < m.Columns; ++column)
                {
                    for (var i = 0; i < m1.Columns; ++i)
                    {
                        m[row, column] += m1[row, i]*m2[i, column];
                    }
                }
            }

            return m;
        }

        public void Apply(Func<double, double> f)
        {
            for (var row = 0; row < Rows; ++row)
            {
                for (var column = 0; column < Columns; ++column)
                {
                    this[row, column] = f(this[row, column]);
                }
            }
        }
    }
}
