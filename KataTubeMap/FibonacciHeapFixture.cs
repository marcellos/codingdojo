﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using NUnit.Framework;

namespace KataTubeMap
{
    [TestFixture]
    public class FibonacciHeapFixture
    {
        private FibonacciHeap<string, int> _heap;

        [SetUp]
        public void Setup()
        {
            _heap = new FibonacciHeap<string, int>();
        }

        [TearDown]
        public void Teardown()
        {
            _heap = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(FibonacciHeap<string, int>), _heap);
        }

        [Test]
        public void InsertTest()
        {
            Assert.IsNull(_heap.Min);
            var node = _heap.Insert("test", 10);
            Assert.AreEqual(10, node.Key);
            Assert.AreEqual(node, _heap.Min);
            node = _heap.Insert("test", 9);
            Assert.AreEqual(9, node.Key);
            Assert.AreEqual(node, _heap.Min);
            node = _heap.Insert("test", 11);
            Assert.AreEqual(11, node.Key);
            Assert.AreNotEqual(node, _heap.Min);
        }

        [Test]
        public void InsertNullTest()
        {
            _heap.Insert(null, 10);
        }

        [Test]
        public void MergeTest()
        {
            _heap.Insert("test", 10);
            _heap.Insert("test", 9);
            _heap.Insert("test", 11);
            var heap2 = new FibonacciHeap<string, int>();
            heap2.Insert("test", 8);
            heap2.Insert("test", 15);
            heap2.Insert("test", 12);
            var heap = FibonacciHeap<string, int>.Merge(_heap, heap2);
            Assert.AreEqual(8, heap.Min.Key);
        }

        [Test]
        public void DecreaseKeyTest()
        {
            var node10 = _heap.Insert("test", 10);
            _heap.Insert("test", 9);
            var node11 = _heap.Insert("test", 11);
            _heap.Insert("test", 8);
            var node15 = _heap.Insert("test", 15);
            _heap.Insert("test", 12);
            var min = _heap.ExtractMin();

            // for the asserts the structure of the heap
            // is rendered to string

            //Console.WriteLine(_heap.ToString());
            Assert.AreEqual("9-{10-11-{15}}-12", _heap.ToString());
            _heap.DecreaseKey(node11, 8);
            //Console.WriteLine(_heap.ToString());
            Assert.AreEqual("8-{15}-9-{10}-12", _heap.ToString());
            Assert.AreEqual(8, node11.Key);
            _heap.DecreaseKey(node10, 9);
            //Console.WriteLine(_heap.ToString());
            Assert.AreEqual("8-{15}-9-{9}-12", _heap.ToString());
            Assert.AreEqual(9, node10.Key);
            _heap.DecreaseKey(node15, 7);
            //Console.WriteLine(_heap.ToString());
            Assert.AreEqual("7-8-9-{9}-12", _heap.ToString());
            Assert.AreEqual(7, node15.Key);
        }

        [Test]
        public void RemoveTest()
        {
            _heap.Insert("test", 10);
            _heap.Insert("test", 9);
            var node = _heap.Insert("test", 11);
            _heap.Insert("test", 8);
            _heap.Insert("test", 15);
            _heap.Insert("test", 12);
            var removed = _heap.Remove(node);
            var current = _heap.Min;
            Assert.AreNotEqual(removed, current);
            while (!current.Right.Equals(_heap.Min))
            {
                current = current.Right;
                Assert.AreNotEqual(removed, current);
            }
        }

        [Test]
        public void ExtraxtMinTest()
        {
            _heap.Insert("test", 10);
            _heap.Insert("test", 9);
            _heap.Insert("test", 11);
            _heap.Insert("test", 8);
            _heap.Insert("test", 15);
            _heap.Insert("test", 12);

            // for the asserts the structure of the heap
            // is rendered to string

            //Console.WriteLine(_heap.ToString());
            Assert.AreEqual("8-9-10-11-15-12", _heap.ToString());
            var min = _heap.ExtractMin();
            //Console.WriteLine(_heap.ToString());
            Assert.AreEqual("9-{10-11-{15}}-12", _heap.ToString());
            Assert.AreEqual(8, min.Key);
            Assert.AreEqual(9, _heap.Min.Key);

            var minValues = new[] { 9, 10, 11, 12, 15 };
            var heapStructures = new[] 
            {
                "10-{12-11-{15}}",
                "11-{15}-12",
                "12-{15}",
                "15",
                "empty"
            };
            var index = 0;
            while (_heap.Count > 0)
            {
                min = _heap.ExtractMin();
                //Console.Write("min:{0}\t", min.Key);
                Assert.AreEqual(minValues[index], min.Key);
                //Console.WriteLine(_heap.ToString());
                Assert.AreEqual(heapStructures[index], _heap.ToString());
                index++;
            }
        }
    }
}
