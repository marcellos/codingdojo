﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System;

namespace KataSoundSynthesizer.Effect
{
    class Echo : IEffectComponent
    {
        private const int DelayLineMask = 65535;
        private const int DelayLineLength = DelayLineMask + 1;
        private readonly float[] _leftDelayLine = new float[DelayLineLength];
        private readonly float[] _rightDelayLine = new float[DelayLineLength];
        private float _leftDelayTime;
        private float _rightDelayTime;
        private int _leftDelayLineOffset;
        private int _rightDelayLineOffset;
        private int _index;
        private int _li, _ri;
        private int _sampleRate;

        public float Mix { get; set; }
        public float FeedbackLevel { get; set; }
        public float LeftDelayTime
        {
            get { return _leftDelayTime; }
            set
            {
                if (value < 0.0f)
                {
                    throw new ArgumentOutOfRangeException("LeftDelayTime", "must be >= 0");
                }

                _leftDelayTime = value;
                InitializeDefaults();
            }
        }

        public float RightDelayTime
        {
            get { return _rightDelayTime; }
            set
            {
                if (value < 0.0f)
                {
                    throw new ArgumentOutOfRangeException("RightDelayTime", "must be >= 0");
                }

                _rightDelayTime = value;
                InitializeDefaults();
            }
        }

        public int SampleRate
        {
            get { return _sampleRate; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("SampleRate", "must be >= 1");
                }

                _sampleRate = value;
                InitializeDefaults();
            }
        }

        public Echo()
        {
            Mix = 0.75f;
            FeedbackLevel = 0.75f;
            _leftDelayTime = 0.5f;
            _rightDelayTime = 0.5f;
            _sampleRate = 1;

            InitializeDefaults();
        }

        private Echo(Echo echo)
        {
            Mix = echo.Mix;
            FeedbackLevel = echo.FeedbackLevel;
            _leftDelayTime = echo._leftDelayTime;
            _rightDelayTime = echo._rightDelayTime;
            _sampleRate = echo._sampleRate;
            
            InitializeDefaults();
        }

        public IEffectComponent MakeInstanceCopy()
        {
            return new Echo(this);
        }

        private void InitializeDefaults()
        {
            _leftDelayLineOffset = (int) (Math.Min(SampleRate, DelayLineLength)*_leftDelayTime);
            _rightDelayLineOffset = (int) (Math.Min(SampleRate, DelayLineLength)*_rightDelayTime);
            _index = 0;
            _li = (_index + _leftDelayLineOffset) & DelayLineMask;
            _ri = (_index + _rightDelayLineOffset) & DelayLineMask;
        }


        public void Apply(float[,] input)
        {
            float feedback;

            _li = (_index + _leftDelayLineOffset) & DelayLineMask;
            _ri = (_index + _rightDelayLineOffset) & DelayLineMask;
            var inputLength = input.Length/2;

            for (var i = 0; i < inputLength; ++i)
            {
                feedback = _leftDelayLine[_index]*FeedbackLevel;
                _leftDelayLine[_li] = input[0, i]*Mix + feedback;
                feedback = _rightDelayLine[_index]*FeedbackLevel;
                _rightDelayLine[_ri] = input[1, i]*Mix + feedback;

                input[0, i] += _leftDelayLine[_index];
                input[1, i] += _rightDelayLine[_index];

                _li = (_li + 1) & DelayLineMask;
                _ri = (_ri + 1) & DelayLineMask;
                _index = (_index + 1) & DelayLineMask;
            }
        }

        public void Reset()
        {
            Array.Clear(_leftDelayLine, 0, _leftDelayLine.Length);
            Array.Clear(_rightDelayLine, 0, _rightDelayLine.Length);
        }
    }
}
