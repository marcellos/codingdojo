﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Linq;
using NUnit.Framework;

namespace KataTubeMap
{
    [TestFixture]
    public class GraphFixture
    {
        private Graph<string, int> _graph;

        [SetUp]
        public void Setup()
        {
            _graph = new Graph<string, int>();
        }

        [TearDown]
        public void Teardown()
        {
            _graph = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(Graph<string, int>), _graph);
        }

        [Test]
        public void AddNodeTest()
        {
            var node = new GraphNode<string, int>("test");
            _graph.Add(node);
            Assert.True(_graph.Nodes.Contains(node));
        }

        [Test]
        public void AddNullNodeTest()
        {
            GraphNode<string, int> node = null;
            
            Assert.Throws<ArgumentNullException>(() => _graph.Add(node));
        }

        [Test]
        public void AddSameNodeTwiceTest()
        {
            var node = new GraphNode<string, int>("test");
            _graph.Add(node);

            Assert.Throws<ArgumentException>(() => _graph.Add(node));
        }

        [Test]
        public void AddEdgeTest()
        {
            var firstNode = new GraphNode<string, int>("first");
            var secondNode = new GraphNode<string, int>("second");
            _graph.Add(firstNode);
            _graph.Add(secondNode);
            var edge = new GraphEdge<string, int>(1, firstNode, secondNode, EdgeDirectionEnum.OmniDirectional);
            _graph.Add(edge);
            Assert.IsTrue(_graph.Edges.Contains(edge));
        }

        [Test]
        public void AddNullEdgeTest()
        {
            GraphEdge<string, int> edge = null;

            Assert.Throws<ArgumentNullException>(() => _graph.Add(edge));
        }

        [Test]
        public void AddEdgeFirstNodeMissingTest()
        {
            var firstNode = new GraphNode<string, int>("first");
            var secondNode = new GraphNode<string, int>("second");
            _graph.Add(secondNode);
            var edge = new GraphEdge<string, int>(1, firstNode, secondNode, EdgeDirectionEnum.OmniDirectional);

            Assert.Throws<ArgumentException>(() => _graph.Add(edge));
        }

        [Test]
        public void AddEdgeSecondNodeMissingTest()
        {
            var firstNode = new GraphNode<string, int>("first");
            var secondNode = new GraphNode<string, int>("second");
            _graph.Add(firstNode);
            var edge = new GraphEdge<string, int>(1, firstNode, secondNode, EdgeDirectionEnum.OmniDirectional);

            Assert.Throws<ArgumentException>(() => _graph.Add(edge));
        }

        [Test]
        public void AddSameEdgeTwiceTest()
        {
            var firstNode = new GraphNode<string, int>("first");
            var secondNode = new GraphNode<string, int>("second");
            _graph.Add(firstNode);
            _graph.Add(secondNode);
            var edge = new GraphEdge<string, int>(1, firstNode, secondNode, EdgeDirectionEnum.OmniDirectional);
            _graph.Add(edge);

            Assert.Throws<ArgumentException>(() => _graph.Add(edge));
        }

        [Test]
        public void RemoveNodeTest()
        {
            var firstNode = new GraphNode<string, int>("first");
            var secondNode = new GraphNode<string, int>("second");
            _graph.Add(firstNode);
            _graph.Add(secondNode);
            var edge = new GraphEdge<string, int>(1, firstNode, secondNode, EdgeDirectionEnum.OmniDirectional);
            _graph.Add(edge);
            Assert.IsTrue(_graph.Edges.Contains(edge));

            _graph.Remove(firstNode);
            Assert.IsFalse(_graph.Nodes.Contains(firstNode));
            Assert.IsFalse(_graph.Edges.Contains(edge));
        }

        [Test]
        public void RemoveNullNodeTest()
        {
            var firstNode = new GraphNode<string, int>("first");
            var secondNode = new GraphNode<string, int>("second");
            _graph.Add(firstNode);
            _graph.Add(secondNode);
            var edge = new GraphEdge<string, int>(1, firstNode, secondNode, EdgeDirectionEnum.OmniDirectional);
            _graph.Add(edge);
            firstNode = null;

            Assert.Throws<ArgumentNullException>(() => _graph.Remove(firstNode));
        }

        [Test]
        public void RemoveEdgeTest()
        {
            var firstNode = new GraphNode<string, int>("first");
            var secondNode = new GraphNode<string, int>("second");
            _graph.Add(firstNode);
            _graph.Add(secondNode);
            var edge = new GraphEdge<string, int>(1, firstNode, secondNode, EdgeDirectionEnum.OmniDirectional);
            _graph.Add(edge);
            Assert.IsTrue(_graph.Edges.Contains(edge));

            _graph.Remove(edge);
            Assert.IsFalse(_graph.Edges.Contains(edge));
        }

        [Test]
        public void RemoveNullEdgeTest()
        {
            var firstNode = new GraphNode<string, int>("first");
            var secondNode = new GraphNode<string, int>("second");
            _graph.Add(firstNode);
            _graph.Add(secondNode);
            var edge = new GraphEdge<string, int>(1, firstNode, secondNode, EdgeDirectionEnum.OmniDirectional);
            _graph.Add(edge);
            Assert.IsTrue(_graph.Edges.Contains(edge));
            edge = null;

            Assert.Throws<ArgumentNullException>(() => _graph.Remove(edge));
        }
    }
}
