﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using System.Collections.Generic;

namespace KataSoundSynthesizer.Riff
{
    class Chunkbase
    {
        protected IList<ChunkField> _fields;
        protected IDictionary<string, object> _valueMap;

        public IEnumerable<ChunkField> Fields
        {
            get { return _fields; }
        }

        public IDictionary<string, object> ValueMap
        {
            get { return _valueMap; }
        }

        protected static IDictionary<string, object> CreateValueMapFromField(IEnumerable<ChunkField> fields)
        {
            var map = new Dictionary<string, object>();
            foreach (var field in fields)
            {
                map.Add(field.name, null);
            }

            return map;
        }
    }
}
