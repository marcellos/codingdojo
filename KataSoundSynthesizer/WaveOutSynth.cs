﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace KataSoundSynthesizer
{
    class WaveOutSynth
    {
        private IntPtr _hWaveOut;
        private WaveOutBuffer[] _buffers;
        private IWaveStream _waveStream;
        private readonly WaveCallback _waveCallback;
        private readonly WaveCallbackInfo _waveCallbackInfo;
        private readonly object _waveOutLock;
        private int _queuedBuffers;

        public event EventHandler<StoppedEventData> Stopped; 

        public static WaveOutCapabilities GetCapabilities(int deviceNumber)
        {
            var caps = new WaveOutCapabilities();

            try
            {
                WinMultiMediaApi.waveOutGetDevCaps((IntPtr) deviceNumber, out caps, Marshal.SizeOf(caps));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return caps;
        }

        public static int GetDeviceCount()
        {
            return WinMultiMediaApi.waveOutGetNumDevs();
        }

        public int DesiredLatency { get; set; }
        public int NumberOfBuffers { get; set; }
        public int DeviceNumber { get; set; }

        public WaveOutSynth()
        {
            DeviceNumber = 0;
            DesiredLatency = 300;
            NumberOfBuffers = 2;

            _waveCallback = Callback;
            _waveOutLock = new object();
            _waveCallbackInfo = WaveCallbackInfo.FunctionCallback();
            _waveCallbackInfo.Connect(_waveCallback);
        }

        public void Init(IWaveStream waveStream)
        {
            _waveStream = waveStream;

            var bufferSize =
                _waveStream.WaveFormat.ConvertLatencyToByteSize((DesiredLatency + NumberOfBuffers - 1)/NumberOfBuffers);

            MmResult result;
            lock (_waveOutLock)
            {
                result = _waveCallbackInfo.WaveOutOpen(out _hWaveOut, DeviceNumber, _waveStream.WaveFormat,
                                                       _waveCallback);
            }

            _buffers = new WaveOutBuffer[NumberOfBuffers];
            for (var i = 0; i < _buffers.Length; ++i)
            {
                _buffers[i] = new WaveOutBuffer(_hWaveOut, bufferSize, _waveStream, _waveOutLock);
            }

            SetWaveOutVolume(1.0f, _hWaveOut, _waveOutLock);
        }

        public void Play()
        {
            EnqueueBuffers();
        }

        private void EnqueueBuffers()
        {
            for (var i = 0; i < NumberOfBuffers; ++i)
            {
                if (!_buffers[i].InQueue)
                {
                    if (_buffers[i].OnDone())
                    {
                        Interlocked.Increment(ref _queuedBuffers);
                    }
                }
            }
        }

        public void Stop()
        {
            MmResult result;
            lock (_waveOutLock)
            {
                result = WinMultiMediaApi.waveOutReset(_hWaveOut);
            }

            if (result != MmResult.NoError)
            {
                throw new Exception("waveOutReset");
            }
        }

        private static void SetWaveOutVolume(float value, IntPtr hWaveOut, object lockObject)
        {
            if (value < 0 || value > 1)
            {
                throw new ArgumentOutOfRangeException("value", "Volume must be between 0.0 and 1.0");
            }

            var left = value;
            var right = value;

            var stereoVolume = (int) (left*0xFFFF) + ((int) (right*0xFFFF) << 16);
            MmResult result;
            lock (lockObject)
            {
                result = WinMultiMediaApi.waveOutSetVolume(hWaveOut, stereoVolume);
            }
        }

        private void Callback(IntPtr hWaveOut, WaveMessage uMsg, IntPtr dwDistance, WaveHeader wavHdr, IntPtr dwReseverd)
        {
            if (uMsg != WaveMessage.WaveOutDone)
            {
                return;
            }

            var hBuffer = (GCHandle) wavHdr.userData;
            var buffer = (WaveOutBuffer) hBuffer.Target;
            Interlocked.Decrement(ref _queuedBuffers);
            Exception error = null;

            lock (_waveOutLock)
            {
                try
                {
                    if (buffer.OnDone())
                    {
                        Interlocked.Increment(ref _queuedBuffers);
                    }
                }
                catch (Exception ex)
                {
                    error = ex;
                }
            }

            if (_queuedBuffers == 0)
            {
                RaiseStoppedEvent(error);
            }
        }

        private void RaiseStoppedEvent(Exception e)
        {
            if (Stopped != null)
            {
                Stopped(this, new StoppedEventData(e));
            }
        }

        public void Dispose()
        {
            if (_buffers != null)
            {
                foreach (var b in _buffers)
                {
                    if (b != null)
                    {
                        b.Dispose();
                    }
                }

                _buffers = null;
            }

            lock (_waveOutLock)
            {
                WinMultiMediaApi.waveOutClose(_hWaveOut);
            }

            _waveCallbackInfo.Disconnect();
        }
    }
}
