﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;

namespace KataRx
{
    [TestFixture]
    public class EventSynchronizerFixture
    {
        private EventSynchronizer _eventSynchronizer;

        [SetUp]
        public void Setup()
        {
            _eventSynchronizer = new EventSynchronizer(TimeSpan.FromSeconds(10));
        }

        [TearDown]
        public void Teardown()
        {
            _eventSynchronizer = null;
        }

        [Test]
        public void InstanceTest()
        {
            Assert.IsInstanceOf(typeof(EventSynchronizer), _eventSynchronizer);
        }

        [Test, Explicit]
        public void TestEventSourceTest()
        {
            var source = new TestEventSource(1);
            source.TestEvent +=
                (sender, e) => Console.WriteLine("sender: {0}, e: {1}", sender.GetType().Name, e.GetType().Name);
            source.FireOff();
            Thread.Sleep(2000);
        }


        [Test]
        public void MergeEventsTest()
        {
            var source1 = new TestEventSource(1);
            var source2 = new TestEventSource(3);
            source1.TestEvent +=
                (sender, e) => Console.WriteLine("sender: {0}, e: {1}", sender.GetHashCode(), e.GetHashCode());
            source2.TestEvent +=
                (sender, e) => Console.WriteLine("sender: {0}, e: {1}", sender.GetHashCode(), e.GetHashCode());

            _eventSynchronizer.MergeAction(source1);
            _eventSynchronizer.MergeAction(source2);
            var stream = _eventSynchronizer.GetMergedEventStream();
            // stream == null ?
            var disposable = stream.Subscribe(
                p => Console.WriteLine("received: {0} {1}", p.Sender.GetHashCode(), p.EventArgs.GetHashCode()),
                ex => Console.WriteLine("onError: '{0}'", ex.Message),
                () => Console.WriteLine("sequence completed"));
            source1.FireOff();
            source2.FireOff();
            Thread.Sleep(5000);
            disposable.Dispose();
        }

    }
}
