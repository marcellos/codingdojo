﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion

using KataSoundSynthesizer.SynthComponent;

namespace KataSoundSynthesizer.Oscillators
{
    class OscillatorWaveStream : WaveStreamBase
    {
        private readonly ISynthComponent _synthComponent;
        private readonly int _durationInSeconds;
        private long _totalSampleCount;

        public OscillatorWaveStream(WaveFormat waveFormat, ISynthComponent oscillator, int durationInSeconds) 
            : base(waveFormat)
        {
            _synthComponent = oscillator;
            _durationInSeconds = durationInSeconds;
        }

        protected override int Read(float[] samples, int offset, int count)
        {
            var sampleRate = WaveFormat.sampleRate;
            var channels = WaveFormat.channels;
            var sampleCount = count/channels;
            var index = 0;

            _synthComponent.RenderSamples(offset, sampleCount);
            var buffer = _synthComponent.GetMonoBuffer();
            for (var i = 0; i < buffer.Length; ++i)
            {
                samples[index + offset] = buffer[i];
                if (channels == 2)
                {
                    samples[index + offset + 1] = buffer[i];
                }

                index += channels;
            }

            _totalSampleCount += sampleCount;
            var returnValue = count;
            if (_totalSampleCount/sampleRate > _durationInSeconds)
            {
                returnValue = 0;
            }

            return returnValue;
        }
    }
}
