﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataCommandDispatcher
{
    public interface ICommand
    {
        void InvokeResultHandler(ICommandResult result);
        void InvokeExceptionHandler(Exception exception);
    }
}
