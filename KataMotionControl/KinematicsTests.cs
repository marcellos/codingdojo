﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using NUnit.Framework;

namespace KataMotionControl
{
    public class KinematicsTests
    {
        [Test]
        public void CalculateDistanceFromSpeedAndConstantAcceleration_WithPositiveSpeed_ReturnsCorrectResult()
        {
            Assert.AreEqual(0.5, Kinematics.CalculateDistanceFromSpeedAndConstantAcceleration(2.0, 4.0));
        }

        [Test]
        public void CalculateDistanceFromSpeedAndConstantAcceleration_WithNeagtiveSpeed_ReturnsCorrectResult()
        {
            Assert.AreEqual(0.5, Kinematics.CalculateDistanceFromSpeedAndConstantAcceleration(-2.0, 4.0));
        }

        [Test]
        public void CalculateDistanceFromSpeedAndConstantAcceleration_WithNegativeAcceleration_ReturnsCorrectResult()
        {
            Assert.AreEqual(-0.5, Kinematics.CalculateDistanceFromSpeedAndConstantAcceleration(2.0, -4.0));
        }

        [Test]
        public void CalculateDistanceFromSpeedAndConstantAcceleration_WithZeroAcceleratiion_ReturnsPositiveInfinty()
        {
            Assert.AreEqual(double.PositiveInfinity, Kinematics.CalculateDistanceFromSpeedAndConstantAcceleration(2.0, 0.0));
        }

        [Test]
        public void CalculateDistanceFromSpeedAndConstantAcceleration_WithZeroSpeed_ReturnsZero()
        {
            Assert.AreEqual(0.0, Kinematics.CalculateDistanceFromSpeedAndConstantAcceleration(0.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndConstantAcceleration_WithPositiveDistance_ReturnsCorrectResult()
        {
            Assert.AreEqual(4.0, Kinematics.CalculateTimeFromDistanceAndConstantAcceleration(32.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndConstantAcceleration_WithNegativeDistance_ReturnsCorrectResult()
        {
            Assert.AreEqual(4.0, Kinematics.CalculateTimeFromDistanceAndConstantAcceleration(-32.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndConstantAcceleration_WithZeroDistance_ReturnsZero()
        {
            Assert.AreEqual(0.0, Kinematics.CalculateTimeFromDistanceAndConstantAcceleration(0.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndConstantAcceleration_WithNegativeAcceleration_ReturnsNaN()
        {
            Assert.AreEqual(double.NaN, Kinematics.CalculateTimeFromDistanceAndConstantAcceleration(32.0, -4.0));
        }

        [Test]
        public void CalculateTimeFromSpeedAndConstantAcceleration_WithPositiveSpeed_ReturnsCorrectResult()
        {
            Assert.AreEqual(8.0, Kinematics.CalculateTimeFromSpeedAndConstantAcceleration(32.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromSpeedAndConstantAcceleration_WithNegativeSpeed_ReturnsCorrectResult()
        {
            Assert.AreEqual(-8.0, Kinematics.CalculateTimeFromSpeedAndConstantAcceleration(-32.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromSpeedAndConstantAcceleration_WithZeroSpeed_ReturnsZero()
        {
            Assert.AreEqual(0.0, Kinematics.CalculateTimeFromSpeedAndConstantAcceleration(0.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromSpeedAndConstantAcceleration_WithNegativeAcceleration_ReturnsCorrectResult()
        {
            Assert.AreEqual(-8.0, Kinematics.CalculateTimeFromSpeedAndConstantAcceleration(32.0, -4.0));
        }

        [Test]
        public void CalculateTimeFromSpeedAndConstantAcceleration_WithZeroAcceleration_ReturnsPositiveInfinity()
        {
            Assert.AreEqual(double.PositiveInfinity, Kinematics.CalculateTimeFromSpeedAndConstantAcceleration(32.0, 0.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndSpeed_WithPositiveDistance_ReturnsCorrectResult()
        {
            Assert.AreEqual(2.0, Kinematics.CalculateTimeFromDistanceAndSpeed(8.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndSpeed_WithNegativeDistance_ReturnsCorrectResult()
        {
            Assert.AreEqual(2.0, Kinematics.CalculateTimeFromDistanceAndSpeed(-8.0, 4.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndSpeed_WithNegativeSpeed_ReturnsCorrectResult()
        {
            Assert.AreEqual(2.0, Kinematics.CalculateTimeFromDistanceAndSpeed(8.0, -4.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndSpeed_WithNegatives_ReturnsCorrectResult()
        {
            Assert.AreEqual(2.0, Kinematics.CalculateTimeFromDistanceAndSpeed(-8.0, -4.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndSpeed_WithZeroSpeed_ReturnsPositiveInfinity()
        {
            Assert.AreEqual(double.PositiveInfinity, Kinematics.CalculateTimeFromDistanceAndSpeed(8.0, 0.0));
        }

        [Test]
        public void CalculateTimeFromDistanceAndSpeed_WithNegativesAndZeroSpeed_ReturnsPositiveInfinity()
        {
            Assert.AreEqual(double.PositiveInfinity, Kinematics.CalculateTimeFromDistanceAndSpeed(-8.0, 0.0));
        }
    }
}
