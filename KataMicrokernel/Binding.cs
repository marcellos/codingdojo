#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;

namespace KataMicrokernel
{
    public struct Binding
    {
        public Type AbstractType { get; private set; }
        public Type ImplementationType { get; private set; }
        public int ScopeHashCode { get; set; }

        internal Binding(Type abstractType, Type implementationType)
            : this()
        {
            AbstractType = abstractType;
            ImplementationType = implementationType;
        }

        internal Binding(Binding binding)
            : this()
        {
            AbstractType = binding.AbstractType;
            ImplementationType = binding.ImplementationType;
            ScopeHashCode = binding.ScopeHashCode;
        }
    }
}