﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;

namespace KataMicrokernel
{
    public class Bindings
    {
        private readonly HashSet<Binding> _bindingsSet;

        public Bindings()
        {
            _bindingsSet = new HashSet<Binding>();
        }

        public Binding Add(Type abstractType, Type implementationType)
        {
            if (abstractType == null)
            {
                throw new ArgumentNullException("abstractType");
            }

            if (implementationType == null)
            {
                throw new ArgumentNullException("implementationType");
            }

            if (abstractType == implementationType)
            {
                throw new ArgumentException("abstractType cannot be same as implementationType");
            }

            if (implementationType.IsInterface)
            {
                throw new ArgumentException("implementationType cannot be an interface type");
            }

            if (implementationType.IsAbstract)
            {
                throw new ArgumentException("implementationType cannot be an abstract type");
            }

            if (!abstractType.IsAssignableFrom(implementationType))
            {
                throw new ArgumentException(string.Format("type '{0}' cannot be assigned to type '{1}' ",
                    implementationType.FullName,
                    abstractType.FullName));
            }

            if (_bindingsSet.Any(b => b.AbstractType == abstractType
                && b.ImplementationType == implementationType))
            {
                return _bindingsSet.First(b => b.AbstractType == abstractType
                    && b.ImplementationType == implementationType);
            }

            var binding = new Binding(abstractType, implementationType);
            _bindingsSet.Add(binding);
            return binding;
        }

        public IEnumerable<Type> GetImplementationTypes(Type abstractType)
        {
            return _bindingsSet
                .Where(b => b.AbstractType == abstractType)
                .Select(b => b.ImplementationType)
                .ToList();
        }

        public Binding GetBindingForImplementationType(Type implementationType)
        {
            return _bindingsSet.FirstOrDefault(b => b.ImplementationType == implementationType);
        }

        public void UpdateBinding(Binding oldBinding, Binding newBinding)
        {
            _bindingsSet.RemoveWhere(b => b.Equals(oldBinding));
            _bindingsSet.Add(newBinding);
        }
    }
}
