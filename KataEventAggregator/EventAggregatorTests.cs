﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace KataEventAggregator
{
    [TestFixture]
    public class EventAggregatorTests
    {
        private IEventAggregator _eventAggregator;

        [SetUp]
        public void Setup()
        {
            _eventAggregator = new EventAggregator();
        }

        [TearDown]
        public void Teardown()
        {
            _eventAggregator = null;
        }

        [Test]
        public void CanCreate_Instance()
        {
            Assert.IsInstanceOf(typeof(EventAggregator), _eventAggregator);
        }

        [Test]
        public void PublishSubscribe_WithInt_MessageDelivered()
        {
            var intEvent = _eventAggregator.GetEvent<int>();

            var message = 0;
            var intObserver = new IntObserver();
            var unsubscriber1 = intEvent.Subscribe(intObserver);
            var unsubscriber2 = intEvent.Subscribe(m => message = m, ThreadOption.UiThread);

            _eventAggregator.Publish(100);
            Assert.AreEqual(100, message);
            _eventAggregator.Publish(200);
            Assert.AreEqual(200, message);

            unsubscriber1.Dispose();
            unsubscriber2.Dispose();
        }

        [Test]
        public void Subscribe_WithDisposeTwice_DoesNotThrowException()
        {
            var intEvent = _eventAggregator.GetEvent<int>();
            var unsubscriber = intEvent.Subscribe(new IntObserver());
            unsubscriber.Dispose();
            unsubscriber.Dispose();
        }

        [Test]
        public void PublishSubscriber_WithSubscriberThrows_ThrowsException()
        {
            var stringEvent = _eventAggregator.GetEvent<string>();
            stringEvent.Subscribe(s =>
            {
                throw new NotSupportedException();
            });

            Assert.Throws<NotSupportedException>(() => _eventAggregator.Publish("message"));
        }

        [Test]
        public void PublishSubscriber_WithTwoSubscriber_OneThrows_ThrowsException_Message_Delivered()
        {
            var stringEvent = _eventAggregator.GetEvent<string>();
            string message = string.Empty;
            Exception error = null;

            var unsubcriber1 = stringEvent.Subscribe(s =>
            {
                throw new NotSupportedException();
            });
            var unsubcriber2 = stringEvent.Subscribe(m => message = m);

            try
            {
                _eventAggregator.Publish("message");
            }
            catch (Exception ex)
            {
                error = ex;
            }

            Assert.AreEqual(false, string.IsNullOrEmpty(message));
            Assert.IsInstanceOf(typeof(NotSupportedException), error);

            unsubcriber1.Dispose();
            unsubcriber2.Dispose();
        }

        [Test]
        public void Subscribe_WithSubscriberNull_ThrowsException()
        {
            var intEvent = _eventAggregator.GetEvent<int>();
            IntObserver nullObserver = null;

            Assert.Throws<ArgumentNullException>(() => intEvent.Subscribe(nullObserver));
        }

        [Test]
        public void Subscribe_WithActionNull_ThrowsException()
        {
            var intEvent = _eventAggregator.GetEvent<int>();
            Action<int> nullAction = null;

            Assert.Throws<ArgumentNullException>(() => intEvent.Subscribe(nullAction));
        }

        [Test]
        public void PublishSubcribe_WithNull_NullSend()
        {
            var stringEvent = _eventAggregator.GetEvent<string>();
            const string InitialMessage = "initial_message";
            var message = InitialMessage;
            var unsubscriber = stringEvent.Subscribe(m => message = m);
            _eventAggregator.Publish<string>(null);
            Assert.IsTrue(string.IsNullOrEmpty(message));
            unsubscriber.Dispose();
        }

        [Test]
        public void PublishSubscribe_WithCollectedObserver_ReferencesArePrunedCorrectly()
        {
            var intEvent = _eventAggregator.GetEvent<int>();
            var observer1 = new IntObserver();
            var observer2 = new IntObserver();
            var unsubscriber1 = intEvent.Subscribe(observer1);
            var unsubscriber2 = intEvent.Subscribe(observer2);
            _eventAggregator.Publish(100);
            Assert.AreEqual(100, observer1.Message);
            Assert.AreEqual(100, observer2.Message);

            observer1 = null;
            unsubscriber1 = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();

            _eventAggregator.Publish(101);
            Assert.AreEqual(101, observer2.Message);

            unsubscriber2.Dispose();
        }

        [Test]
        public void PublishSubscribe_WithCollectedUnsubscriber_ReferencesArePrunedCorrectly()
        {
            var intEvent = _eventAggregator.GetEvent<int>();
            var message1 = 0;
            var message2 = 0;
            var unsubscriber1 = intEvent.Subscribe(m => message1 = m);
            var unsubscriber2 = intEvent.Subscribe(m => message2 = m);
            _eventAggregator.Publish(100);
            Assert.AreEqual(100, message1);
            Assert.AreEqual(100, message2);

            unsubscriber1 = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();

            _eventAggregator.Publish(101);
            Assert.AreEqual(100, message1);
            Assert.AreEqual(101, message2);

            unsubscriber2.Dispose();
        }

        [TestCase(100000), Explicit]
        public void PublishSubscribe_WithObservers_ObserveMemoryConsumption(int numberOfObjects)
        {
            var intEvent = _eventAggregator.GetEvent<int>();
            var observerList = new List<IntObserver>();
            var unsubscriberList = new List<IDisposable>();

            Console.Out.WriteLine("creating {0} objects..", numberOfObjects);
            for (var i = 0; i < numberOfObjects; i++)
            {
                var observer = new IntObserver();
                var unsubscriber = intEvent.Subscribe(observer);
                observerList.Add(observer);
                unsubscriberList.Add(unsubscriber);
            }

            Console.Out.WriteLine("publishing 1 message..");
            _eventAggregator.Publish(100);

            Console.Out.WriteLine("asserting message has been delivered..");
            for (var i = 0; i < numberOfObjects; i++)
            {
                Assert.AreEqual(100, observerList[i].Message);
            }

            Console.Out.WriteLine("memory consumption: {0} bytes", Environment.WorkingSet);

            var numberOfObjectsToRemove = numberOfObjects / 2;
            Console.Out.WriteLine("removing {0} objects..", numberOfObjectsToRemove);
            for (var i = 0; i < numberOfObjectsToRemove; i++)
            {
                observerList.RemoveAt(i);
                unsubscriberList.RemoveAt(i);
            }

            Console.Out.WriteLine("collecting garbage..");
            GC.Collect();
            GC.WaitForPendingFinalizers();

            Console.Out.WriteLine("publishing 1 message..");
            _eventAggregator.Publish(101);

            Console.Out.WriteLine("asserting message has been delivered..");
            for (var i = 0; i < numberOfObjectsToRemove; i++)
            {
                Assert.AreEqual(101, observerList[i].Message);
            }

            Console.Out.WriteLine("memory consumption: {0} bytes", Environment.WorkingSet);

            Console.Out.WriteLine("unsubscribing..");
            unsubscriberList.ForEach(u => u.Dispose());
        }

        private class IntObserver : IObserver<int>
        {
            public int Message
            {
                get;
                private set;
            }

            public void OnNext(int value)
            {
                Message = value;
            }

            public void OnError(Exception error)
            {
                throw new NotImplementedException();
            }

            public void OnCompleted()
            {
                throw new NotImplementedException();
            }
        }
    }
}
