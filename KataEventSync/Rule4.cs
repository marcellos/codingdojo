﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataEventSync
{
    public class Rule4 : Rule<User>
    {
        private bool _isNameFred, _isNameMike;

        protected override object DoEvaluate(User message)
        {
            _isNameFred = _isNameFred || message.Name.Equals("Fred");
            _isNameMike = _isNameMike || message.Name.Equals("Mike");
            if (_isNameMike && _isNameMike)
            {
                _isNameFred = _isNameMike = false;
                return "$rule4";
            }

            return null;
        }
    }
}
