﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace KataEventSync
{
    public class EventAggregator : IEventAggregator
    {
        public static Action<Action> DefaultPublicationThreadMarshaler = action => action();
        public Action<Action> PublicationThreadMarshaler { get; set; }
        private readonly List<Handler> _handlers = new List<Handler>();

        public EventAggregator()
        {
            PublicationThreadMarshaler = DefaultPublicationThreadMarshaler;
        }

        public virtual void Subscribe(object instance)
        {
            lock (_handlers)
            {
                if (_handlers.Any(h => h.IsMatch(instance)))
                {
                    return;
                }

                _handlers.Add(new Handler(instance));
            }
        }

        public virtual void Unsubscribe(object instance)
        {
            lock (_handlers)
            {
                var registeredInstance = _handlers.FirstOrDefault(h => h.IsMatch(instance));
                if (registeredInstance != null)
                {
                    _handlers.Remove(registeredInstance);
                }
            }
        }

        public virtual void Publish(object message)
        {
            Publish(message, PublicationThreadMarshaler);
        }

        public virtual void Publish(object message, Action<Action> marshal)
        {
            lock (_handlers)
            {
                marshal(() =>
                    {
                        var messageType = message.GetType();
                        var deadHandlers = _handlers.Where(h => !h.Handle(messageType, message)).ToList();
                        if (deadHandlers.Any())
                        {
                            foreach (var handler in deadHandlers)
                            {
                                _handlers.Remove(handler);
                            }
                        }
                    });
            }
        }

        protected class Handler
        {
            private readonly WeakReference _reference;
            private readonly Dictionary<Type, MethodInfo> _supportedHandlers = new Dictionary<Type, MethodInfo>();

            public Handler(object handler)
            {
                _reference = new WeakReference(handler);

                var interfaces = handler.GetType().GetInterfaces()
                    .Where(h => typeof(IHandle).IsAssignableFrom(h) && h.IsGenericType);
                foreach (var @interface in interfaces)
                {
                    var type = @interface.GetGenericArguments()[0];
                    var handleMethod = @interface.GetMethod("Handle");
                    _supportedHandlers[type] = handleMethod;
                }
            }

            public bool IsMatch(object instance)
            {
                return _reference.Target == instance;
            }

            public bool Handle(Type messageType, object message)
            {
                var target = _reference.Target;
                if (target == null)
                {
                    return false;
                }

                foreach (var pair in _supportedHandlers.Where(pair => pair.Key.IsAssignableFrom(messageType)))
                {
                    pair.Value.Invoke(target, new[] { message });
                    return true;
                }

                return true;
            }
        }
    }
}
