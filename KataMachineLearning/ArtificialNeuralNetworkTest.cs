﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

namespace KataMachineLearning
{
    [TestFixture]
    public class ArtificialNeuralNetworkTest
    {
        [Test]
        public void Calculate_WhenPropagated_ThenGetResult()
        {
            var layerNodes = new[] { 2, 3, 1 };
            var w1 = new Matrix(layerNodes[0], layerNodes[1]);
            var w2 = new Matrix(layerNodes[1], layerNodes[2]);
            var w = new[] { w1, w2 };
            var ann = new ArtificialNeuralNetwork(layerNodes, w);
            var v = CreateValues();

            var output = ann.Calculate(v);
            Assert.AreEqual(2, output.GetLength(0));
            Assert.AreEqual(1, output.GetLength(1));
        }

        private static double[,] CreateValues()
        {
            var v = new double[2,2];
            v[0, 0] = 1.0;
            v[0, 1] = 0.9;
            v[1, 0] = 0.8;
            v[1, 1] = 0.7;

            return v;
        }
    }
}
