﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
using System;
using System.IO;
using NUnit.Framework;

namespace KataSoundSynthesizer.Wave
{
    [TestFixture]
    public class FileReaderTest
    {
        private const string OpenHat003 = @"Open Hat 003.wav";

        private static string CurrentDirectory = TestContext.CurrentContext.TestDirectory;

        [Test]
        public void Read_WithOpenHat003_ThenFileIsRead()
        {
            var waveData = FileReader.Read(Path.Combine(CurrentDirectory, "SoundBank", OpenHat003));
            Assert.AreEqual(2, waveData.Channels);
            Assert.AreEqual(44100, waveData.SampleRate);
            Assert.AreEqual(16, waveData.BitsPerSample);
        }

        [Test]
        public void Read_WithOpenHat003_ThenConvertBuffer()
        {
            var waveData = FileReader.Read(Path.Combine(CurrentDirectory, "SoundBank", OpenHat003));
            var floatBuffer = WaveDataConverter.ConvertToFloatBuffer(waveData);
            Assert.AreEqual(66150, floatBuffer.Length);
        }
    }
}
