﻿#region license and copyright
/*
 * The MIT License, Copyright (c) 2011-2020 Marcel Schneider
 * for details see License.txt
 */
#endregion
        
namespace KataCompiler
{
    public enum SpanTypeEnum
    {
        Text,
        Whitespace,
        EndOfLine
    }
}
